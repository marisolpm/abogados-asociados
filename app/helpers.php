<?php

function cabecera($copia, $textypos, $pdf, $recibo, $x_plus, $y_plus) {

    $pdf->Image($_SERVER['DOCUMENT_ROOT'] . '\img\logo.jpeg', 15 + $x_plus, 12 + $y_plus, 25);
    $pdf->SetFont('Arial', 'B', 10);

    $pdf->setY(15 + $y_plus);
    $pdf->setX(83 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("ESTUDIO JURÍDICO"));

    $pdf->SetFont('Arial', 'B', 15);
    $pdf->setY(21 + $y_plus);
    $pdf->setX(71 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("MEDINA & ASOCIADOS"));



// Agregamos los datos del cliente
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setY(26 + $y_plus);
    $pdf->setX(56 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("Calle Yanacocha y Mercado, Edificio Asbun Antiguo, Piso 1 Of. 7"));

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setY(30 + $y_plus);
    $pdf->setX(53 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("Contacto: 2911332 - 78797801, Email: asociadosmedina07@gmail.com"));

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setY(34 + $y_plus);
    $pdf->setX(90 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("La Paz - Bolivia"));

    // Agregamos los datos del cliente

    $pdf->Line(159 + $x_plus, 15 + $y_plus, 195 + $x_plus, 15 + $y_plus);
    $pdf->Line(159 + $x_plus, 15 + $y_plus, 159 + $x_plus, 33 + $y_plus);
    $pdf->Line(195 + $x_plus, 15 + $y_plus, 195 + $x_plus, 33 + $y_plus);
    $pdf->Line(159 + $x_plus, 33 + $y_plus, 195 + $x_plus, 33 + $y_plus);

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setY(17 + $y_plus);
    $pdf->setX(160 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("RECIBO ELECTRÓNICO"));

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(140, 0, 0);
    $pdf->setY(22 + $y_plus);
    $pdf->setX(169 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("N° " . $recibo[0]->o_numero_recibo));

    $pdf->SetFont('Arial', '', 6);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->setY(27 + $y_plus);
    $pdf->setX(168 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($copia));
}

function cuerpo($textypos, $pdf, $recibo, $x_plus, $y_plus) {
    $pdf->Line(15 + $x_plus, 40 + $y_plus, 195 + $x_plus, 40 + $y_plus);
    $pdf->Line(15 + $x_plus, 40 + $y_plus, 15 + $x_plus, 112 + $y_plus);
    $pdf->Line(195 + $x_plus, 40 + $y_plus, 195, 112 + $y_plus);
    $pdf->Line(15 + $x_plus, 112 + $y_plus, 195, 112 + $y_plus);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->setY(42 + $y_plus);
    $pdf->setX(17 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("En el día:"));


    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(42 + $y_plus);
    $pdf->setX(60 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($recibo[0]->o_fecha_literal));

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->setY(52 + $y_plus);
    $pdf->setX(17 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("Recibí del Señor(es):"));

    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(52 + $y_plus);
    $pdf->setX(60 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($recibo[0]->o_nombre_cliente));

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->setY(62 + $y_plus);
    $pdf->setX(17 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("La Suma de:"));

    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(62 + $y_plus);
    $pdf->setX(60 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($recibo[0]->o_monto_pago . ".- (" . $recibo[0]->o_monto_pago_literal . ")"));

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->setY(72 + $y_plus);
    $pdf->setX(17 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("Por Concepto de:"));

    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(72 + $y_plus);
    $pdf->setX(60 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($recibo[0]->o_glosa_pago));

    $pdf->Line(35 + $x_plus, 95 + $y_plus, 85 + $x_plus, 95 + $y_plus);
    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(97 + $y_plus);
    $pdf->setX(40 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("ENTREGUÉ CONFORME"));

    $pdf->setY(101 + $y_plus);
    $pdf->setX(40 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode($recibo[0]->o_nombre_cliente));
    
    $pdf->setY(106 + $y_plus);
    $pdf->setX(40 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("CI: " . $recibo[0]->o_ci));

    $pdf->Line(125 + $x_plus, 95 + $y_plus, 175 + $x_plus, 95 + $y_plus);
    $pdf->SetFont('Arial', '', 10);
    $pdf->setY(97 + $y_plus);
    $pdf->setX(130 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("RECIBÍ CONFORME"));

    $pdf->setY(101 + $y_plus);
    $pdf->setX(130 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode(strtoupper(str_replace('.',' ',$recibo[0]->o_usuario_mod))));
    
    $pdf->setY(106 + $y_plus);
    $pdf->setX(130 + $x_plus);
    $pdf->Cell(5, $textypos, utf8_decode("CI: " . $recibo[0]->o_ci_usuario));
}

function getNumPagesInPDF($filePath) {
    if (!file_exists($filePath))
        return 0;
    if (!$fp = @fopen($filePath, "r"))
        return 0;
    $i = 0;
    $type = "/Contents";
    while (!feof($fp)) {
        $line = fgets($fp, 255);
        $x = explode($type, $line);
        if (count($x) > 1) {
            $i++;
        }
    }
    fclose($fp);
    return (int) $i;
}
