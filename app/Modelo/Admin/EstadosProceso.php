<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class EstadosProceso extends Model {

    protected $table = 'estados_proceso';
    protected $fillable = ['estado_proceso', 'descripcion', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaEstadosProceso() {
        $proceso = \DB::select('SELECT * FROM proc_estado_proceso();');
        return $proceso;
    }

    protected static function RegistroEstadosProceso($tipo_registro, $cliente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, Auth::user()->id));
        return $proceso;
    }

    protected static function ActualizarEstadosProceso($tipo_registro, $cliente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, $id_proceso) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, Auth::user()->id, $id_proceso));
        return $proceso;
    }

}
