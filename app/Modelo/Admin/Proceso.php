<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Proceso extends Model {

    protected $table = 'procesos';
    protected $fillable = ['id_cliente', 'id_abogado', 'id_posicion', 'contrario', 'proceso', 'tipo_proceso',
        'estado_proceso', 'involucrados', 'fecha_inicio', 'fecha_final', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaProcesos($id_proceso, $id_cliente) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_listado(?,?);', array($id_proceso, $id_cliente));
        return $proceso;
    }

    protected static function RegistroProceso($tipo_registro, $cliente, $expediente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $expediente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, Auth::user()->id));
        return $proceso;
    }

    protected static function ActualizarProceso($tipo_registro, $cliente, $expediente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, $id_proceso) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $expediente, $posicion, $contrario, $nombre_proceso, $descripcion, $tipo_proceso, $estado_proceso, $i_involucrados, $abogado, $cita_fecha_hora, Auth::user()->id, $id_proceso));
        return $proceso;
    }

    protected static function EliminarProceso($tipo_registro, $id_proceso) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, -1, -1, -1, '', '', '', -1, -1, '', -1, '2000-01-01 00:00', Auth::user()->id, $id_proceso));
        return $proceso;
    }
    
    protected static function ArchivarProceso($tipo_registro, $id_proceso) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, -1, -1, -1, '', '', '', -1, -1, '', -1, '2000-01-01 00:00', Auth::user()->id, $id_proceso));
        return $proceso;
    }

    protected static function ListaProcesosDocumentos($id_proceso, $id_proceso_doc) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_documento_listado(?,?);', array($id_proceso, $id_proceso_doc));
        return $proceso;
    }

    protected static function ListaProcesosPosicion($id_posicion) {
        $proceso = \DB::select('SELECT * FROM proc_proceso_posicion(?);', array($id_posicion));
        return $proceso;
    }

}
