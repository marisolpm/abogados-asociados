<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Mensajeria extends Model {

    protected $table = 'mensajeria';
    protected $fillable = ['id_cliente', 'id_empleado', 'asunto', 'mensaje', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaMensajes($id_mensaje, $id_cliente, $id_abogado) {
        $mensaje = \DB::select('SELECT * FROM proc_mensaje_listado(?,?,?);', array($id_mensaje, $id_cliente, $id_abogado));
        return $mensaje;
    }

protected static function RegistroMensaje($id_mensaje,$id_cliente,$id_abogado,$asunto,$mensaje) {
        $mensaje = \DB::select('SELECT * FROM proc_mensaje_ins_upd(?,?,?,?,?,?)',
                        array($id_mensaje,$id_cliente,$id_abogado,$asunto,$mensaje, Auth::user()->id));
        return $mensaje;
    }
}