<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Recibo extends Model {

    protected $table = 'recibos_pagos';
    protected $fillable = ['id_cuenta', 'numero_recibo', 'id_tipo_pago', 'glosa_pago', 'monto_pago', 'registro', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaRecibos($id_recibo, $id_cliente) {
        $cuenta = \DB::select('SELECT * FROM proc_recibo_listado(?,?);', array($id_recibo, $id_cliente));
        return $cuenta;
    }

    protected static function RegistroRecibo($tipo_registro, $id_recibo, $cliente, $id_cuenta, $monto, $glosa, $numero_recibo) {
        $cuenta = \DB::select('SELECT * FROM proc_recibo_ins_upd(?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $id_recibo, $cliente, $id_cuenta, $monto, $glosa, Auth::user()->id, $numero_recibo));
        return $cuenta;
    }

    protected static function datosRecibo() {
        $numeroCuenta = \DB::select('SELECT * FROM fn_numero_recibo();');
        return $numeroCuenta;
    }

}
