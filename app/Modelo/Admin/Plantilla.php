<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Plantilla extends Model {

    protected $table = 'plantillas';
    protected $fillable = ['id_tipo_plantilla', 'plantilla', 'url', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaPlantillas($id_plantilla, $id_usuario, $estado) {
        $plantilla = \DB::select('SELECT * FROM proc_plantilla_listado(?,?,?);', array($id_plantilla, $id_usuario, $estado));
        return $plantilla;
    }

    protected static function RegistroPlantilla($plantilla, $plantilla_url) {
        $plantilla = \DB::select('SELECT * FROM proc_plantilla_ins_upd(?,?,?)',
                        array($plantilla, $plantilla_url, Auth::user()->id));
        return $plantilla;
    }

}
