<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Expediente extends Model {

    protected $table = 'expedientes';
    protected $fillable = ['id_expediente', 'id_cliente', 'codigo_expediente','nro_expediente', 'demandante', 'demandado', 'materia', 'contingencia', 'asignado',
        'respaldo','registrado', 'modificado', 'usuario_mod', 'usuario_reg', 'estado', 'observaciones', 'estado_expediente'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaExpedientes($id_expediente, $id_cliente, $posicion) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_listado(?,?,?);', array($id_expediente, $id_cliente, $posicion));
        return $expediente;
    }

    protected static function RegistroExpediente($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, $estado_expediente, $contingencia, $observaciones) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, $estado_expediente, $contingencia, '[]', $observaciones, Auth::user()->id));
        return $expediente;
    }

    protected static function ActualizarExpediente($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, $estado_expediente, $contingencia, $observaciones, $id) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, $estado_expediente, $contingencia, '[]', $observaciones, Auth::user()->id, $id));
        return $expediente;
    }

    protected static function ListaExpedientesDocumentos($id_expediente, $id_expediente_doc) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_documento_listado(?,?);', array($id_expediente, $id_expediente_doc));
        return $expediente;
    }

}
