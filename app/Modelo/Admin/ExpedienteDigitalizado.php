<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class ExpedienteDigitalizado extends Model {

    protected $table = 'expedientes_digitalizados';
    protected $fillable = ['id', 'id_expediente', 'id_cliente', 'nro_expediente', 'nro_expediente', 'tipo_expediente',
        'texto_expediente', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod', 'estado', 'url_documento'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaExpedientes($id_expediente, $id_cliente) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_digitalizado_listado(?,?);', array($id_expediente, $id_cliente));
        return $expediente;
    }

    protected static function RegistroExpediente($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, $estado_expediente, $contingencia, $observaciones) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_digitalizado_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $cliente, $codigo, $numero, $demandante, $demandado, $materia, '', Auth::user()->id, $estado_expediente, $contingencia, '[]', $observaciones));
        return $expediente;
    }

    protected static function ActualizarExpediente($tipo_registro, $id_expediente, $numero, $texto, $tipo, $materia, $url, $id_cliente, $id, $url_word) {
        $expediente = \DB::select('SELECT * FROM proc_expediente_digitalizado_ins_upd(?,?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $id_expediente, $numero, $texto, $tipo, $materia, $url, Auth::user()->id, $id_cliente, $id, $url_word));
        return $expediente;
    }

}
