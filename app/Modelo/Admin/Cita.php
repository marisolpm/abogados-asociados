<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model {

    protected $table = 'citas';
    protected $fillable = ['id_cliente', 'id_empleado', 'titulo', 'nota', 'mensaje', 'fecha_hora_cita', 'lugar_cita', 'estado_cita', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaCitas($id_cita, $id_cliente,$id_abogado) {
        $cuenta = \DB::select('SELECT * FROM proc_cita_listado(?,?,?);', array($id_cita, $id_cliente, $id_abogado));
        return $cuenta;
    }

    protected static function RegistroCita($tipo_registro, $id_cita, $id_cliente, $id_abogado, $asunto, $lugar_cita, $estado_cita, $fecha_hora_cita, $descripcion_cita) {
        $cita = \DB::select('SELECT * FROM proc_cita_ins_upd(?,?,?,?,?,?,?,?,?,?)',
                        array($tipo_registro, $id_cita, $id_cliente, $id_abogado, $asunto, $lugar_cita, $estado_cita, $fecha_hora_cita, $descripcion_cita, Auth::user()->id));
        return $cita;
    }

    protected static function datosCita() {
        $numeroCuenta = \DB::select('SELECT * FROM fn_numero_recibo();');
        return $numeroCuenta;
    }

}
