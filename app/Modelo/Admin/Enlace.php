<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Enlace extends Model {

    protected $table = 'enlaces';
    protected $fillable = ['nombre', 'enlace', 'descripcion', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaEnlaces($id_enlace, $id_usuario, $estado) {
        $enlace = \DB::select('SELECT * FROM proc_enlace_listado(?,?,?);', array($id_enlace, $id_usuario, $estado));
        return $enlace;
    }

    protected static function RegistroEnlace($enlace, $nombre, $descripcion, $id) {
        $enlace = \DB::select('SELECT * FROM proc_enlace_ins_upd(?,?,?,?,?)',
                        array($enlace, $nombre, $descripcion, Auth::user()->id, $id));
        return $enlace;
    }

}
