<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model {

    protected $table = 'cuentas';
    protected $fillable = ['id_cliente', 'id_proceso', 'id_tipo_cuenta', 'numero_cuenta', 'estado_cuenta', 'registro', 'modificado', 'usuario_reg', 'usuario_mod', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaCuentas($id_proceso, $id_cliente) {
        $proceso = \DB::select('SELECT * FROM proc_cuenta_listado(?,?);', array($id_proceso, $id_cliente));
        return $proceso;
    }

    protected static function RegistroCuenta($tipo_registro,$id_cuenta,$cliente,$tipo_cuenta,$proceso,$descripcion,$numero_cuenta) {
        $proceso = \DB::select('SELECT * FROM proc_cuenta_ins_upd(?,?,?,?,?,?,?,?)',
                        array($tipo_registro,$id_cuenta,$cliente,$tipo_cuenta,$proceso,$descripcion, Auth::user()->id,$numero_cuenta));
        return $proceso;
    }

    protected static function datosCuenta() {
        $numeroCuenta = \DB::select('SELECT * FROM fn_numero_cuenta();');
        return $numeroCuenta;
    }

}
