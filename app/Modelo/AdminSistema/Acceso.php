<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;

class Acceso extends Model {

    protected $table = 'acceso.accesos';
    protected $fillable = ['id', 'opc_id', 'rls_id', 'registrado', 'modificado', 'usr_id', 'estado'];
    public $timestamps = false;
    protected $primaryKey = 'id';

    protected static function getListar() {
        $acceso = Acceso::join('acceso.opciones as o', 'o.id', '=', 'acceso.accesos.opc_id')
                ->join('acceso.roles as r', 'r.id', '=', 'acceso.accesos.rls_id')
                ->select('acceso.accesos.id',
                        'acceso.accesos.opc_id',
                        'o.opcion',
                        'o.contenido',
                        'acceso.accesos.rls_id',
                        'r.rol',
                        'acceso.accesos.registrado',
                        'acceso.accesos.modificado')
                ->where('acceso.accesos.estado', 'A')
                ->where('acceso.accesos.rls_id', '1')
                ->OrderBy('o.opc_id', 'ASC')
                ->get();
        return $acceso;
    }

    protected static function getListarRol() {
        $rol_asg = Rol::where('acceso.roles.estado', 'A')
                        //->join('users as u', 'u.id', '=', 'acceso.roles.usr_id')
                ->get();
        return $rol_asg;
    }

    protected static function getListarOpcion() {
        $opcion = Opcion::join('acceso.grupos as grp', 'grp.id', '=', 'acceso.opciones.grp_id')
                ->select('grp.grupo', 'acceso.opciones.contenido', 'acceso.opciones.id', 'acceso.opciones.usr_id',
                        'acceso.opciones.opcion')
                ->where('acceso.opciones.estado', 'A')
                ->whereNotIn('acceso.opciones.id', function ($q) {
                    $q->select('acceso.accesos.opc_id')
                    ->from('acceso.accesos')
                    ->where('acceso.accesos.rls_id', '1')
                    ->where('acceso.accesos.estado', 'A');
                })->OrderBy('acceso.opciones.id', 'ASC')
                ->get();
        return $opcion;
    }

    protected static function getListarOpcionParam($vb) {
        $opciones = \DB::select('select * from acceso.proc_lista_op(?)', array($vb));
        return $opciones;
    }

    protected static function getListarAccesoParam($vb) {
        $acceso = \DB::select('select * from acceso.proc_lista_ac(?)', array($vb));
        return $acceso;
    }

}
