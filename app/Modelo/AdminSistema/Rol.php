<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model {

    protected $table = 'acceso.roles';
    protected $fillable = ['id', 'rol', 'registrado', 'modificado', 'usuario_reg', 'usuario_mod','estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Usuario() {
        return HasMany('App\Rol');
    }

    protected static function getListar() {
        $rol = \DB::select("SELECT 
                r.id, r.rol, to_char(r.modificado, 'DD/MM/YYYY HH24:MI') as modificado, u.usuario
                FROM
                acceso.roles r
                JOIN users u ON u.id = r.usuario_mod
                WHERE
                r.estado = ?", array('A'));
        return $rol;
    }

    protected static function setBuscar($id) {
        $rol = Rol::where('id', $id)->first();
        return $rol;
    }

    protected static function getDestroy($id) {
        $rol = Rol::where('id', $id)->update(['estado' => 'B']);
        return $rol;
    }

    protected static function getRolUser($vb) {
        $rol = \DB::select('select * from acceso.proc_lista_ra(?)', array($vb));
        return $rol;
    }

    protected static function getRolUserCreate($row) {
        $rol = Rol::select('accesoi.roles.id', 'acceso.roles.rol')
                        ->where('acceso.roles.estado', 'A')
                        ->whereNotIn('acceso.roles.id', function ($q) use ($row) {
                            $q->select('acceso.usuarios_roles.rls_id')
                            ->from('acceso.usuarios_roles')
                            ->where('acceso.usuarios_roles.usr_id', $row[0])
                            ->where('acceso.usuarios_roles.estado', 'A');
                        })->get();
        return $rol;
    }

    public function sel_consulta() {
        $rol = Rol::select('acceso.roles.id', 'acceso.roles.rol')
                        ->where('acceso.roles.estado', 'A')
                        ->whereNotIn('acceso.roles.id', function ($q) use ($arr_user) {
                            $q->select('acceso.usuarios_roles.rls_id')
                            ->from('acceso.usuarios_roles')
                            ->where('acceso.usuarios_roles.usr_id', $arr_user[0])
                            ->where('acceso.usuarios_roles.estado', 'A');
                        })->get();
        return $rol;
    }

}
