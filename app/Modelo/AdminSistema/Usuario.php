<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model {

    protected $table = 'users';
    protected $fillable = ['id', 'usuario', 'id_persona', 'id_rol', 'estado', 'password', 'registrado', 'modificado'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function getListar() {
        $usr = \DB::select("SELECT u.id, (p.nombres ||' '||p.paterno ||' '||p.materno) as nombres, u.usuario as usuario, to_char(u.modificado,'DD/MM/YYYY hh12:mi am') as modificado, (SELECT usuario FROM users WHERE id = u.usuario_mod) as usuario_mod  FROM users u JOIN personas p ON p.id = u.id_persona WHERE u.estado != ?",array('B'));
        return $usr;
    }

    protected static function setBuscar($id) {
        $respuesta = \DB::select("SELECT (p.nombres ||' '||p.paterno ||' '||p.materno) as nombre, u.usuario as nombre_usuario, u.id  FROM users u JOIN personas p ON p.id = u.id_persona WHERE u.id = ?", array($id));
        return $respuesta;
    }

    protected static function setUsuarioduplicado($nombre) {
        $respuesta = Usuario::where('usr_usuario', $nombre)->where('usr_estado', 'A')->count();
        return $respuesta;
    }

    protected static function getEstado() {
        $usuario = Usuario::where('users.estado', 'A')
                ->get();
        return $usuario;
    }

    protected static function setDestroy($id) {
        $usuario = Usuario::where('id', $id)
                ->delete();
        return $usuario;
    }

    protected static function actualizarUsuario($id, $usuario, $id_usuario) {
        $usuario = Usuario::where('id', $id)
                ->update(['usuario' => $usuario, 'usuario_mod' => $id_usuario, 'modificado' => date("Y-m-d H:i:s")]);
        return $usuario;
    }

}
