<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;

class Opcion extends Model {

    protected $table = 'acceso.opciones';
    protected $fillable = ['id',
        'grp_id',
        'opcion',
        'contenido',
        'adicional',
        'orden',
        'imagen',
        'registrado',
        'modificado',
        'usr_id',
        'estado'];
    public $timestamps = false;
    protected $primaryKey = 'id';

    protected static function getListar() {
        $opcion = \DB::select('SELECT o.id,g.grupo,o.grp_id,o.opcion,o.contenido,u.usuario
                                    FROM acceso.opciones o 
                                    JOIN acceso.grupos g ON o.grp_id = g.id
                                    JOIN users u ON o.usr_id = u.id
                                    WHERE g.estado = ? AND o.estado = ?
                                    ORDER BY g.id DESC', array('A', 'A'));
        return $opcion;
    }

    protected static function setBuscar($id) {
        $opcion = Opcion::where('id', $id)->first();
        return $opcion;
    }
    
    protected static function getDestroy($id)
    {
        $opcion = Opcion::where('id', $id)->update(['estado' => 'B']);
        return $opcion;
    }

}
