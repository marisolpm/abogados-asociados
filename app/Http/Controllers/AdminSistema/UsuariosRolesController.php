<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\AdminSistema\Usuario;
use App\Modelo\AdminSistema\Rol;
use App\Modelo\AdminSistema\RolUsuario;
use Session;
use Yajra\Datatables\Datatables;
use Auth;

class UsuariosRolesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'usuariosRoles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.sistema.rolusuario.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function listaU() {
        $rolUser = Usuario::getEstado();
        return Datatables::of($rolUser)
                        ->addColumn('acciones', function ($rolUser) {
                            return "<a class='btn btn-default' style='background:#57BCAA' onclick='listar(" . $rolUser->id . ")'><i class='fa fa-eye'></i></a>";
                        })->rawColumns(['acciones'])
                        ->make(true);
    }

    public function listaR($id_r) {
        $rol = Rol::getRolUser($id_r);
        return Datatables::of($rol)
                        ->addColumn('rolasignado', function ($rol) {
                            return '
                    <input type="checkbox" name="rolasignado[]" id="' . $rol->usrls_id . '" value="' . $rol->usrls_id . '">
                    ';
                        })->rawColumns(['rolasignado'])
                        ->make(true);
    }

    public function listaRno($id) {
        $rolusuario = RolUsuario::getlistar($id);
        return Datatables::of($rolusuario)
                        ->addColumn('rolnoasignado', function ($rolusuario) {
                            return '
                    <input tabindex="1" type="checkbox" name="rolnoasignado[]" id="' . $rolusuario->rls_id . '" value="' . $rolusuario->rls_id . '">
                    ';
                        })->rawColumns(['rolnoasignado'])
                        ->make(true);
    }

    public function actualiza($val) {
        $ids = Auth::user()->id;
        $arr_user = $val;
        $arr_opc = $_POST['rolnoasignado'];
        for ($i = 0; $i < count($arr_opc); $i++) {
            RolUsuario::create([
                'usr_id' => $arr_user,
                'rls_id' => $arr_opc[$i],
                'usuarios_usr_id' => $ids,
            ]);
        }
        Session::flash('message', 'Asignación Correcta.');
    }

    public function actualiza1($val) {
        $ids = Auth::user()->id;
        $arr_user = $val;
        $arr_acc = $_POST['rolasignado'];
        for ($i = 0; $i < count($arr_acc); $i++) {
            RolUsuario::where('id', $arr_acc[$i])
                    ->update(['estado' => 'B', 'modificado' => date('Y-m-d H:i:s'), 'usuarios_usr_id' => $ids]);
        }
    }

}
