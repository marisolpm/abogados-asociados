<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\AdminSistema\Grupo;
use App\Modelo\AdminSistema\Acceso;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class AccesosController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'accesos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
       $rol    = Acceso::getListarRol();
       $opc    = Acceso::getListarOpcionParam(13);
       $acceso = Acceso::getListarAccesoParam(13);
        return view('admin.sistema.accesos.index',compact('rol', 'opc', 'acceso'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $opc = Opcion::getListar();
        return Datatables::of($opc)->addColumn('acciones', function ($opc) {
                            return '<div class="btn-group">
                                        <a class="btn btn-primary"  style="margin-left:12px;" onClick="Mostrar(' . $opc->id . ');" data-toggle="tooltip"  title="Modificar" data-target="#myUpdate"><i class="fa fa-edit"></i> Editar</a>
                                        <a class="btn btn-danger" style="margin-left:12px;" onClick="Eliminar(' . $opc->id . ');" ><i class="fa fa-trash"></i> Eliminar</a>
                                    </div>';
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $ids = Auth::user()->id;

        $opcion = Opcion::create([
                    'opcion' => $request['opcion'],
                    'contenido' => $request['contenido'],
                    'grp_id' => $request['id'],
                    'adicional' => '1',
                    'orden' => 1,
                    'usr_id' => $ids,
                    'estado' => 'A',
        ]);
        return response()->json(["Mensaje" => "Se registro Correctamente"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $opcion = Opcion::setBuscar($id);
        return response()->json($opcion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $ids = Auth::user()->id;
        $opcion = Opcion::where('id', $id)->first();
        $opcion->grp_id = $request->grp_id;
        $opcion->opcion = $request->opcion;
        $opcion->contenido = $request->contenido;
        $opcion->save();
        return response()->json(["Mensaje" => "Se actualizo Correctamente"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $opcion = Opcion::getDestroy($id);
        return response()->json(["Mensaje" => "Se elimino Correctamente"]);
    }

}
