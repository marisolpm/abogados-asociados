<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Modelo\Admin\Persona;
use App\Modelo\AdminSistema\Usuario;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'usuarios';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $persona = Persona::OrderBy('id_persona', 'desc');
        $personassssss = Persona::selectRaw('CONCAT( paterno, materno, nombres) as full_name, id_persona')->orderBy('id_persona', 'DESC');
        return view('admin.sistema.usuario.index', compact('persona'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $usr = Usuario::getListar();
        return datatables::of($usr)->addColumn('acciones', function ($usuario) {
                            return '<div class="btn-group">
                                        <a class="btn btn-success"  onclick="resetearAcceso(' . $usuario->id . ')"><i class="fa fa-keyboard-o"></i> Resetear</a>
                                        <a class="btn btn-primary"  style="margin-left:12px;" onclick = "MostrarUsuario(' . $usuario->id . ')"><i class="fa fa-edit"></i> Editar</a>
                                        <a class="btn btn-danger" style="margin-left:20px;" onclick = "EliminarUsuario(' . $usuario->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                                    </div>';
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $usr = Usuario::setBuscar($id);
        return response()->json($usr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $contrasena = \DB::select('SELECT * FROM proc_cambiar_contraeña(?,?,?,?)', array($id, Auth::user()->id, '', 1));
        return response()->json($contrasena);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $usuario = Usuario::setDestroy($id);
        return response()->json(array('Mensaje' => 'Se actualizó de manera correcta'));
    }

    public function actualizarUsuario() {
        
        $usuario = Usuario::actualizarUsuario($_POST['id'], trim($_POST['usuario']), Auth::user()->id);
        return response()->json(array('Mensaje' => 'Se actualizó de manera correcta'));
    }

}
