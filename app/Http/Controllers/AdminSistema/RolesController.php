<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Modelo\AdminSistema\Rol;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'roles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rol = Rol::getListar();
        return view('admin.sistema.roles.index', compact('rol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $rol = Rol::getListar();
        $rol = new \Illuminate\Support\Collection($rol);
        return Datatables::of($rol)->addColumn('acciones', function ($rol) {
                            return '<div class="btn-group">
                                        <a class="btn btn-primary"  style="margin-left:12px;" onclick ="Mostrar(' . $rol->id . ')"><i class="fa fa-edit"></i> Editar</a>
                                        <a class="btn btn-danger" style="margin-left:20px;" onclick ="Eliminar(' . $rol->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                                    </div>';
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $ids = Auth::user()->id;

        Rol::create([
            'rol' => $request['rls_rol'],
            'usr_id' => $ids,
            'estado' => 'A',
        ]);

        return response()->json(["Mensaje" => "Se registro Correctamente"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $rol = Rol::setBuscar($id);
        return response()->json($rol);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $ids = Auth::user()->id;
        $rol = Rol::setBuscar($id);
        $rol->rol = $request->rls_rol;
        //$rol->modificado = date('Y-m-d H:i:s');
        $rol->usuario_mod = $ids;
        $rol->save();
        return response()->json(['mensaje' => 'Se actualizo el rol']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $ids = Auth::user()->id;
        $rol = Rol::getDestroy($id);
        return response()->json(["Mensaje" => "Se registro Correctamente"]);
    }

}
