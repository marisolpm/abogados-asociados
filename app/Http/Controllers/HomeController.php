<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $_SESSION['grupoController'] = '';
        $_SESSION['sGrupoController'] = '';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thejson=null;
        $citas = \DB::select('SELECT * FROM citas WHERE id_empleado = ?;',array( Auth::user()->id));
        foreach($citas as $event){
	$thejson[] = array("title"=>$event->titulo."\n".$event->mensaje,"description"=>$event->mensaje,"url"=>"./?view=editreservation&id=".$event->id,"start"=>$event->fecha_hora_cita);
}
        return view('welcome',compact('thejson'));
    }
}
