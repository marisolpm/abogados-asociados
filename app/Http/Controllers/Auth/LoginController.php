<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        date_default_timezone_set("America/La_Paz");
        $this->middleware('guest')->except('logout');
        $_SESSION['grupoController'] = '';
        $_SESSION['sGrupoController'] = '';
    }

    public function resetearContrasenaMail() {
        $contrasena = \DB::select('SELECT * FROM proc_cambiar_contraeña(?,?,?,?)', array(1, 1, $_POST['mail'], 3));
        $destinoCC = ',';
        $url_upload = '';
        $data = array('clienteN' => $clienteN, "abogadoN" => $abogadoN, 'asunto' => $asunto, "mensaje" => $mensaje);
        $sendMail = Mail::send('email.mail', $data, function ($message) use ($destinoArray, $destinoCC, $url_upload, $asunto) {
                    $message->to($destinoArray)->subject($asunto);
                    $message->from('asociadosmedina07@gmail.com', 'Medina & Asociados');
//                    $destinoCCM = explode(',', $destinoCC);
//                    foreach ($destinoCCM as $dCC) {
//                        $message->addCC($dCC);
//                    }
//                    $message->attach($url_upload);
                });

        $respuesta = array("retorno_codigo" => $asunto, "err_mensaje" => "SE REENVIÓ CORRECTAMENTE");

    }

    public function resetearContrasena() {
        $contrasena = \DB::select('SELECT * FROM proc_cambiar_contraeña(?,?,?,?)', array(1, 1, $_POST['mail'], 3));
        return response()->json($contrasena);
    }

}
