<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Expediente;
use App\Modelo\Admin\ExpedienteDigitalizado;
use App\Modelo\Admin\AbogadoExpediente;
use Yajra\Datatables\Datatables;
use Alimranahmed\LaraOCR\Services\OcrAbstract;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\Style\Language;

class ExpedientesDigitalizadosController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Expedientes';
        $_SESSION['sGrupoController'] = 'digitalizacion';
        $_SESSION['textoCompleto'] = '';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $clientes = Persona::ListaPersonas(-1, 2);
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.expedientes_digitalizados.index', compact('clientes', 'empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if (empty($_POST['textoExtraido']))
            $_POST['textoExtraido'] = '';
        $numero_documento = $_POST['numero'];
        $texto_documento = $_POST['textoExtraido'];
        $tipo_documento = $_POST['tipo'];
        $materia_documento = $_POST['materia'];
        $cliente = $_POST['id_cliente'];
        $id_expediente = $_POST['id_expediente'];
        $url_subida = '';

        if (!empty($_FILES['archivo']['tmp_name'])) {
            $nombre = '_' . date('YmdHis') . '.pdf';
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/expediente/digitalizados/';
            $url_subida = '/expediente/digitalizados/' . basename($_FILES['archivo']['name']);
            $fichero_subido = $dir_subida . basename($_FILES['archivo']['name']);
            move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);
        }

        $expediente = \DB::select('SELECT * FROM proc_expediente_digitalizado_ins_upd(?,?,?,?,?,?,?,?,?);',
                        array(
                            1,
                            $id_expediente,
                            $numero_documento,
                            $texto_documento,
                            $tipo_documento,
                            $materia_documento,
                            $url_subida,
                            Auth::user()->id,
                            $cliente
        ));
        return response()->json($expediente);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if (empty($request->textoExtraido))
            $request->textoExtraido = '';

        $url_subida = $request->url;

        $nombre = "DIG_" . $id . ".docx";

        $expedientes = ExpedienteDigitalizado::ListaExpedientes($id, -1);
        if ($expedientes[0]->o_url_documento != '') {
            unlink($_SERVER['DOCUMENT_ROOT'] . '/expediente/repositorio/' . $nombre);
        }
        $respuesta = ExpedienteDigitalizado::ActualizarExpediente(
                        3,
                        '',
                        '',
                        addslashes($request->textoExtraido),
                        0,
                        0,
                        $url_subida,
                        0,
                        $id,
                        "/expediente/repositorio/" . $nombre
        );

        $expedientes = ExpedienteDigitalizado::ListaExpedientes($id, -1);
        //dd(preg_replace('/[0-9\@\.\;\" "]+/', '', $expedientes[0]->o_texto_expediente));
        $separador = "\r\n"; // Usar salto de línea
        $separada = explode($separador, $expedientes[0]->o_texto_expediente);

        $documento = new \PhpOffice\PhpWord\PhpWord();
        $propiedades = $documento->getDocInfo();

        $documento->getCompatibility()->setOoxmlVersion(15);

        $documento->getSettings()->setThemeFontLang(new Language("ES-MX"));

        $seccion = $documento->addSection();
        $textoExtraido = "";
        foreach ($separada as $texto) {
            $seccion->addText(htmlspecialchars($texto));
        }



        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($documento, "Word2007");

        $objWriter->save("expediente/repositorio/" . $nombre);

        $collection = new \Illuminate\Support\Collection($respuesta);
        return response()->json($collection);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
    }

    public function ExtraeTexto() {


        if (isset($_FILES['archivo']) && $_FILES['archivo']['tmp_name'] != '') {
            set_time_limit(3600);
            $imagen = $_FILES["archivo"];
            $imagePath = $imagen["tmp_name"];
            $destino = $_SERVER['DOCUMENT_ROOT'] . '/expediente/digitalizados/digitalizado.jpg';
            $leerTexto = $_SERVER['DOCUMENT_ROOT'] . '/expediente/digitalizados/digitalizado';
            shell_exec('magick convert -density 300 -trim ' . $imagePath . ' -quality 20 ' . $destino);
            $_SESSION['textoCompleto'] = '';
            $paginas = getNumPagesInPDF($imagePath);
            
            $query = \DB::select('UPDATE expedientes_proceso_digitalizar SET paginas_ejecutado = 0, paginas_total = ' . $paginas . ', registrado = NOW() WHERE id = 1');

            if (!empty($_FILES['archivo']['tmp_name'])) {

                $nombre = 'ESC_' . $_POST['id'] . '.pdf';
                $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/expediente/repositorio/';
                $url_subida = '/expediente/repositorio/' . $nombre;
                $fichero_subido = $dir_subida . $nombre;
                move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);
            }

            //$textoExtraido = $paginas;
            $texto = array('texto' => $_SESSION['textoCompleto'], 'url' => $url_subida);

            // dd(\OCR::scan($imagePath));
            return response()->json($texto);
        }
    }

    public function ProcesoExtraeTexto() {
        $destino = $_SERVER['DOCUMENT_ROOT'] . '/expediente/digitalizados/digitalizado.jpg';
        $leerTexto = $_SERVER['DOCUMENT_ROOT'] . '/expediente/digitalizados/digitalizado';
        $paginas_total = \DB::select('SELECT paginas_total FROM expedientes_proceso_digitalizar WHERE id = 1;');
        //dd($paginas_total[0]->paginas_total);
        $paginas_ejecutado = \DB::select('SELECT paginas_ejecutado FROM expedientes_proceso_digitalizar WHERE id = 1;');
        if ($paginas_ejecutado[0]->paginas_ejecutado <= ($paginas_total[0]->paginas_total - 1) && ($paginas_total[0]->paginas_total - 1)!= 0) {
            
            
                $textoExtraido = \OCR::scan($leerTexto . '-' . $paginas_ejecutado[0]->paginas_ejecutado . '.jpg');
                $_SESSION['textoCompleto'] = $_SESSION['textoCompleto'] . ' ' . $textoExtraido;
                unlink($leerTexto . '-' . $paginas_ejecutado[0]->paginas_ejecutado . '.jpg');
            } else {
                
                $_SESSION['textoCompleto'] = \OCR::scan($destino);
                unlink($destino);
            }
            
            $query = \DB::select('UPDATE expedientes_proceso_digitalizar SET paginas_ejecutado = paginas_ejecutado + 1 WHERE id = 1');

            $percentage = round((($paginas_ejecutado[0]->paginas_ejecutado +1) * 100) / $paginas_total[0]->paginas_total, 2);
            
//            $date_add = new DateTime($row_process['date_add']);
//            $date_upd = new DateTime($row_process['date_upd']);
//            $diff = $date_add->diff($date_upd);
//
//            $execute_time = '';
//
//            if ($diff->days > 0) {
//                $execute_time .= $diff->days . ' dias';
//            }
//            if ($diff->h > 0) {
//                $execute_time .= ' ' . $diff->h . ' horas';
//            }
//            if ($diff->i > 0) {
//                $execute_time .= ' ' . $diff->i . ' minutos';
//            }
//
//            if ($diff->s > 1) {
//                $execute_time .= ' ' . $diff->s . ' segundos';
//            } else {
//                $execute_time .= ' 1 segundo';
//            }

            $query = \DB::select('UPDATE expedientes_proceso_digitalizar SET porcentage = ' . $percentage . ' WHERE id = 1');

            $row = array(
                'executed' => $paginas_ejecutado[0]->paginas_ejecutado + 1,
                'total' => $paginas_total[0]->paginas_total,
                'percentage' => round($percentage, 0),
                'execute_time' => '',
                'texto' => $_SESSION['textoCompleto']
            );
            
            return response()->json($row);
//            if($paginas_ejecutado + 1 == $paginas_total ){
//                $texto = array('texto' => $_SESSION['textoCompleto'], 'url' => $url_subida);
//
//            // dd(\OCR::scan($imagePath));
//            return response()->json($texto);
//            }
       
      
    }

    public function ListaExpedientes($id_expediente, $id_cliente) {
        $expedientes = ExpedienteDigitalizado::ListaExpedientes($id_expediente, $id_cliente);
        return Datatables::of($expedientes)
                        ->addColumn('o_escaneado', function ($expedientes) {
                            if ($expedientes->o_url_documento != '') {
                                return '
                               <a class="btn btn-default" title="Descargar PDF" target="_blank"  href="' . $expedientes->o_url_documento . '?=' . date("YmdHis") . '"><i class="fa fa-file-pdf-o"> Ver Escaneado</i></a>                                
                            ';
                            }
                        })
                        ->addColumn('o_accion', function ($expedientes) {
                            if ($expedientes->o_url_documento != '') {
                                return '
                                <a class="btn btn-info" data-toggle="tooltip" title="Digitalizar Expediente"  onclick="digitalizarExpediente(' . $expedientes->o_id . ')"><i class="fa fa-file"> Digitalizar</i></a>
                                <a class="btn btn-primary"  onclick="mostrarExpediente(' . $expedientes->o_id . ')"><i class="fa fa-edit"></i> Editar</a>
                                <a class="btn btn-default" title="Descargar"  href="' . $expedientes->o_url_word . '?=' . date("YmdHis") . '"><i class="fa fa-file-word-o"> Ver Word</i></a>
                                <a class="btn btn-warning" data-toggle="tooltip" title="Subir Word Modificado"  onclick="SubirDocumento(' . $expedientes->o_id . ')"><i class="fa fa-save"> Subir Word Modificado</i></a>
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar expediente"  onclick="detalleProceso(' . $expedientes->o_id . ')"><i class="fa fa-trash"> Eliminar</i></a>
                    ';
                            } else {
                                return '
                                <a class="btn btn-info" data-toggle="tooltip" title="Digitalizar Expediente"  onclick="digitalizarExpediente(' . $expedientes->o_id . ')"><i class="fa fa-file"> Digitalizar</i></a>
                                <a class="btn btn-primary"  onclick="mostrarExpediente(' . $expedientes->o_id . ')"><i class="fa fa-edit"></i> Editar</a>
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar expediente"  onclick="detalleProceso(' . $expedientes->o_id . ')"><i class="fa fa-trash"> Eliminar</i></a>
                    ';
                            }
                        })->rawColumns(['o_escaneado', 'o_accion'])
                        ->make(true);
    }

    public function verDigitalizado($id) {

        $expedientes = ExpedienteDigitalizado::ListaExpedientes($id, -1);

        header("Content-Disposition: attachment; filename=" . $expedientes[0]->o_url_word . "");
    }

    public function subirWord() {
        if (isset($_FILES['respaldo_doc']) && $_FILES['respaldo_doc']['tmp_name'] != '') {
            $extension = '.docx';
            $id_expediente = $_POST['id_expediente'];
            $nombre_documento = 'DIG_' . $id_expediente . $extension;
            unlink($_SERVER['DOCUMENT_ROOT'] . '/expediente/repositorio/' . $nombre_documento);
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/expediente/repositorio/';
            $fichero_subido = $dir_subida . $nombre_documento;
            if (copy($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                return response()->json(array("Mensaje" => "Subido correctamente"));
            }
        }
    }

}
