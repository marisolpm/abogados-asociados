<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Recibo;
use Yajra\Datatables\Datatables;
use App\User;
use Codedge\Fpdf\Fpdf\Fpdf;

class RecibosController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Seguimiento Económico';
        $_SESSION['sGrupoController'] = 'recibos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.recibos.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Recibo::RegistroRecibo(
                        $request->tipo_registro,
                        $request->id_recibo,
                        $request->cliente,
                        $request->cuenta,
                        $request->monto,
                        $request->glosa,
                        $request->numero_recibo
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function ListaRecibos($id_recibo, $id_cliente) {
        $recibos = Recibo::ListaRecibos($id_recibo, $id_cliente);
        return Datatables::of($recibos)
                        ->addColumn('o_recibo', function ($recibos) {
                            return '
                                <a class="btn btn-success" data-toggle="tooltip" title="Ver/Imprimir recibo"  onclick="respaldoRecibo(' . $recibos->o_id . ')"><i class="fa fa-print"></i></a>
                    ';
                        })
                        ->addColumn('o_accion', function ($recibos) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Editar recibo"  onclick="mostrarRecibo(' . $recibos->o_id . ')"><i class="fa fa-edit"></i></a>
                    ';
                        })->rawColumns(['o_recibo', 'o_accion'])
                        ->make(true);
    }

    public function registrarRespaldo() {
        //dd($_POST);
        if (isset($_FILES['respaldo_doc']) && $_FILES['respaldo_doc']['tmp_name'] != '') {
            $id_proceso = $_POST['id_proceso'];
            //$id_tipo_documento = $_POST['id_tipo_documento'];
            $nombre_documento = $_POST['nombre_documento'];
            $observacion_documento = $_POST['observacion_documento'];
            $nombre = '_' . date('YmdHis') . '.jpg';
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/proceso/respaldo/';
            $url_subida = '/proceso/respaldo/' . basename($_FILES['respaldo_doc']['name']);
            $fichero_subido = $dir_subida . basename($_FILES['respaldo_doc']['name']);

            if (move_uploaded_file($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                $proceso = \DB::select('SELECT * FROM proc_proceso_documento_ins_upd(?,?,?,?,?,?,?);', array(1, $id_proceso, 1, $nombre_documento, $observacion_documento, $url_subida, 1));
                return response()->json($proceso);
            }
        }
    }

    public function datosRecibo() {
        $recibo = Recibo::datosRecibo();
        return response()->json($recibo);
    }

    public function imprimirRecibo($id_recibo) {
        $recibos = Recibo::ListaRecibos($id_recibo, -1);
        $textypos = 5;
        $pdf = new FPDF();
        $pdf->AddPage();

        cabecera("COPIA EMPRESA", $textypos, $pdf, $recibos, 0, 0);
        cuerpo($textypos, $pdf, $recibos, 0, 0);
        $pdf->Line(10, 40 + 85, 200, 40 + 85);
        cabecera("COPIA CLIENTE", $textypos, $pdf, $recibos, 0, 125);
        cuerpo($textypos, $pdf, $recibos, 0, 125);
        $pdf->output();
        exit;
    }

}
