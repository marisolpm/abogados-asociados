<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Plantilla;
use App\Modelo\Admin\Persona;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;

class PlantillasController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Plantillas e Integración';
        $_SESSION['sGrupoController'] = 'plantillas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.plantilla.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if (isset($request->archivo) && $request->archivo != '') {

            $nombre_documento = str_replace(' ', '_', strtoupper($request->plantilla));
            $nombre = $nombre_documento . '.doc';
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/plantilla/';
            $url_subida = '/plantilla/' . $nombre;
            $fichero_subido = $dir_subida . $nombre;

            if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
                $respuesta = Plantilla::RegistroPlantilla(
                                strtoupper($request->plantilla),
                                $url_subida
                );
                return response()->json($respuesta);
            } else {
                return response()->json(array('Mensaje' => 'No se subio la plantilla'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $plantilla = Plantilla::ListaPlantillas($id, -1, -1);
        //dd($plantilla);
        header("Content-Disposition: attachment; filename=" . $_SERVER['DOCUMENT_ROOT'] . $plantilla[0]->o_url_plantilla . "");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $plantilla = Plantilla::find($id);
        unlink($_SERVER['DOCUMENT_ROOT'] . $plantilla->url);
        $plantilla->delete();
        if ($plantilla) {

            $respuesta = array("retorno_codigo" => $id, "err_mensaje" => "SE ELIMINÓ CORRECTAMENTE");
            return response()->json($respuesta);
        }
    }

    public function ListaPlantillas($id_plantilla, $id_usuario, $estado) {
        $plantilla = Plantilla::ListaPlantillas($id_plantilla, $id_usuario, $estado);
        return Datatables::of($plantilla)
                        ->addColumn('o_enlace', function ($plantilla) {
                            return '
                                <a class="btn btn-default" title="Descargar"  href="' . $plantilla->o_url_plantilla . '"><i class="fa fa-file-word-o"> ' . $plantilla->o_url_plantilla . '</i></a>';
                        })
                        ->addColumn('o_accion', function ($plantilla) {
                            return '
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar plantilla"  onclick="eliminarPlantilla(' . $plantilla->o_id . ')"><i class="fa fa-trash-o"></i></a>';
                        })->rawColumns(['o_enlace', 'o_accion'])
                        ->make(true);
    }

}
