<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Proceso;
use Yajra\Datatables\Datatables;
use App\User;

class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Dashboard';
        $_SESSION['sGrupoController'] = '/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $reportes = \DB::select('SELECT * FROM proc_reporte();');
       return view('welcome', compact('reportes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Proceso::RegistroProceso(
                        $request->tipo_registro,
                        $request->cliente,
                        $request->expediente,
                        $request->posicion,
                        $request->contrario,
                        $request->nombre,
                        $request->descripcion,
                        $request->tipo,
                        $request->estado,
                        $request->involucrados,
                        $request->abogados,
                        '2000-01-01 00:00 '
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $respuesta = Proceso::ActualizarProceso(
                        $request->tipo_registro,
                        $request->cliente,
                        $request->expediente,
                        $request->posicion,
                        $request->contrario,
                        $request->nombre,
                        $request->descripcion,
                        $request->tipo,
                        $request->estado,
                        $request->involucrados,
                        $request->abogados,
                        '2000-01-01 00:00 ',
                        $id
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $respuesta = Proceso::EliminarProceso(3, $id);
        return response()->json($respuesta[0]);
    }

    public function ListaProcesos($id_proceso, $id_cliente) {
        $procesos = Proceso::ListaProcesos($id_proceso, $id_cliente);
        return Datatables::of($procesos)
                        ->addColumn('proceso', function ($procesos) {
                            return '<b>Proceso</b> ' . $procesos->o_proceso . '<br><b>Tipo</b> ' . $procesos->o_nombre_proceso;
                        })
                        ->addColumn('modificado', function ($procesos) {
                            return '<b>Fecha</b> ' . $procesos->o_modificado . '<br><b>Usuario</b> ' . $procesos->o_usuario_mod;
                        })
                        ->addColumn('o_accion', function ($procesos) {
                            return '
                                <a class="btn btn-success" data-toggle="tooltip" title="Mostrar documentos"  onclick="detalleProceso(' . $procesos->o_id . ')"><i class="fa fa-file"></i></a>
                                <a class="btn btn-primary" data-toggle="tooltip" title="Editar proceso"  onclick="mostrarProceso(' . $procesos->o_id . ')"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar proceso"  onclick="eliminarProceso(' . $procesos->o_id . ')"><i class="fa fa-trash"></i></a>
                                <a class="btn btn-info" data-toggle="tooltip" title="Archivar proceso"  onclick="archivarProceso(' . $procesos->o_id . ')"><i class="fa fa-folder"></i></a>
                    ';
                        })->rawColumns(['proceso','modificado', 'o_accion'])
                        ->make(true);
    }

    public function registrarRespaldo() {
        //dd($_POST);
        if (isset($_FILES['respaldo_doc']) && $_FILES['respaldo_doc']['tmp_name'] != '') {
            $extension = '.' . explode('.', $_FILES['respaldo_doc']['name'])[1];
            $id_proceso = $_POST['id_proceso'];
            //$id_tipo_documento = $_POST['id_tipo_documento'];
            $nombre_documento = $_POST['nombre_documento'];
            $observacion_documento = $_POST['observacion_documento'];
            $nombre = $id_proceso . '_' . date('YmdHis') . $extension;
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/proceso/respaldo/';
            $url_subida = '/proceso/respaldo/' . $nombre;
            $fichero_subido = $dir_subida . $nombre;

            if (move_uploaded_file($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                $proceso = \DB::select('SELECT * FROM proc_proceso_documento_ins_upd(?,?,?,?,?,?,?);', array(1, $id_proceso, 1, $nombre_documento, $observacion_documento, $url_subida, 1));
                return response()->json($proceso[0]);
            }
        }
    }

    public function ListaProcesosDocumentos($id_proceso) {
        $procesos = Proceso::ListaProcesosDocumentos($id_proceso);
        return Datatables::of($procesos)
                        ->addColumn('o_documentos', function ($procesos) {
                            return '
                                <a class="btn btn-success" data-toggle="tooltip" title="Ver documento"  onclick="detalleProceso(' . $procesos->o_id . ')"><i class="fa fa-file"></i></a>
                    ';
                        })
                        ->addColumn('o_accion', function ($procesos) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Eliminar documento"  onclick="mostrarProceso(' . $procesos->o_id . ')"><i class="fa fa-trash"></i></a>
                    ';
                        })->rawColumns(['o_documentos', 'o_accion'])
                        ->make(true);
    }

}
