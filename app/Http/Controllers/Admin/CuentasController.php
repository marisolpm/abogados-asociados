<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Cuenta;
use Yajra\Datatables\Datatables;
use App\User;

class CuentasController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Seguimiento Económico';
        $_SESSION['sGrupoController'] = 'cuentas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.cuentas.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Cuenta::RegistroCuenta(
                        $request->tipo_registro,
                        $request->id_cuenta,
                        $request->cliente,
                        $request->tipo_cuenta,
                        $request->procesos,
                        $request->descripcion,
                        $request->numero_cuenta
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $cuenta = Cuenta::find($id)->delete();
        if ($cuenta) {
            return response()->json(array('err_codigo'=>$id,'err_mensaje'=> 'Se eliminó correctamente' ));
        }
    }

    public function ListaCuentas($id_cuenta, $id_cliente) {
        $cuentas = Cuenta::ListaCuentas($id_cuenta, $id_cliente);
        return Datatables::of($cuentas)
                        ->addColumn('o_accion', function ($cuentas) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Editar cuenta"  onclick="mostrarCuenta(' . $cuentas->o_id . ')"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar cuenta"  onclick="eliminarCuenta(' . $cuentas->o_id . ')"><i class="fa fa-trash"></i></a>
                    ';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

    public function registrarRespaldo() {
        //dd($_POST);
        if (isset($_FILES['respaldo_doc']) && $_FILES['respaldo_doc']['tmp_name'] != '') {
            $id_proceso = $_POST['id_proceso'];
            //$id_tipo_documento = $_POST['id_tipo_documento'];
            $nombre_documento = $_POST['nombre_documento'];
            $observacion_documento = $_POST['observacion_documento'];
            $nombre = '_' . date('YmdHis') . '.jpg';
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/proceso/respaldo/';
            $url_subida = '/proceso/respaldo/' . basename($_FILES['respaldo_doc']['name']);
            $fichero_subido = $dir_subida . basename($_FILES['respaldo_doc']['name']);

            if (move_uploaded_file($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                $proceso = \DB::select('SELECT * FROM proc_proceso_documento_ins_upd(?,?,?,?,?,?,?);', array(1, $id_proceso, 1, $nombre_documento, $observacion_documento, $url_subida, 1));
                return response()->json($proceso);
            }
        }
    }

    public function datosCuenta() {
        $cuenta = Cuenta::datosCuenta();
        return response()->json($cuenta);
    }

}
