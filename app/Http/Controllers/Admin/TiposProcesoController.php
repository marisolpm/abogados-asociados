<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\TiposProceso;
use Yajra\Datatables\Datatables;
use App\User;

class TiposProcesoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Gestión Procesal';
        $_SESSION['sGrupoController'] = 'procesos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.procesos.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Proceso::RegistroProceso(
                        $request->tipo_registro,
                        $request->cliente,
                        $request->posicion,
                        $request->contrario,
                        $request->nombre,
                        $request->descripcion,
                        $request->tipo,
                        $request->estado,
                        $request->involucrados,
                        $request->abogados,
                        '2000-01-01 00:00 '
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $respuesta = Proceso::ActualizarProceso(
                        $request->tipo_registro,
                        $request->cliente,
                        $request->posicion,
                        $request->contrario,
                        $request->nombre,
                        $request->descripcion,
                        $request->tipo,
                        $request->estado,
                        $request->involucrados,
                        $request->abogados,
                        '2000-01-01 00:00 ',
                        $id
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function ListaTiposProceso() {
        $tiposProceso = TiposProceso::ListaTiposProceso();
        return Datatables::of($tiposProceso)
                        ->addColumn('o_accion', function ($tiposProceso) {
                            return '
                                <a class="btn btn-success" data-toggle="tooltip" title="Mostrar documentos"  onclick="detalleProceso(' . $tiposProceso->o_id . ')"><i class="fa fa-file"></i></a>
                                <a class="btn btn-primary" data-toggle="tooltip" title="Editar proceso"  onclick="mostrarProceso(' . $tiposProceso->o_id . ')"><i class="fa fa-edit"></i></a>
                    ';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

}
