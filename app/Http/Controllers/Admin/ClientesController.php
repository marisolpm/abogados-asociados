<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class ClientesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Gestión Personas';
        $_SESSION['sGrupoController'] = 'clientes';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.clientes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $departamentos = Persona::ListaDepartamentos(-1);
        return view('admin.clientes.crear', compact('departamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $respuesta = Persona::RegistroPersonas(
                        2,
                        '',
                        $request->nombre,
                        $request->paterno,
                        $request->materno,
                        $request->ci,
                        $request->ci_expedido,
                        $request->fecha_nacimiento,
                        $request->email,
                        $request->contacto,
                        $request->direccion,
                        '',
                        0
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show($id_cliente) {
        $clientes = Persona::ListaPersonas($id_cliente, 2);
        $departamentos = Persona::ListaDepartamentos(-1);
        return view('admin.clientes.ver', compact('clientes', 'departamentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id_cliente) {

        $clientes = Persona::ListaPersonas($id_cliente, 2);
        $departamentos = Persona::ListaDepartamentos(-1);
        return view('admin.clientes.editar', compact('clientes', 'departamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_cliente) {
        $respuesta = Persona::ActualizarPersonas(
                        2,
                        $id_cliente,
                        '',
                        $request->nombre,
                        $request->paterno,
                        $request->materno,
                        $request->ci,
                        $request->ci_expedido,
                        $request->fecha_nacimiento,
                        $request->email,
                        $request->contacto,
                        $request->direccion,
                        '',
                        0
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $clientes = Persona::EliminarPersonas($id);
        return response()->json($clientes);
    }
    
    public function ListaPersonas($id_persona, $id_tipo) {
        $clientes = Persona::ListaPersonas($id_persona, $id_tipo);
        return Datatables::of($clientes)
                        ->addColumn('o_accion', function ($clientes) {
                            return '
                                <a class="btn btn-default"  href="/clientes/' . $clientes->o_prs_id . '"><i class="fa fa-eye"></i> Ver</a>
                                <a class="btn btn-primary"  style="margin-left:5px;" href="/clientes/' . $clientes->o_prs_id . '/edit"><i class="fa fa-edit"></i> Editar</a>
                                <a class="btn btn-danger" style="margin-left:5px;" onclick="eliminarCliente(' . $clientes->o_prs_id . ')" ><i class="fa fa-trash"></i> Eliminar</a>
                    ';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

    public function ListaDepartamentos($id_departamento) {
        $departamentos = Persona::ListaDepartamentos($id_departamento);
        $collection = new \Illuminate\Support\Collection($departamentos);
        return response()->json($collection);
    }
    
    public function verificaCI() {
        
        $prs_ci = strtoupper($_POST['prs_ci']);
        $prs_ci = str_replace('LP', '', $prs_ci);
        if (!empty($prs_ci)) {
            $persona_ci = Persona::where('ci', '=', $prs_ci)->where('estado', '!=', 'C')->limit(1)->get();
            //dd($persona_ci);
            if ($persona_ci->count() > 0) {
                //return '<div class="alert alert-danger"><strong>C.I.: ' . $prs_ci . ' se halla registrado.</strong></div>';
                return response()->json($persona_ci->toArray());
            }
        }
    }

}
