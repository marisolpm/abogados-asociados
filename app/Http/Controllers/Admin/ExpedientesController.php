<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Expediente;
use App\Modelo\Admin\AbogadoExpediente;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class ExpedientesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Expedientes';
        $_SESSION['sGrupoController'] = 'expedientes';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $clientes = Persona::ListaPersonas(-1, 2);
        //$clientes = Persona::ListaPersonas($id_cliente, 2);
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.expedientes.index', compact('clientes', 'empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if (isset($request->respaldo_doc) && $request->respaldo_doc != '') {
            //$id_tipo_documento = $_POST['id_tipo_documento'];
            $nombre_documento = $request->codigo;
            $nombre = '_' . date('YmdHis') . '.jpg';
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/expediente/respaldo/';
            $url_subida = '/expediente/respaldo/' . $nombre_documento;
            $fichero_subido = $dir_subida . basename($_FILES['respaldo_doc']['name']);

            if (move_uploaded_file($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                $proceso = \DB::select('SELECT * FROM proc_proceso_documento_ins_upd(?,?,?,?,?,?,?);', array(1, $id_proceso, 1, $nombre_documento, $observacion_documento, $url_subida, Auth::user()->id));
                return response()->json($proceso);
            }
        }

        if (empty($request->observaciones)) {
            $request->observaciones = '';
        }
        $respuesta = Expediente::RegistroExpediente(
                        1,
                        $request->cliente,
                        $request->codigo,
                        $request->numero,
                        $request->demandante,
                        $request->demandado,
                        $request->materia,
                        $request->estado,
                        $request->contingencia,
                        $request->observaciones
        );
        $collection = new \Illuminate\Support\Collection($respuesta);
        return response()->json($collection[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_cliente) {
        $clientes = Persona::ListaPersonas($id_cliente, 2);
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.expedientes.ver', compact('clientes', 'empleados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (empty($request->observaciones)) {
            $request->observaciones = '';
        }

        $respuesta = Expediente::ActualizarExpediente(
                        2,
                        $request->cliente,
                        $request->codigo,
                        $request->numero,
                        $request->demandante,
                        $request->demandado,
                        $request->materia,
                        $request->estado,
                        $request->contingencia,
                        $request->observaciones,
                        $id
        );
        $collection = new \Illuminate\Support\Collection($respuesta);
        return response()->json($collection[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function ListaExpedientes($id_expediente, $id_cliente, $posicion) {
        $expedientes = Expediente::ListaExpedientes($id_expediente, $id_cliente, $posicion);
        return Datatables::of($expedientes)
                        ->addColumn('accion', function ($expedientes) {
                            return '
                                <a class="btn btn-warning"  onclick="seguimientoExpediente(' . $expedientes->o_id . ')"><i class="fa fa-eye"></i> Seguimiento</a>
                                <a class="btn btn-success"  onclick="detalleExpediente(' . $expedientes->o_id . ')"><i class="fa fa-users"></i> Abg. Asig.</a> 
                                <a class="btn btn-info" data-toggle="tooltip" title="Mostrar documentos"  onclick="detalleExpedienteDoc(' . $expedientes->o_id . ')"><i class="fa fa-file"> Doc.</i></a>
                                <a class="btn btn-primary"  onclick="mostrarExpediente(' . $expedientes->o_id . ')"><i class="fa fa-edit"></i> Editar</a>
                                
                    ';
                        })->rawColumns(['accion'])
                        ->make(true);
    }

    public function registrarRespaldo() {
        //dd($_POST);
        if (isset($_FILES['respaldo_doc']) && $_FILES['respaldo_doc']['tmp_name'] != '') {
            $extension = '.' . explode('.', $_FILES['respaldo_doc']['name'])[1];
            $id_expediente = $_POST['id_expediente'];
            //$id_tipo_documento = $_POST['id_tipo_documento'];
            $nombre_documento = $_POST['nombre_documento'];
            $observacion_documento = $_POST['observacion_documento'];
            $nombre = $id_expediente . '_' . date('YmdHis') . $extension;
            $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/expediente/respaldo/';
            $url_subida = '/expediente/respaldo/' . $nombre;
            $fichero_subido = $dir_subida . $nombre;

            if (move_uploaded_file($_FILES['respaldo_doc']['tmp_name'], $fichero_subido)) {
                $proceso = \DB::select('SELECT * FROM proc_expediente_documento_ins_upd(?,?,?,?,?,?,?);', array(1, $id_expediente, 1, $nombre_documento, $observacion_documento, $url_subida, 1));
                return response()->json($proceso[0]);
            }
        }
    }

    public function eliminarRespaldo($id_proc_doc) {

        $expediente = \DB::select('UPDATE expedientes_documentos SET usuario_mod = ?, estado = ? WHERE id = ?;', array(Auth::user()->id, 'B', $id_proc_doc));
        return response()->json(array('Mensaje' => 'Dado de Baja'));
    }

    public function ListaExpepdientesDocumentos($id_expediente, $id_exediente_doc) {
        $expediente = Expediente::ListaExpedientesDocumentos($id_expediente, $id_exediente_doc);
        return Datatables::of($expediente)
                        ->addColumn('o_documentos', function ($expediente) {
                            return '
                                <a class="btn btn-success" data-toggle="tooltip" title="Ver documento"  onclick="respaldoExpediente(' . $expediente->o_id . ')"><i class="fa fa-file"></i></a>
                    ';
                        })
                        ->addColumn('o_accion', function ($expediente) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Eliminar documento"  onclick="eliminarRespaldo(' . $expediente->o_id . ')"><i class="fa fa-trash"></i></a>
                    ';
                        })->rawColumns(['o_documentos', 'o_accion'])
                        ->make(true);
    }
    
    public function verificaEXP() {
        
        $codigo = strtoupper($_POST['codigo']);
        $codigo = trim($codigo);
        if (!empty($codigo)) {
            $expediente = Expediente::where('codigo_expediente', '=', $codigo)->where('estado', '!=', 'B')->limit(1)->get();
            if ($expediente->count() > 0) {
                //return '<div class="alert alert-danger"><strong>C.I.: ' . $prs_ci . ' se halla registrado.</strong></div>';
                return response()->json($expediente->toArray());
            }
        }
    }

}
