<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\User;
use Yajra\Datatables\Datatables;

class EmpleadosController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Gestión Personas';
        $_SESSION['sGrupoController'] = 'empleados';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $departamentos = Persona::ListaDepartamentos(-1);
        $areas = Persona::ListaTipoProceso();
        return view('admin.empleados.index', compact('departamentos', 'areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $departamentos = Persona::ListaDepartamentos(-1);
        $areas = Persona::ListaTipoProceso();
        return view('admin.empleados.crear', compact('departamentos', 'areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
       
        if(empty($request->matricula)) {
            $request->matricula = '';
        }
        
        if(empty($request->fecha_nacimiento)) {
            $request->fecha_nacimiento = '1900-01-01';
        }
        $respuesta = Persona::RegistroPersonas(
                1,
                '',
                $request->nombre,
                $request->paterno,
                $request->materno,
                $request->ci,
                $request->ci_expedido,
                $request->fecha_nacimiento,
                $request->email,
                $request->contacto,
                $request->direccion,
                $request->matricula,
                $request->area
                );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show($id_cliente) {
        $clientes = Persona::ListaPersonas($id_cliente, 1);

        return view('admin.empleados.ver', compact('clientes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id_cliente) {

        $clientes = Persona::ListaPersonas($id_cliente, 1);
        $departamentos = Persona::ListaDepartamentos(-1);
        $areas = Persona::ListaTipoProceso();
        return view('admin.empleados.editar', compact('clientes', 'departamentos', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_cliente) {
        if (empty($request->matricula)) {
            $request->matricula = '';
        }

        if (empty($request->fecha_nacimiento)) {
            $request->fecha_nacimiento = '1900-01-01';
        }
        $respuesta = Persona::ActualizarPersonas(
                        1,
                        $id_cliente,
                        '',
                        $request->nombre,
                        $request->paterno,
                        $request->materno,
                        $request->ci,
                        $request->ci_expedido,
                        $request->fecha_nacimiento,
                        $request->email,
                        $request->contacto,
                        $request->direccion,
                        $request->matricula,
                        $request->area
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $empleados = Persona::EliminarPersonas($id);
        return response()->json($empleados);
    }

    public function ListaPersonas($id_persona, $id_tipo) {
        $empleados = Persona::ListaPersonas($id_persona, $id_tipo);
        return Datatables::of($empleados)
                        ->addColumn('o_accion', function ($empleados) {
                            return '
                                <a class="btn btn-default"  href="/empleados/' . $empleados->o_prs_id . '"><i class="fa fa-eye"></i> Ver</a>
                                <a class="btn btn-primary"  style="margin-left:1px;" href="/empleados/' . $empleados->o_prs_id . '/edit"><i class="fa fa-edit"></i> Editar</a>
                                <a class="btn btn-danger" style="margin-left:1px;" onclick="eliminarEmpleado(' . $empleados->o_prs_id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                    ';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

}
