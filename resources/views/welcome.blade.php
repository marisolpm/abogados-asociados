@extends('layouts.partials.main')
@section('content')
<link href="assets/css/chosen.css" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Reporte</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> <?php echo 'dashboard'; ?></a></li>
        <li class="active">Reporte</li>
    </ol>
</section>

<section class="content">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @foreach($reportes as $reporte)
    <div class="col-md-3">
        <div class="{{$reporte->o_class}}">
            <div class="inner">
                <h3>{{$reporte->o_cantidad}}</h3>

                <p>{{$reporte->o_tipo}}</p>
            </div>
            <div class="small-box icon">
                <i class="{{$reporte->o_icon}}"></i>
            </div>
            <a href="{{$reporte->o_link}}" class="small-box-footer">
                Más detalle <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    @endforeach
</section>


<script src="assets/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/redactor.min.js"></script>
<script type="text/javascript">

</script>

<script>

</script>
@endsection