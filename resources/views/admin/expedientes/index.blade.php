
@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .chosen-container {
        width: 100% !important;
    }
    #progress_bar {
        margin: 10px 0;
        padding: 3px;
        border: 1px solid #000;
        font-size: 14px;
        clear: both;
        opacity: 0;
        -moz-transition: opacity 1s linear;
        -o-transition: opacity 1s linear;
        -webkit-transition: opacity 1s linear;
    }
    #progress_bar.loading {
        opacity: 1.0;
    }
    #progress_bar .percent {
        background-color: #99ccff;
        height: auto;
        width: 0;
    }

</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        <?php echo 'Expedientes' ?>
        <small><?php echo 'Ver' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/expedientes">Expedientes</a></li>
        <li class="active"><?php echo 'Ver' ?></li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        @include('admin.expedientes.partial.modalUploadFile')
        @include('admin.expedientes.partial.modalViewFile')
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Expedientes</span>

                    </a>
                </li>
                <li class="" id="tabSeguimiento">
                    <a data-toggle="tab" href="#seguimiento">
                        <i class="fa fa-table"></i> <span>Seguimiento Expediente</span>

                    </a>
                </li>
                <li class="" id="tabDetalle">
                    <a data-toggle="tab" href="#detalle">
                        <i class="fa fa-eye"></i> <span>Detalle Expediente (Abogados Asignados)</span>

                    </a>
                </li>
                <li class="" id="tabDocumento">
                    <a data-toggle="tab" href="#documentos">
                        <i class="fa fa-file"></i> <span>Detalle Expediente (Documentos)</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo/Editar Expediente</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body table-responsive">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped" id="lts_expedientes">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Expediente N°</th>
                                        <th>Demandante</th>
                                        <th>Demandado</th>
                                        <th>Estado</th>
                                        <th>Fecha Registro</th>
                                        <th>Usuario Registro</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="seguimiento">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Expediente</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 table-responsive">
                                <table class="table-bordered">
                                    <thead>
                                        <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoS"></td><th>Materia</th><td>&nbsp;</td><td id="materiaS"></td><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroS"></span></td><th>Usuario registro</th><td>&nbsp;</td><td ><i class="fa fa-user"></i> <span id="usuregS"></span></td></tr>
                                        <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteS"></td><th>Estado</th><td>&nbsp;</td><td id="estadoS"></td><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoS"></span></td><th>Usuario modificación</th><td>&nbsp;</td><td ><i class="fa fa-user"></i> <span id="usumodS"></span></td></tr>
                                        <tr><th>Contingencia</th><td>&nbsp;</td><td id="contingenciaS"></td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                    </thead>

                                    <thead>
                                        <tr><th>Demandante</th><td>&nbsp;</td><td id="demandanteS"></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                        <tr><th>Demandado</th><td>&nbsp;</td><td id="demandadoS"></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                    </thead>
                                </table>

                            </div>


                            <div class="col-md-12">
                                <h4>Estado del Procesos</h4>
                                <div class="table-responsive">
                                    <table class="table-bordered table-striped" style="width: 100%">
                                        <thead class="cf">
                                        <th>
                                        <th>Estado</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Final</th>
                                        <th>Registro</th>
                                        <th>Usuario Registro</th>

                                        </th>
                                        </thead>
                                        <tbody id="lts_proceso_seguimiento"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="tab-pane" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Expediente</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoE"></td></tr>
                                            <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Materia</th><td>&nbsp;</td><td id="materiaE"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoE"></td></tr>
                                            <tr><th>Contingencia</th><td>&nbsp;</td><td id="contingenciaE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroE"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoE"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Demandante</th><td>&nbsp;</td><td id="demandanteE"></td></tr>
                                            <tr><th>Demandado</th><td>&nbsp;</td><td id="demandadoE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <h4>Abogados asignados</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_abogados_asignados">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Paterno</th>
                                                <th>Materno</th>
                                                <th>Perfíl</th>
                                                <th>Asignación</th>
                                                <th>Desvinculación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Asignar un abogado al caso</h4>
                                <hr>
                                <h6><b>Nota:</b> Seleccione los abogados que estaran a cargo del caso. si desea puede retirarlo
                                    haciendo clic en el aspa "X" luego debe pulsar los cambios en el boton <b>"Asignar & Guardar"</b></h6>
                            </div>

                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="expediente" name="expediente" type="hidden" value=""/>
                                <input id="tipo_registro_abogado" name="tipo_registro_abogado" type="hidden" value="1"/>
                                <div class="col-md-8">
                                    <select name="abogados[]" id="abogado_chz"  class="chzn" multiple="multiple" >
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->o_prs_id}}">{{$empleado->o_prs_nombre_completo}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a  class="btn btn-primary" id="registrar_abogado_expediente"><i class="fa fa-plus-circle"></i> Asignar & Guardar</a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="documentos">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Documentos</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoEC"></td></tr>
                                            <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteEC"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Materia</th><td>&nbsp;</td><td id="materiaEC"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoEC"></td></tr>
                                            <tr><th>Contingencia</th><td>&nbsp;</td><td id="contingenciaEC"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroEC"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoEC"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Demandante</th><td>&nbsp;</td><td id="demandanteEC"></td></tr>
                                            <tr><th>Demandado</th><td>&nbsp;</td><td id="demandadoEC"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Documentos del Expediente</h4>
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-xs-12">
                                        <div class="btn-group pull-right">
                                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                                <input id="proceso" name="proceso" type="hidden" value=""/>
                                                <input id="tipo_registro_proceso" name="tipo_registro_proceso" type="hidden" value="1"/>

                                                <div class="col-md-12">
                                                    <a  class="btn btn-primary" onclick="SubirDocumento(1)"><i class="fa fa-plus-circle"></i> Subir Documento</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_expediente_documento">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre Documento</th>
                                                <th>Enlace</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Registro</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVO/EDITAR EXPEDIENTE</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-12" id="result-expediente"></div>
                            <form action="" method="POST" id="formularioExpediente" enctype='multipart/form-data'>
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="cliente" name="cliente" type="hidden" value="{{$clientes[0]->o_prs_id}}"/>    
                                <input id="expediente" name="expediente" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>    
                                <div class="col-md-2">
                                    <label for="codigo" style="clear:both;">Código de expediente</label>
                                    <input type="text" name="codigo" id="codigo" placeholder="Código" class="form-control" autocomplete="off">
                                </div>


                                <div class="col-md-2">
                                    <label for="numero" style="clear:both;">Expediente N°</label>
                                    <input type="text" name="numero" id="numero" placeholder="Expediente N°" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-3">
                                    <label for="demandante" style="clear:both;">Demandante</label>
                                    <select name="demandante"  id="demandante" class="form-control" style="width: 100%; height: 50px">

                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="materia">Materia</label>
                                    <select name="materia" id="materia" class="form-control">
                                        <option value="0">Seleccione materia</option>
                                        <option value="1">Judicial</option>
                                        <option value="2">Administrativo</option>
                                        <option value="3">Policial</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="demandado">Demandado</label>
                                    <select name="demandado" id="demandado" class="form-control" style="width: 100%;height: 60px">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="estado" style="clear:both;">Estado</label>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="0">Seleccione estado</option>
                                        <option value="1">Postulatoria</option>
                                        <option value="2">Probatoria</option>
                                        <option value="3">Expositiva</option>
                                        <option value="4">Consultiva</option>
                                        <option value="5">Juicio</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="contingencia">Contingencia</label>
                                    <select name="contingencia" id="contingencia" class="form-control">
                                        <option value="0">Seleccione contingencia</option>
                                        <option value="1">Remoto</option>
                                        <option value="2">Probable</option>
                                        <option value="3">Eventual</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <label for="observaciones" style="clear:both;">Observaciones/Comentario</label>
                                    <textarea name="observaciones" id="observaciones" class="form-control" placeholder="Observaciones/Comentario"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <a  class="btn btn-success"  id="registrar_expediente">REGISTRAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  


<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
                                        $('#codigo').on('keyup', function () {
                                        var codigo = $(this).val();
                                        verifica_exp(codigo);
                                        });
                                        function verifica_exp(codigo) {
                                        var dataString = 'codigo=' + codigo;
                                        var token = $("#token_registrar").val();
                                        $.ajax({
                                        type: "POST",
                                                headers: {'X-CSRF-TOKEN': token},
                                                url: "/expedienteVerificaEXP",
                                                data: dataString,
                                                success: function (data) {
                                                console.log(data);
                                                if (data.length !== 0) {
                                               $('#numero').val(data[0].nro_expediente);
//                        $('#paterno').attr('disabled', 'disabled');
                                                $('#demandante').select2("val", data[0].demandante);
//                        $('#materno').attr('disabled', 'disabled');
                                                $('#demandado').select2("val", data[0].demandado);
//                        $('#materno').attr('disabled', 'disabled');
                                                $('#materia').val(data[0].materia);
//                        $('#c_procedencia').attr('disabled', 'disabled');
                                                $('#estado').val(data[0].estado_expediente);
//                        $('#fec_nacimiento').attr('disabled', 'disabled');
                                                $('#contingencia').val(data[0].contingencia);
//                        $('#direccion').attr('disabled', 'disabled');
                                                $('#observaciones').val(data[0].observaciones);
//                        $('#telefono').attr('disabled', 'disabled');
                                               $('#result-expediente').fadeIn(1000).html('<div class="alert alert-danger"><strong>Código de Expediente se halla registrado.</strong></div>');
                                                $('#registrar_expediente').attr('disabled', 'disabled');
                                                } else {
                                                $('#nombre').val('');
//                        $('#nombres').removeAttr('disabled');
                                                $('#paterno').val('');
//                        $('#paterno').removeAttr('disabled');
                                                $('#materno').val('');
//                        $('#materno').removeAttr('disabled');
                                                $('#ci_expedido').val(0);
//                        $('#c_procedencia').removeAttr('disabled');
                                                $('#fecha_nacimiento').val('');
//                        $('#fec_nacimiento').removeAttr('disabled');
                                                $('#direccion').val('');
//                        $('#direccion').removeAttr('disabled');
//                        $('#direccionaux').val('');
//                        $('#direccionaux').removeAttr('disabled');
                                                $('#contacto').val('');
//                        $('#telefono').removeAttr('disabled');
//                        $('#telefonoaux').val('');
//                        $('#telefonoaux').removeAttr('disabled');
//                        $('#celular').val('');
//                        $('#celular').removeAttr('disabled');
//                        $('#rutaPatio').val(0);
//                        $('#rutaPatio').removeAttr('disabled');
                                                $('#email').val('');
//                        $('#correo').removeAttr('disabled');
//                        $('#estadocivil').val(0);
//                        $('#estadocivil').removeAttr('disabled');
                                                $('#result-expediente').fadeIn(1000).html(data);
                                                $('#registrar_expediente').removeAttr('disabled');
                                                }



                                                }
                                                });
                                                }
                                                $(function () {

                                                $('.chzn').chosen();
                                                });
                                                var id_cliente = $('#cliente').val();
                                                function SubirDocumento(exp_id) {
                                                jQuery('#myUploadFile').modal('show');
                                                }

                                                function resetear() {
                                                document.getElementById('formularioExpediente').reset();
                                                $('#tipo_registro').val(1);
                                                }
                                                function detalleExpediente(id)
                                                {
                                                $('.nav-tabs a[href="#detalle"]').tab('show');
                                                var path = '/expedientesListado/' + id + '/-1/-1';
                                                $.get(path, function (res) {
                                                $('#expediente').val(id);
                                                document.getElementById("codigoE").innerHTML = res.data[0].o_id_expediente;
                                                document.getElementById("expedienteE").innerHTML = res.data[0].o_nro_expediente;
                                                document.getElementById("demandanteE").innerHTML = res.data[0].o_nombre_demandante;
                                                document.getElementById("demandadoE").innerHTML = res.data[0].o_nombre_demandado;
                                                document.getElementById("materiaE").innerHTML = res.data[0].o_materia;
                                                document.getElementById("estadoE").innerHTML = res.data[0].o_estado_expediente;
                                                document.getElementById("contingenciaE").innerHTML = res.data[0].o_contingencia;
                                                document.getElementById("registroE").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoE").innerHTML = res.data[0].o_modificado;
                                                document.getElementById("observacionesE").innerHTML = res.data[0].o_observaciones;
                                                });
                                                $('#lts_abogados_asignados').DataTable({
                                                destroy: true,
                                                        processing: true,
                                                        serverSide: true,
                                                        ajax: '/expedientesAbogadoListado/' + id + '/-1',
                                                        columns: [
                                                        {data: "o_nombre", orderable: false},
                                                        {data: "o_paterno"},
                                                        {data: "o_materno"},
                                                        {data: "o_perfil"},
                                                        {data: "o_asignacion"},
                                                        {data: "o_desvinculacion"},
                                                        {data: "o_accion"}
                                                        ],
                                                        "language": {
                                                        "url": "/lenguaje"
                                                        },
                                                        "lengthMenu": [[10, 25, 50, - 1], [10, 25, 50, "All"]],
                                                });
                                                }

                                                function detalleExpedienteDoc(id)
                                                {
                                                $('.nav-tabs a[href="#documentos"]').tab('show');
                                                var path = '/expedientesListado/' + id + '/-1/-1';
                                                $.get(path, function (res) {
                                                $('#expediente').val(id);
                                                $('#id_expediente').val(id);
                                                document.getElementById("codigoEC").innerHTML = res.data[0].o_id_expediente;
                                                document.getElementById("expedienteEC").innerHTML = res.data[0].o_nro_expediente;
                                                document.getElementById("demandanteEC").innerHTML = res.data[0].o_nombre_demandante;
                                                document.getElementById("demandadoEC").innerHTML = res.data[0].o_nombre_demandado;
                                                document.getElementById("materiaEC").innerHTML = res.data[0].o_materia;
                                                document.getElementById("estadoEC").innerHTML = res.data[0].o_estado_expediente;
                                                document.getElementById("contingenciaEC").innerHTML = res.data[0].o_contingencia;
                                                document.getElementById("registroEC").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoEC").innerHTML = res.data[0].o_modificado;
                                                document.getElementById("observacionesEC").innerHTML = res.data[0].o_observaciones;
                                                });
                                                $('#lts_expediente_documento').DataTable({
                                                destroy: true,
                                                        processing: true,
                                                        serverSide: true,
                                                        ajax: '/expedientesListadoDocumentos/' + id + '/-1',
                                                        columns: [
                                                        {data: "o_nombre_documento", orderable: false},
                                                        {data: "o_documentos"},
                                                        {data: "o_registrado"},
                                                        {data: "o_usuario_reg"},
                                                        {data: "o_accion"}
                                                        ],
                                                        "language": {
                                                        "url": "/lenguaje"
                                                        },
                                                        "lengthMenu": [[10, 25, 50, - 1], [10, 25, 50, "All"]],
                                                });
                                                }

                                                function seguimientoExpediente(id)
                                                {
                                                $('.nav-tabs a[href="#seguimiento"]').tab('show');
                                                $('#tipo_registro').val(2);
                                                var path = '/expedientesListado/' + id + '/-1/-1';
                                                $.get(path, function (res) {

                                                document.getElementById("codigoS").innerHTML = res.data[0].o_id_expediente;
                                                document.getElementById("expedienteS").innerHTML = res.data[0].o_nro_expediente;
                                                document.getElementById("demandanteS").innerHTML = res.data[0].o_nombre_demandante;
                                                document.getElementById("demandadoS").innerHTML = res.data[0].o_nombre_demandado;
                                                document.getElementById("materiaS").innerHTML = res.data[0].o_materia;
                                                document.getElementById("estadoS").innerHTML = res.data[0].o_estado_expediente;
                                                document.getElementById("contingenciaS").innerHTML = res.data[0].o_contingencia;
                                                document.getElementById("registroS").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoS").innerHTML = res.data[0].o_modificado;
                                                document.getElementById("usuregS").innerHTML = res.data[0].o_usuario_reg;
                                                document.getElementById("usumodS").innerHTML = res.data[0].o_usuario_mod;
                                                let respuesta = $('#lts_proceso_seguimiento');
                                                respuesta.find('tr').remove();
                                                console.log(res.data[0].o_expediente_seguimiento);
                                                if (res.data[0].o_expediente_seguimiento !== null) {
                                                let datos = JSON.parse(res.data[0].o_expediente_seguimiento.replace(/&quot;/g, '"'));
                                                var contador = 1;
                                                for (let item of datos) {
                                                respuesta.append('<tr>\n\
                                    <td width="2%"><center>' + contador + '</center></td>\n\
                                    <td width="15%">' + item.o_nombre_estado_proceso + '</td>\n\
                                    <td width="15%">' + item.o_fecha_inicio + '</td>\n\
                                    <td width="15%">' + item.o_fecha_final + '</td>\n\
                                    <td width="15%">' + item.o_registrado + '</td>\n\
                                    <td width="15%">' + item.o_usuario_reg + '</td>\n\
                                    </tr>');
                                                contador++;
                                                }

                                                } else {
                                                respuesta.append('<tr>\n\
                                    <td colspan = "6"><center>NO SE TIENE REGISTROS</center></td>\n\
                                 </tr>');
                                                }



                                                });
                                                }

                                                function mostrarExpediente(id)
                                                {
                                                $('.nav-tabs a[href="#nuevo"]').tab('show');
                                                $('#tipo_registro').val(2);
                                                var path = '/expedientesListado/' + id + '/-1/-1';
                                                $.get(path, function (res) {
                                                $('#expediente').val(id);
                                                $('#codigo').val(res.data[0].o_id_expediente);
                                                $('#numero').val(res.data[0].o_nro_expediente);
                                                $('#demandante').select2("val", res.data[0].o_demandante);
                                                $('#demandado').select2("val", res.data[0].o_demandado);
                                                $('#materia').val(res.data[0].o_id_materia);
                                                $('#estado').val(res.data[0].o_id_estado_expediente);
                                                $('#contingencia').val(res.data[0].o_id_contingencia);
                                                $('#observaciones').val(res.data[0].o_observaciones);
                                                });
                                                }

                                                function desvincularAbogado(id_abogado) {
                                                var route = "/abogados/" + id_abogado;
                                                var token = $("#token_registrar").val();
                                                swal({
                                                title: "¿Desea DESCINCULAR al abogado?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, DESVINCULAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                type: 'DELETE',
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                dataType: 'json',
                                                                success: function (s)
                                                                {
                                                                swal('ABOGADO  ' + s.retorno_codigo, s.err_mensaje, "success");
                                                                $('#lts_abogados_asignados').DataTable().ajax.reload();
                                                                },
                                                                error: function ()
                                                                {
                                                                alert('Ocurrio un error en el servidor ..');
                                                                }
                                                        });
                                                        });
                                                }

                                                function respaldoExpediente(exp_id) {
                                                jQuery('#myViewExpedienteDoc').modal('show');
                                                $.ajax({
                                                url: '/expedientesListadoDocumentos/-1/' + exp_id,
                                                        type: 'GET',
                                                        dataType: 'json',
                                                        success: function (s)
                                                        {
                                                        console.log(s.data[0].o_ruta_documento);
                                                        var pdfhtml = '';
                                                        pdfhtml = '<iframe src="' + s.data[0].o_ruta_documento + '?=' + (new Date()).getTime() + '" width="100%" height="500" frameborder="0" allowtransparency="true"></iframe>';
                                                        $('#vistapdf').html(pdfhtml);
                                                        return true;
                                                        },
                                                        error: function ()
                                                        {
                                                        alert('Ocurrio un error en el servidor ..');
                                                        }
                                                });
                                                }

                                                function eliminarRespaldo(id_exp_doc) {

                                                var route = "/eliminarRespaldoExp/" + id_exp_doc;
                                                swal({
                                                title: "¿Eliminar documento de respaldo?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, eliminar!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                type: 'GET',
                                                                cache: false,
                                                                contentType: false,
                                                                processData: false,
                                                                success: function (data) {
                                                                swal('RESPALDO', data.Mensaje, "success");
                                                                $('#lts_expediente_documento').DataTable().ajax.reload();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                }
                                                $(function () {
                                                $("#demandante").select2();
                                                $("#demandado").select2();
                                                $.ajax({
                                                url: '/personas/-1/2',
                                                        type: 'GET',
                                                        dataType: 'json',
                                                        success: function (s)
                                                        {
                                                        $('#demandante').find('option').remove();
                                                        $('#demandante').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                        $(s.data).each(function (k, z) { // indice, valor
                                                        $('#demandante').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                        });
                                                        $('#demandado').find('option').remove();
                                                        $('#demandado').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                        $(s.data).each(function (k, z) { // indice, valor
                                                        $('#demandado').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                        });
                                                        },
                                                        error: function ()
                                                        {
                                                        alert('Ocurrio un error en el servidor ..');
                                                        }
                                                });
                                                $("#lts_expedientes").DataTable({
                                                processing: true,
                                                        serverSide: true,
                                                        ajax: '/expedientesListado/-1/-1/-1',
                                                        "dom": 'Bfrtip',
                                                        "buttons": ['pageLength',
                                                        {
                                                        extend: 'excelHtml5',
                                                                title: 'REPORTE REGISTRO DE EXPEDIENTES',
                                                                text: '<i class="fa fa-file-excel-o"></i>',
                                                                titleAttr: 'Excel',
                                                                exportOptions: {
                                                                columns: [0, 1, 2, 3, 4, 5, 6]
                                                                }
                                                        },
                                                        {
                                                        extend: 'pdfHtml5',
                                                                text: '<i class="fa fa-file-pdf-o"></i>',
                                                                titleAttr: 'PDF',
                                                                title: 'REPORTE REGISTRO DE EXPEDIENTES',
                                                                exportOptions: {
                                                                columns: [0, 1, 2, 3, 4, 5, 6]
                                                                },
                                                                orientation: 'landscape',
                                                                pageSize: 'Letter',
                                                                download: 'open',
                                                                filename: 'REPORTE REGISTRO DE EXPEDIENTES',
                                                                alignment: 'center'
                                                        }
                                                        ],
                                                        columns: [
                                                        {data: "o_id_expediente", orderable: false},
                                                        {data: "o_nro_expediente"},
                                                        {data: "o_nombre_demandante"},
                                                        {data: "o_nombre_demandado"},
                                                        {data: "o_estado_expediente"},
                                                        {data: "o_registrado"},
                                                        {data: "o_usuario_mod"},
                                                        {data: "accion"}

                                                        ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                        "language": {
                                                        "url": "/lenguaje"
                                                        },
                                                        "lengthMenu": [[20, 25, 50, - 1], [20, 25, 50, "All"]]
                                                });
                                                });
                                                $("#registrar_expediente").click(function () {
                                                var tipo_registro = $('#tipo_registro').val();
                                                var token = $("#token_registrar").val();
                                                if (tipo_registro == 1) {
                                                var route = "/expedientes";
                                                swal({
                                                title: "¿Desea registrar el expediente?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, REGISTRAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formularioExpediente').serialize(),
                                                                success: function (data) {

                                                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_expedientes').DataTable().ajax.reload();
                                                                document.getElementById('formularioExpediente').reset();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                } else {
                                                var expediente = $('#expediente').val();
                                                var route = "/expedientes/" + expediente;
                                                swal({
                                                title: "¿Desea actualizar el expediente?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'PUT',
                                                                dataType: 'json',
                                                                data: $('#formularioExpediente').serialize(),
                                                                success: function (data) {

                                                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_expedientes').DataTable().ajax.reload();
                                                                document.getElementById('formularioExpediente').reset();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                }
                                                });
                                                $("#registrar_respaldo").click(function () {

                                                var route = "/registrarRespaldoExp";
                                                var token = $("#token_respaldo").val();
                                                var file_data = $('#respaldo_doc').prop('files')[0];
                                                var form_data = new FormData();
                                                form_data.append('respaldo_doc', file_data);
                                                form_data.append('id_expediente', $('#id_expediente').val());
                                                form_data.append('nombre_documento', $('#nombre_documento').val());
                                                form_data.append('observacion_documento', $('#observacion_documento').val());
                                                swal({
                                                title: "¿Subir documento de respaldo?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, subir!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                cache: false,
                                                                contentType: false,
                                                                processData: false,
                                                                data: form_data,
                                                                success: function (data) {
                                                                swal('RESPALDO', data.err_mensaje, "success");
                                                                $("#myUploadFile").modal('toggle');
                                                                $('#lts_expediente_documento').DataTable().ajax.reload();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                });
                                                $("#registrar_abogado_expediente").click(function () {
                                                var tipo_registro = $('#tipo_registro_abogado').val();
                                                var token = $("#token_registrar_abogado").val();
                                                if (tipo_registro == 1) {
                                                var route = "/abogados";
                                                swal({
                                                title: "¿Desea asignar abogado?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, ASIGNAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formulario_asignacion_abogado').serialize(),
                                                                success: function (data) {
                                                                swal('ABOGADO(S)', data.err_mensaje, "success");
                                                                $('#lts_abogados_asignados').DataTable().ajax.reload();
                                                                $('abogado_chz').val(null).trigger("change")
                                                                        document.getElementById('formulario_asignacion_abogado').reset();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                } else {
                                                var expediente = $('#expediente').val();
                                                var route = "/expedientes/" + expediente;
                                                swal({
                                                title: "¿Desea actualizar el expediente?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                        if (!isConfirm)
                                                                return;
                                                        $.ajax({
                                                        url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'PUT',
                                                                dataType: 'json',
                                                                data: $('#formularioExpediente').serialize(),
                                                                success: function (data) {
                                                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_expedientes').DataTable().ajax.reload();
                                                                document.getElementById('formularioExpediente').reset();
                                                                }, error: function (result) {
                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                        }
                                                        });
                                                        });
                                                }
                                                });
                                                //Progress Bar
                                                var reader;
                                                var progress = document.querySelector('.percent');
                                                function abortRead() {
                                                reader.abort();
                                                }

                                                function errorHandler(evt) {
                                                switch (evt.target.error.code) {
                                                case evt.target.error.NOT_FOUND_ERR:
                                                        alert('File Not Found!');
                                                break;
                                                case evt.target.error.NOT_READABLE_ERR:
                                                        alert('File is not readable');
                                                break;
                                                case evt.target.error.ABORT_ERR:
                                                        break; // noop
                                                default:
                                                        alert('An error occurred reading this file.');
                                                }
                                                ;
                                                }

                                                function updateProgress(evt) {
                                                // evt is an ProgressEvent.
                                                if (evt.lengthComputable) {
                                                var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
                                                // Increase the progress bar length.
                                                if (percentLoaded < 100) {
                                                progress.style.width = percentLoaded + '%';
                                                progress.textContent = percentLoaded + '%';
                                                }
                                                }
                                                }

                                                function handleFileSelect(evt) {
                                                // Reset progress indicator on new file selection.
                                                progress.style.width = '0%';
                                                progress.textContent = '0%';
                                                reader = new FileReader();
                                                reader.onerror = errorHandler;
                                                reader.onprogress = updateProgress;
                                                reader.onabort = function (e) {
                                                alert('File read cancelled');
                                                };
                                                reader.onloadstart = function (e) {
                                                document.getElementById('progress_bar').className = 'loading';
                                                };
                                                reader.onload = function (e) {
                                                // Ensure that the progress bar displays 100% at the end.
                                                progress.style.width = '100%';
                                                progress.textContent = '100%';
                                                setTimeout("document.getElementById('progress_bar').className='';", 2000);
                                                }

                                                // Read in the image file as a binary string.
                                                reader.readAsBinaryString(evt.target.files[0]);
                                                }

                                                document.getElementById('respaldo_doc').addEventListener('change', handleFileSelect, false);
                                                //End progress bar

</script>
@endsection