@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>Enlaces
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="/cuentas">Enlaces</a></li>
        <li class="active">Listado</li>
    </ol>
</section>

<section class="content" >
    <div class="row">

        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Enlacess</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo Enlace</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped table-responsive" id="lts_enlaces" width="100%">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Enlace</th>
                                        <th>Descripcion</th>
                                        <th width: 20%>Link</th>
                                        <th>Fecha Registro</th>
                                        <th>Usuario Registro</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVO ENLACE</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioEnlace" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="id_enlace" name="id_enlace" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>

                                <div class="col-md-3">
                                    <label for="nombre_enlace" style="clear:both;">Nombre enlace</label>
                                    <input type="text" name="nombre_enlace" id="nombre_enlace" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label for="descripcion_enlace" style="clear:both;">Descripción</label>
                                    <textarea name="descripcion_enlace" id="descripcion_enlace" class="form-control" placeholder="Descripción mensaje"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <label for="enlace" style="clear:both;">Link</label>
                                    <textarea name="enlace" id="enlace" class="form-control" placeholder="http:://"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <a  class="btn btn-success"  id="enviar_enlace">ENVIAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">

                                        $(function () {
                                            $("#lts_enlaces").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/enlacesListado/-1/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE ENLACES',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE ENLACES',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE ENLACES',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_id", orderable: false},
                                                    {data: "o_nombre_enlace"},
                                                    {data: "o_descripcion_enlace"},
                                                    {data: "o_accion_enlace"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });
                                            

                                        });

                                        function resetear() {
                                            $('#tipo_registro').val(1);
                                            $('#id_enlace').val(-1);
                                        }
                                        function editarEnlace(id)
                                        {
                                            $('.nav-tabs a[href="#nuevo"]').tab('show');
                                            $('#tipo_registro').val(2);
                                            var path = '/enlacesListado/' + id + '/-1/-1';
                                            $.get(path, function (res) {
                                                $('#id_enlace').val(id);
                                                $('#nombre_enlace').val(res.data[0].o_nombre_enlace);
                                                $('#descripcion_enlace').val(res.data[0].o_descripcion_enlace);
                                                $('#enlace').val(res.data[0].o_enlace);

                                            });

                                        }

                                        $("#enviar_enlace").click(function () {
                                            var token = $("#token_registrar").val();
                                            var id = $('#id_enlace').val();
                                            console.log(id);

                                            if ($('#tipo_registro').val() == 1) {
                                                var route = "/enlaces";
                                                swal({
                                                    title: "¿Desea registrar el enlace?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, REGISTRAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formularioEnlace').serialize(),
                                                                success: function (data) {
                                                                    swal('ENLACE', "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_enlaces').DataTable().ajax.reload();
                                                                    document.getElementById('formularioEnlace').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            } else {

                                                var route = "/enlaces/" + id;
                                                swal({
                                                    title: "¿Desea actualizar el enlace?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'PUT',
                                                                dataType: 'json',
                                                                data: $('#formularioEnlace').serialize(),
                                                                success: function (data) {
                                                                    swal('ENLACE', "ACTUALIZADO DE MANERA EXITOSA", "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_enlaces').DataTable().ajax.reload();
                                                                    document.getElementById('formularioEnlace').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            }
                                        });


</script>
@endsection