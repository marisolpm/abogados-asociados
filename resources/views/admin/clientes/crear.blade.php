@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo 'Clientes' ?>
        <small><?php echo 'Crear' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/clientes">Clientes</a></li>
        <li class="active"><?php echo 'Crear' ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Crear</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="" method="POST" id="formularioCreatePersona" >
                    <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="box-body">
                        <div class="col-md-12" id="result-ci"></div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="ci" style="clear:both;">C.I.</label>
                                <input type="text" id="ci" name="ci" placeholder="C.I." class="form-control" autocomplete="off">

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="ci_expedido" style="clear:both;">Expedido</label>
                                <select id="ci_expedido" name="ci_expedido" class="form-control">
                                    <option value="0">Seleccione lugar expedido</option>
                                    @foreach($departamentos as $departamento)
                                    <option value="{{$departamento->o_dep_id}}">{{$departamento->o_departamento}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="name" style="clear:both;">Nombre</label>
                                <input type="text" id="nombre" name="nombre" placeholder="Nombre" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="paterno" style="clear:both;">Paterno</label>
                                <input type="text" id="paterno" name="paterno" placeholder="Paterno" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="materno" style="clear:both;">Materno</label>
                                <input type="text" id="materno" name="materno" placeholder="Materno" class="form-control" autocomplete="off">
                            </div>
                        </div>



                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dob" style="clear:both;">Fecha de nacimiento</label>
                                <input type="text" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="YYYY-MM-DD" class="form-control datepicker" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email" style="clear:both;">Correo</label>
                                <input type="email" id="email" name="email" placeholder="ejemplo@ejemplo.com" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Teléfono/Celular</label>
                                <input type="text" id="contacto" name="contacto" placeholder="XXXXXXX" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Dirección</label>
                                <textarea id="direccion" name="direccion"  class="form-control" placeholder="Dirección"></textarea>
                            </div>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <a id="registrar_persona" class="btn btn-primary">Registrar</a>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>  
<script type="text/javascript">

    $('#ci').on('keyup', function () {

        var prs_ci = $(this).val();
//            console.log((/^\d{8}[A-Z]$/.test(prs_ci)));
        if ((/^\d{7}$/.test(prs_ci))) {
            verifica_ci(prs_ci);
        } else if ((/^\d{8}$/.test(prs_ci))) {
            verifica_ci(prs_ci);
        } else if (/^\d{7}-$/.test(prs_ci)) {
            verifica_ci(prs_ci);
        } else if (/^\d{8}-$/.test(prs_ci)) {
            verifica_ci(prs_ci);
        } else if (/^\d{7}-\d{1}[A-Z]$/.test(prs_ci)) {
            verifica_ci(prs_ci);
        } else if (/^\d{8}-\d{1}[A-Z]$/.test(prs_ci)) {
            verifica_ci(prs_ci);

        } else {
            console.log('No posee el formato requerido');
        }

    });
    function verifica_ci(prs_ci) {
        var dataString = 'prs_ci=' + prs_ci;
        var token = $("#token_registrar").val();
        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': token},
            url: "/personaVerificaCI",
            data: dataString,
            success: function (data) {
                console.log(data);
                if (data.length !== 0) {
                    $('#nombre').val(data[0].nombres);
//                        $('#nombres').attr('disabled', 'disabled');
                    $('#paterno').val(data[0].paterno);
//                        $('#paterno').attr('disabled', 'disabled');
                    $('#materno').val(data[0].materno);
//                        $('#materno').attr('disabled', 'disabled');
                    $('#ci_expedido').val(data[0].ci_expedido);
//                        $('#c_procedencia').attr('disabled', 'disabled');
                    $('#fecha_nacimiento').val(data[0].fecha_nacimiento);
//                        $('#fec_nacimiento').attr('disabled', 'disabled');
                    $('#direccion').val(data[0].direccion);
//                        $('#direccion').attr('disabled', 'disabled');
                    $('#contacto').val(data[0].celular);
//                        $('#telefono').attr('disabled', 'disabled');
                    $('#email').val(data[0].email);
//                        $('#correo').attr('disabled', 'disabled');
                    $('#result-ci').fadeIn(1000).html('<div class="alert alert-danger"><strong>C.I. se halla registrado.</strong></div>');
                    $('#registrar_persona').attr('disabled', 'disabled');
                } else {
                    $('#nombre').val('');
//                        $('#nombres').removeAttr('disabled');
                    $('#paterno').val('');
//                        $('#paterno').removeAttr('disabled');
                    $('#materno').val('');
//                        $('#materno').removeAttr('disabled');
                    $('#ci_expedido').val(0);
//                        $('#c_procedencia').removeAttr('disabled');
                    $('#fecha_nacimiento').val('');
//                        $('#fec_nacimiento').removeAttr('disabled');
                    $('#direccion').val('');
//                        $('#direccion').removeAttr('disabled');
//                        $('#direccionaux').val('');
//                        $('#direccionaux').removeAttr('disabled');
                    $('#contacto').val('');
//                        $('#telefono').removeAttr('disabled');
//                        $('#telefonoaux').val('');
//                        $('#telefonoaux').removeAttr('disabled');
//                        $('#celular').val('');
//                        $('#celular').removeAttr('disabled');
//                        $('#rutaPatio').val(0);
//                        $('#rutaPatio').removeAttr('disabled');
                    $('#email').val('');
//                        $('#correo').removeAttr('disabled');
//                        $('#estadocivil').val(0);
//                        $('#estadocivil').removeAttr('disabled');
                    $('#result-ci').fadeIn(1000).html(data);
                    $('#registrar_persona').removeAttr('disabled');
                }



            }
        });
    }
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".txtarea").wysihtml5();
    });

    jQuery('.datepicker').datetimepicker({
        lang: 'en',
        i18n: {
            de: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do.", "Lu", "Ma", "Mi",
                    "Ju", "Vi", "Sa.",
                ]
            }
        },
        timepicker: false,
        format: 'Y-m-d'
    });

    $("#registrar_persona").click(function () {

        var route = "/clientes";
        var token = $("#token_registrar").val();
        swal({
            title: "¿Desea registrar?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, REGISTRAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            dataType: 'json',
                            data: $('#formularioCreatePersona').serialize(),
                            success: function (data) {
                                swal(data.retorno_codigo, data.err_mensaje, "success");
                                window.location.href = "/clientes"
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    });

</script>

@endsection