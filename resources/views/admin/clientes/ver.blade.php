@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo 'Clientes' ?>
        <small><?php echo 'Ver' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/clientes">Clientes</a></li>
        <li class="active"><?php echo 'Ver' ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ver</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post">
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <center><h3 class="panel-title" id="informacionPersonal" style="font-weight: bold"></h3></center>
                                    <br>
                                    <center>
                                        <img width="150" <?php if (strlen($clientes[0]->o_prs_url) < 2) { ?> src= "/assets/img/avatar5.png" <?php } else { ?> src="{{$clientes[0]->o_prs_url}}" <?php } ?> height="150" id="imagen_preview" class="ImagePreviewBox" />

                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="name" style="clear:both;">Nombre</label>
                                    </div>	
                                    <div class="col-md-5">
                                        <?php echo $clientes[0]->o_prs_nombre_completo ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="name" style="clear:both;">C.I.</label>
                                    </div>	
                                    <div class="col-md-5">
                                        <?php echo $clientes[0]->o_prs_ci_completo ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="dob" style="clear:both;">Fecha nacimiento</label>
                                    </div>	
                                    <div class="col-md-5">	
                                        <?php echo $clientes[0]->o_prs_fecha_nacimiento ?>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="email" style="clear:both;">Email</label>
                                    </div>	
                                    <div class="col-md-5">	
                                        <?php echo $clientes[0]->o_prs_email ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="username" style="clear:both;">Nombre usuario</label>
                                    </div>	
                                    <div class="col-md-5">
                                        <?php echo $clientes[0]->o_prs_nombre_usuario ?>
                                    </div>
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="contact" style="clear:both;">Teléfono</label>
                                    </div>	
                                    <div class="col-md-5">
                                        <?php echo $clientes[0]->o_prs_contacto ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="contact" style="clear:both;">Dirección</label>
                                    </div>	
                                    <div class="col-md-5">
                                        <?php echo $clientes[0]->o_prs_direccion ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection