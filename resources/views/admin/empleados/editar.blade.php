@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Empleados
        <small> Editar</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/empleados">Empleados</a></li>
        <li class="active"> Editar</li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="" method="POST" id="formularioEditarPersona" >
                    <input id="token_editar" name="token_editar" type="hidden" value="{{ csrf_token() }}"/>
                    <input id="id_persona" name="id_persona" type="hidden" value="{{ $clientes[0]->o_prs_id }}"/>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <center><h3 class="panel-title" id="informacionPersonal" style="font-weight: bold"></h3></center>
                                <br>
                                <center>
                                    <img width="150" <?php if (strlen($clientes[0]->o_prs_url) < 2) { ?> src= "/assets/img/avatar5.png" <?php } else { ?> src="{{$clientes[0]->o_prs_url}}" <?php } ?> height="150" id="imagen_preview" class="ImagePreviewBox" />
                                    <br>
                                    <button type="button" class="btn btn-default" onclick="rotar_foto_1(1, 1)" data-toggle="tooltip" title data-original-title="Girar foto a la izquierda">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </button><button type="button" class="btn btn-default" onclick="rotar_foto_1(1, 2)" data-toggle="tooltip" title data-original-title="Girar foto a la derecha">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                    <br><br><span class="btn btn-primary btn-file">
                                        TOMAR FOTO / SUBIR IMÁGEN <input type="file" id="imagen" class="btn btn-success btn-block" onchange="revisarImagen(this, 1);" value="TOMAR FOTO / SUBIR IMÁGEN" ></span>

                                </center>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" style="clear:both;">Nombre</label>
                                <input type="text" name="nombre" placeholder="Nombre" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_nombres}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paterno" style="clear:both;">Paterno</label>
                                <input type="text" name="paterno" placeholder="Paterno" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_paterno}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="materno" style="clear:both;">Materno</label>
                                <input type="text" name="materno" placeholder="Materno" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_materno}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ci" style="clear:both;">C.I.</label>
                                <input type="text" name="ci" placeholder="C.I." class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_ci}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ci_expedido" style="clear:both;">Expedido</label>
                                <select name="ci_expedido" class="form-control">
                                    <option value="0">Seleccione lugar expedido</option>
                                    @foreach($departamentos as $departamento)
                                    <option value="{{$departamento->o_dep_id}}" <?php
                                    if ($clientes[0]->o_prs_expedido == $departamento->o_dep_id) {
                                        echo 'selected';
                                    }
                                    ?> >{{$departamento->o_departamento}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dob" style="clear:both;">Fecha de nacimiento</label>
                                <input type="text" name="fecha_nacimiento" placeholder="YYYY-MM-DD" class="form-control datepicker" autocomplete="off" value="{{$clientes[0]->o_prs_fecha_nacimiento}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="area" style="clear:both;">Área</label>
                            <select name="area" class="form-control">
                                <option value="0">Seleccione Área</option>
                                @foreach($areas as $area)
                                <option value="{{$area->o_id}}" <?php
                                if ($clientes[0]->o_prs_id_area == $area->o_id) {
                                    echo 'selected';
                                }
                                ?> >{{$area->o_tipo_proceso}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="matricula" style="clear:both;">Matricula</label>
                                <input type="matricula" name="matricula" placeholder="" value="{{$clientes[0]->o_prs_matricula}}" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" style="clear:both;">Correo</label>
                                <input type="email" name="email" placeholder="ejemplo@ejemplo.com" class="form-control" value="{{$clientes[0]->o_prs_email}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Teléfono/Celular</label>
                                <input type="text" name="contacto" placeholder="XXXXXXX" class="form-control" value="{{$clientes[0]->o_prs_contacto}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Dirección</label>
                                <textarea name="direccion"  class="form-control" placeholder="Dirección">{{$clientes[0]->o_prs_direccion}}</textarea>
                            </div>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <a id="actualizar_persona" class="btn btn-primary">Actualizar</a>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>  

<script type="text/javascript">
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".txtarea").wysihtml5();
    });

    jQuery('.datepicker').datetimepicker({
        lang: 'en',
        i18n: {
            de: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do.", "Lu", "Ma", "Mi",
                    "Ju", "Vi", "Sa.",
                ]
            }
        },
        timepicker: false,
        format: 'Y-m-d'
    });

    $("#actualizar_persona").click(function () {

        var route = "/empleados/" + $("#id_persona").val();
        var token = $("#token_editar").val();
        swal({
            title: "¿Desea editar el registro?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, EDITAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioEditarPersona').serialize(),
                            success: function (data) {
                                swal(data.retorno_codigo, data.err_mensaje, "success");
                                window.location.href = "/empleados"
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    });

    function revisarImagen(input, num) {
        var id_preview = input.getAttribute("id") + "_preview";
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function (e) {
                var id_preview_text = "#" + id_preview;
                var base64image = e.target.result;
                $("body").append("<canvas id='tempCanvas' width='150' height='150' style='display:none'></canvas>");
                var canvas = document.getElementById("tempCanvas");
                var ctx = canvas.getContext("2d");
                var cw = canvas.width;
                var ch = canvas.height;
                var maxW = 150;
                var maxH = 150;
                var img = new Image;
                img.src = this.result;
                img.onload = function () {
                    var iw = img.width;
                    var ih = img.height;
                    var scale = Math.min((maxW / iw), (maxH / ih));
                    var iwScaled = iw * scale;
                    var ihScaled = ih * scale;
                    canvas.width = iwScaled;
                    canvas.height = ihScaled;
                    ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
                    base64image = canvas.toDataURL("image/jpeg");
                    $(id_preview_text).attr('src', base64image).width(150).height(150);
                    imagen[num] = base64image;
                    $("#tempCanvas").remove();
                }
            };
            reader.readAsDataURL(input.files[0]);
        }


        // creacion de registro POST
        var form_data = new FormData();
        var file_data = $('#imagen').prop('files')[0];
        form_data.append('prs_id', $('#id_persona').val());
        form_data.append('prs_imagen', file_data);
        //console.log(form_data);
        $.ajax({
            url: "/subirFoto",
            headers: {'X-CSRF-TOKEN': $("#token_editar").val()},
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'text',
            data: form_data,
            success: function (data) {
                var imagen = JSON.parse(data);
                var hora = new Date().getTime();
                document.getElementById('imagen_preview').src = imagen.imagen + '?' + hora;
                $('#imagen').val('');
                swal("Foto fue subida", "Correctamente!", "success");
            }, error: function (result) {
                $.LoadingOverlay("hide", true);
                swal("Opss..!", "No se pudo actualizar foto de la persona!", "error")
            }
        });
    }
    function rotar_foto_1(prs_id, tipo_rotacion) {
        prs_id = $("#id_persona").val();
        $.ajax({
            data: {prs_id: prs_id, tipo_rotacion: tipo_rotacion},
            url: '/rotarFoto',
            headers: {'X-CSRF-TOKEN': $("#token_editar").val()},
            type: 'POST',
            dataType: 'json',
            cache: false,
            success: function (r)
            {
                var hora = new Date().getTime();
                if (r.imagen !== ' ') {

                    imageObject = document.getElementById('imagen_preview');
                    imageObject.src = r.imagen + '?' + hora;
                }

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
    }
</script>

@endsection