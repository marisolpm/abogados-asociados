@extends('layouts.partials.main')
@section('content')
<link href="assets/css/chosen.css" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Agenda
        <small>Citas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="">Agenda</a></li>
        <li class="active">Citas</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @include('admin.citas.partial.modalCrear')
        <!-- left column -->
        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista de Citas</span>

                    </a>
                </li>
                <li class="" id="tabDetalle">
                    <a data-toggle="tab" href="#detalle">
                        <i class="fa fa-eye"></i> <span>Calendario</span>

                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered table-striped" id="lts_cita">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Cliente</th>
                                        <th>Abogado</th>
                                        <th>Asunto</th>
                                        <th>Mensaje</th>
                                        <th>Lugar</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Hora</th>
                                        <th>Usuario Edición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>     
                    </div>
                </div>


                <div class="tab-pane fade" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Citas</h3>

                        </div>
                        <div class="panel-body">



                            <div class="card-content table-responsive">
                                <div id="calendar"  style="border: 1px solid #000;padding:2px"></div>
                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
</section>  


<script src="assets/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/redactor.min.js"></script>
<script type="text/javascript">


    function editarCita(id) {

        $.ajax({
            url: "/citasListado/" + id + "/-1",
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                console.log(s.data[0]);
                $("#tipo_registro").val(2);
                $('#id_cita').val(s.data[0].o_id);
                $("#fecha").val(s.data[0].o_fecha_cita);
                $("#hora").val(s.data[0].o_hora_cita);

                $('#cliente').select2("val", s.data[0].o_id_cliente);
                $('#empleado').select2("val", s.data[0].o_id_abogado);

                $('#asunto').val(s.data[0].o_asunto);
                $('#estado_cita').val(s.data[0].o_estado_cita);
                $('#lugar').val(s.data[0].o_lugar_cita);
                $('#descripcion').text(s.data[0].o_descripcion);
                $('#modal-event').modal();

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
    }
    $(function () {
        $("#cliente").select2();
        $("#empleado").select2();
        $('#calendar').fullCalendar({
            lang: 'es',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '<?php echo date('Y-m-d'); ?>',
            editable: true,

            events: "/citasListadoCalendario/-1/-1",
            dayClick: function (date, jsEvent, view) {
                document.getElementById('formularioCita').reset();
                $('#descripcion').text('');
                $("#fecha").val(date.format("YYYY-MM-DD"));
                $("#hora").val(date.format("HH:mm"));
                $("#tipo_registro").val(1);
                $('#id_cita').val(-1);

                $('#modal-event').modal();
            },
            eventClick: function (calEvent, jsEvent, view) {
                $("#tipo_registro").val(2);
                $('#id_cita').val(calEvent.id_cita);
                $("#fecha").val(calEvent.start.format("YYYY-MM-DD"));
                $("#hora").val(calEvent.start.format("HH:mm"));
                $('#cliente').val(calEvent.id_cliente);
                $('#empleado').val(calEvent.id_abogado);
                $('#asunto').val(calEvent.asunto);
                $('#estado_cita').val(calEvent.estado_cita);
                $('#lugar').val(calEvent.lugar);
                $('#descripcion').text(calEvent.description);
                $('#modal-event').modal();


            }
        });

        $("#lts_cita").DataTable({
            processing: true,
            serverSide: true,
            ajax: '/citasListado/-1/-1',
            "dom": 'Bfrtip',
            "buttons": ['pageLength',
                {
                    extend: 'excelHtml5',
                    title: 'REPORTE CITAS',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: 'REPORTE CITAS',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    },
                    orientation: 'landscape',
                    pageSize: 'Letter',
                    download: 'open',
                    filename: 'REPORTE CITAS',
                    alignment: 'center'
                }
            ],
            columns: [
                {data: "o_id", orderable: false},
                {data: "o_nombre_cliente"},
                {data: "o_nombre_abogado"},
                {data: "o_asunto"},
                {data: "o_descripcion"},
                {data: "o_lugar_cita"},
                {data: "o_descripcion_estado_cita"},
                {data: "o_fecha_cita"},
                {data: "o_hora_cita"},
                {data: "o_usuario_mod"},
                {data: "o_accion"}

            ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $('td', nRow).css('background-color', '#FFFFFF');
            },
            responsive: true,
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
        });

        $.ajax({
            url: '/personas/-1/2',
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#cliente').find('option').remove();
                $('#cliente').append('<option value=0>ELIGA UNA PERSONA</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#cliente').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                });

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });

        $.ajax({
            url: '/personas/-1/1',
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#empleado').find('option').remove();
                $('#empleado').append('<option value=0>ELIGA UNA PERSONA</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#empleado').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                });

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });


        $("#registrar_cita").click(function () {
            var tipo_registro = $('#tipo_registro').val();
            var token = $("#token_registrar").val();
            if (tipo_registro == 1) {
                var route = "/citas";
                swal({
                    title: "¿Desea registrar cita?",
                    text: "Se sugiere revise los datos antes de proceder",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Sí, REGISTRAR!",
                    cancelButtonText: "No, revisar",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true
                },
                        function (isConfirm) {
                            if (!isConfirm)
                                return;
                            $.ajax({
                                url: route,
                                headers: {'X-CSRF-TOKEN': token},
                                type: 'POST',
                                dataType: 'json',
                                data: $('#formularioCita').serialize(),
                                success: function (data) {
                                    swal('CITA ' + data.retorno_codigo, data.err_mensaje, "success");
                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                    document.getElementById('formularioCita').reset();
                                    $('#modal-event').modal('hide');
                                    $('#lts_cita').DataTable().ajax.reload();
                                    $('#calendar').fullCalendar('refetchEventSources', '/citasListadoCalendario/-1/-1');


                                }, error: function (result) {
                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                }
                            });
                        });
            } else {
                var id_cita = $('#id_cita').val();
                var route = "/citas/" + id_cita;

                swal({
                    title: "¿Desea actualizar la cita?",
                    text: "Se sugiere revise los datos antes de proceder",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Sí, ACTUALIZAR!",
                    cancelButtonText: "No, revisar",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true
                },
                        function (isConfirm) {
                            if (!isConfirm)
                                return;
                            $.ajax({
                                url: route,
                                headers: {'X-CSRF-TOKEN': token},
                                type: 'PUT',
                                dataType: 'json',
                                data: $('#formularioCita').serialize(),
                                success: function (data) {

                                    swal('CITA ' + data.retorno_codigo, data.err_mensaje, "success");
                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                    $('#modal-event').modal('hide');
                                    $('#lts_cita').DataTable().ajax.reload();
                                    $('#calendar').fullCalendar('refetchEventSources', '/citasListadoCalendario/-1/-1');
                                }, error: function (result) {
                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                }
                            });
                        });
            }
        });

    });


    function eliminarCita(id) {
        var id_cita = id;
        var route = "citas/" + id_cita;
        var token = $("#token_registrar").val();
        swal({
            title: "¿Desea eliminar la cita ID: " + id_cita + "?",
            text: "Se sugiere revise los datos antes de proceder",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, ELIMINAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (!isConfirm)
                        return;
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'DELETE',
                        dataType: 'json',
                        data: $('#formularioCita').serialize(),
                        success: function (data) {

                            swal('CITA N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                            $('.nav-tabs a[href="#lista"]').tab('show');
                            $('#lts_cita').DataTable().ajax.reload();
                            document.getElementById('formularioCitas').reset();
                        }, error: function (result) {
                            swal("Opss..!", "La cita posee registro en otras tablas, no es posible eliminar!", "error");
                        }
                    });
                });
    }
</script>

<script>

</script>
@endsection