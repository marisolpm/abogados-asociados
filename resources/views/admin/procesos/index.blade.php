@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    #progress_bar {
        margin: 10px 0;
        padding: 3px;
        border: 1px solid #000;
        font-size: 14px;
        clear: both;
        opacity: 0;
        -moz-transition: opacity 1s linear;
        -o-transition: opacity 1s linear;
        -webkit-transition: opacity 1s linear;
    }
    #progress_bar.loading {
        opacity: 1.0;
    }
    #progress_bar .percent {
        background-color: #99ccff;
        height: auto;
        width: 0;
    }

</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        <?php echo 'Procesos' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/procesos">Procesos</a></li>
        <li class="active"><?php echo 'Listado' ?></li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        @include('admin.procesos.partial.modalUploadFile')
        @include('admin.procesos.partial.modalViewFile')
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista" onclick="actualizarListado();">
                        <i class="fa fa-folder"></i> <span>Lista Procesos</span>

                    </a>
                </li>
                <li class="" id="tabDetalle">
                    <a data-toggle="tab" href="#detalle">
                        <i class="fa fa-eye"></i> <span>Detalle del Proceso (Documentos)</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo/Editar Proceso</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="lts_procesos">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th># Expediente</th>
                                        <th>Datos Proceso</th>
                                        <th>Fecha Inicio</th>
                                        <th>Cliente</th>
                                        <th>Abogado(a)</th>
                                        <th>Posición</th>
                                        <th>Estado</th>
                                        <th>Última Edición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Proceso</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-borderless">
                                        <thead>
                                            <tr><th>Nombre</th><td>&nbsp;</td><td id="nombreP"></td><th>Fecha Inicio</th><td>&nbsp;</td><td><i class="fa fa-clock-o"></i> <span id="fechaIP"></span></td><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoP"></span></td></tr>
                                            <tr><th>Tipo Proceso</th><td>&nbsp;</td><td id="tipoP"></td><th>Estado</th><td>&nbsp;</td><td id="estadoP"></td><th>Usuario modificación</th><td>&nbsp;</td><td ><i class="fa fa-user"></i> <span id="usrmodificadoP"></span></td></tr>
                                            <tr><th>Descripción</th><td>&nbsp;</td><td id="descripcionP" style="width: 20%"></td><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroP"></span></td></tr>
                                            <tr><th></th><td></td><td></td><th>Usuario registro</th><td>&nbsp;</td><td ><i class="fa fa-user"></i> <span id="usrregistroP"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-borderless">
                                        <thead>
                                            <tr>
                                                <th>Cliente</th><td>&nbsp;</td><td id="clienteP"></td>
                                                <th>Persona Contraria</th><td>&nbsp;</td><td id="contrarioP"></td>
                                            </tr>
                                            <tr>
                                                <th>Posición Cliente</th><td>&nbsp;</td><td id="posicionP"></td>
                                                <th>Involucrados</th><td>&nbsp;</td><td id="involucradosP"></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Documentos del Proceso</h4>
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-xs-12">
                                        <div class="btn-group pull-right">
                                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                                <input id="proceso" name="proceso" type="hidden" value=""/>
                                                <input id="tipo_registro_proceso" name="tipo_registro_proceso" type="hidden" value="1"/>

                                                <div class="col-md-12">
                                                    <a  class="btn btn-primary" onclick="SubirDocumento(1)"><i class="fa fa-plus-circle"></i> Subir Documento</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_proceso_documento">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre Documento</th>
                                                <th>Enlace</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Registro</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVO/EDITAR PROCESO</h3>

                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioProceso" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="proceso" name="proceso" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>    

                                <div class="col-md-3" id="posicionReg">
                                    <label for="posicion" style="clear:both;">Posición del Cliente</label>
                                    <select name="posicion" id="posicion" class="form-control" onchange="posicionCliente(this)">
                                        <option value="0">Elija posición</option>
                                        <option value="1">Demandante</option>
                                        <option value="2">Demandado</option>
                                    </select>
                                    <input id="posicionAct" class="form-control" readonly style="display: none">
                                </div>
                                <div class="col-md-3" id="clienteReg">
                                    <label for="cliente" style="clear:both;">Cliente</label>
                                    <select name="cliente" id="cliente" class="form-control" style="width: 100%" onchange="expedientesCliente(this)">
                                    </select>
                                    <input id="clienteAct" class="form-control" readonly style="display: none">
                                </div>
                                <div class="col-md-3" id="expedienteReg">
                                    <label for="expediente" style="clear:both;">Expediente Judicial</label>
                                    <select style="display: yes" name="expediente" id="expediente" class="form-control" onchange="expedientesContrario(this)">
                                    </select>
                                    <input id="expedienteAct" class="form-control" readonly style="display: none">
                                </div>
                                <div class="col-md-3" id="contrarioReg">
                                    <label for="contrario" style="clear:both;">Persona Contraria a la Demanda</label>
                                    <input type="text" name="contrario" id="contrario" placeholder="Nombre de la Persona" class="form-control" autocomplete="off" readonly>
                                    <input id="contrarioAct" class="form-control" readonly style="display: none">
                                </div>
                                <div class="col-md-3" id="procesoReg">
                                    <label for="nombre" style="clear:both;">Nombre Proceso</label>
                                    <input type="text" name="nombre" id="nombre" placeholder="Nombre Proceso" class="form-control" autocomplete="off" style="display: yes">
                                </div>
                                <div class="col-md-3" id="tipoReg">
                                    <label for="tipo" style="clear:both;">Tipo de Proceso</label>
                                    <select name="tipo" id="tipo" class="form-control" style="display: yes">

                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="estado" style="clear:both;">Estado</label>
                                    <select name="estado" id="estado" class="form-control">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="abogado" style="clear:both;">Abogado(a)</label>
<!--                                    <select name="abogados[]" id="abogado"  class="chzn form-control" style="width: 100%" multiple="multiple" data-placeholder="Asigne Abogados" >
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->o_prs_id}}">{{$empleado->o_prs_nombre_completo}}</option>
                                        @endforeach
                                    </select>-->
                                    <select name="abogados" id="abogado"  class="form-control" style="width: 100%" data-placeholder="Asigne Abogados" >
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->o_prs_id}}">{{$empleado->o_prs_nombre_completo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="descripcion" style="clear:both;">Descripción</label>
                                    <textarea name="descripcion" id="descripcion" class="form-control" placeholder="Descripción proceso"></textarea>
                                </div>


                                <div class="col-md-6">
                                    <label for="involucrados" style="clear:both;">Personas involucradas</label>
                                    <textarea name="involucrados" id="involucrados" class="form-control" placeholder="Involucrados"></textarea>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <a  class="btn btn-success"  id="registrar_proceso">REGISTRAR</a>
                                    <input type="reset" class="btn btn-primary" value="LIMPIAR">
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    var id_cliente = $('#cliente').val();

    function actualizarListado() {
        $("#posicion").show();
        $("#posicionAct").hide();
        $("#cliente").next(".select2-container").show();
        $("#clienteAct").hide();
        $("#expediente").show();
        $("#expedienteAct").hide();
        $("#lts_procesos").DataTable({
            processing: true,
            serverSide: true,
            ajax: '/procesosListado/-1/-1',
            "dom": 'Bfrtip',
            "buttons": ['pageLength',
                {
                    extend: 'excelHtml5',
                    title: 'REPORTE PROCESOS',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: 'REPORTE PROCESOS',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    },
                    orientation: 'landscape',
                    pageSize: 'Letter',
                    download: 'open',
                    filename: 'REPORTE PROCESOS',
                    alignment: 'center'
                }
            ],
            columns: [
                {data: "o_id", orderable: false},
                {data: "o_id_nro_expediente"},
                {data: "proceso"},
                {data: "o_fecha_inicio"},
                {data: "o_nombre_cliente"},
                {data: "o_nombre_abogado"},
                {data: "o_nombre_posicion"},
                {data: "o_nombre_estado_proceso"},
                {data: "modificado"},
                {data: "o_accion"}

            ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $('td', nRow).css('background-color', '#FFFFFF');
            },
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
        });

    }

    function expedientesCliente(id_cliente) {

        var posicion = document.getElementById("posicion").value;
        $.ajax({
            url: '/expedientesListado/-1/' + id_cliente.value + '/' + posicion,
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#expediente').find('option').remove();
                $('#expediente').append('<option value=0>ELIGA EXPEDIENTE</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#expediente').append('<option value= ' + z.o_id + '>' + z.o_nro_expediente + '</option>');
                });
            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
    }

    function expedientesContrario(id_expediente) {
        console.log(id_expediente.value);
        var posicion = document.getElementById("posicion").value;
        var cliente = $("#cliente").val();
        $.ajax({
            url: '/expedientesListado/' + id_expediente.value + '/' + cliente + '/' + posicion,
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                console.log(s);
                $(s.data).each(function (k, z) { // indice, valor
                    $('#contrario').val(z.o_nombre_demandado);
                });
            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
    }

    function posicionCliente(id_posicion) {

        $.ajax({
            url: '/procesosPosicion/' + id_posicion.value,
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#cliente').find('option').remove();
                $('#cliente').append('<option value = -1 selected>SELECCIONE CLIENTE</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#cliente').append('<option value= ' + z.o_cliente + '>' + z.o_nombre_cliente + '</option>');
                });
            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
    }

    function SubirDocumento(pro_id) {
        jQuery('#myUploadFile').modal('show');
    }
    function cancelar() {
        $('.nav-tabs a[href="#lista"]').tab('show');
        actualizarListado();
        $('#tipo_registro').val(1);
    }
    function resetear() {
        $('#tipo_registro').val(1);
    }
    function detalleProceso(id)
    {
        $('.nav-tabs a[href="#detalle"]').tab('show');
        var path = '/procesosListado/' + id + '/-1';
        $.get(path, function (res) {
            $('#id_proceso').val(id);
            console.log(res);
            document.getElementById("nombreP").innerHTML = res.data[0].o_proceso;
            document.getElementById("fechaIP").innerHTML = res.data[0].o_fecha_inicio;
            document.getElementById("tipoP").innerHTML = res.data[0].o_nombre_proceso;
            document.getElementById("estadoP").innerHTML = res.data[0].o_nombre_estado_proceso;
            document.getElementById("descripcionP").innerHTML = res.data[0].o_descripcion;
            document.getElementById("registroP").innerHTML = res.data[0].o_registrado;
            document.getElementById("modificadoP").innerHTML = res.data[0].o_modificado;
            document.getElementById("usrregistroP").innerHTML = res.data[0].o_usuario_reg;
            document.getElementById("usrmodificadoP").innerHTML = res.data[0].o_usuario_mod;
            document.getElementById("clienteP").innerHTML = res.data[0].o_nombre_cliente;
            document.getElementById("contrarioP").innerHTML = res.data[0].o_contrario;
            document.getElementById("posicionP").innerHTML = res.data[0].o_nombre_posicion;
            document.getElementById("involucradosP").innerHTML = res.data[0].o_involucrados;
        });

        $('#lts_proceso_documento').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: '/procesosListadoDocumentos/' + id + '/-1',
            columns: [
                {data: "o_nombre_documento", orderable: false},
                {data: "o_documentos"},
                {data: "o_registrado"},
                {data: "o_usuario_mod"},
                {data: "o_accion"}
            ],

            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

    }
    function mostrarProceso(id)
    {
        $("#posicion").toggle('hide');
        $("#posicionAct").toggle('show');
        $("#cliente").next(".select2-container").hide();
        $("#clienteAct").toggle('show');
        $("#expediente").toggle('hide');
        $("#expedienteAct").toggle('show');

        $('.nav-tabs a[href="#nuevo"]').tab('show');
        $('#tipo_registro').val(2);
        var path = '/procesosListado/' + id + '/-1';
        $.get(path, function (res) {
            console.log(res);
            $('#proceso').val(id);
            $('#posicionAct').val(res.data[0].o_nombre_posicion);
            $("#clienteAct").val(res.data[0].o_nombre_cliente);
            $('#contrario').val(res.data[0].o_contrario);
            $('#nombre').val(res.data[0].o_proceso);
            $('#tipo').val(res.data[0].o_tipo_proceso);
            $('#estado').val(res.data[0].o_estado_proceso);
            $('#abogado').select2('val', res.data[0].o_id_abogado);
            $('#descripcion').val(res.data[0].o_descripcion);
            $('#involucrados').val(res.data[0].o_involucrados);
            $('#expedienteAct').val(res.data[0].o_id_nro_expediente);
        });
    }
    function eliminarProceso(id) {
        var id_proceso = id;
        var route = "/procesos/" + id_proceso;
        var token = $("#token_registrar").val();
        swal({
            title: "¿Desea eliminar el proceso ID: " + id_proceso + "?",
            text: "Se sugiere revise los datos antes de proceder",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, ELIMINAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (!isConfirm)
                        return;
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'DELETE',
                        dataType: 'json',
                        data: $('#formularioProceso').serialize(),
                        success: function (data) {

                            swal('PROCESO N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                            $('.nav-tabs a[href="#lista"]').tab('show');
                            $('#lts_procesos').DataTable().ajax.reload();
                            document.getElementById('formularioProceso').reset();
                        }, error: function (result) {
                            swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                        }
                    });
                });
    }

    function archivarProceso(id) {
        var id_proceso = id;
        var route = "/archivarProcesos/" + id_proceso;
        var token = $("#token_registrar").val();
        swal({
            title: "¿Desea archivar el proceso ID: " + id_proceso + "?",
            text: "Se sugiere revise los datos antes de proceder",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, ARCHIVAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (!isConfirm)
                        return;
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'GET',
                        dataType: 'json',
                        data: $('#formularioProceso').serialize(),
                        success: function (data) {

                            swal('PROCESO N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                            $('.nav-tabs a[href="#lista"]').tab('show');
                            $('#lts_procesos').DataTable().ajax.reload();
                            document.getElementById('formularioProceso').reset();
                        }, error: function (result) {
                            swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                        }
                    });
                });
    }

    function respaldoProceso(pro_id) {
        console.log(pro_id);
        jQuery('#myViewProcesoDoc').modal('show');

        $.ajax({
            url: '/procesosListadoDocumentos/-1/' + pro_id,
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                console.log(s.data[0].o_ruta_documento);
                var pdfhtml = '';
                pdfhtml = '<iframe src="' + s.data[0].o_ruta_documento + '?=' + (new Date()).getTime() + '" width="100%" height="500" frameborder="0" allowtransparency="true"></iframe>';
                $('#vistapdf').html(pdfhtml);
                return true;

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });

    }
    $(function () {
        $('#cliente').select2();
        $('#abogado').select2();

        $.ajax({
            url: '/tiposProcesoListado',
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#tipo').find('option').remove();
                $('#tipo').append('<option value=0>ELIGA TIPO DE PROCESO</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#tipo').append('<option value= ' + z.o_id + '>' + z.o_tipo_proceso + '</option>');
                });

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });

        $.ajax({
            url: '/estadosProcesoListado',
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#estado').find('option').remove();
                $('#estado').append('<option value=0>ELIGA ESTADO DE PROCESO</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#estado').append('<option value= ' + z.o_id + '>' + z.o_estado_proceso + '</option>');
                });

            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });
        $("#lts_procesos").DataTable({
            processing: true,
            serverSide: true,
            ajax: '/procesosListado/-1/-1',
            "dom": 'Bfrtip',
            "buttons": ['pageLength',
                {
                    extend: 'excelHtml5',
                    title: 'REPORTE PROCESOS',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: 'REPORTE PROCESOS',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    },
                    orientation: 'landscape',
                    pageSize: 'Letter',
                    download: 'open',
                    filename: 'REPORTE PROCESOS',
                    alignment: 'center'
                }
            ],
            columns: [
                {data: "o_id", orderable: false},
                {data: "o_id_nro_expediente"},
                {data: "proceso"},
                {data: "o_fecha_inicio"},
                {data: "o_nombre_cliente"},
                {data: "o_nombre_abogado"},
                {data: "o_nombre_posicion"},
                {data: "o_nombre_estado_proceso"},
                {data: "modificado"},
                {data: "o_accion"}

            ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $('td', nRow).css('background-color', '#FFFFFF');
            },
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
        });

    });
    $("#registrar_proceso").click(function () {
        var tipo_registro = $('#tipo_registro').val();
        var token = $("#token_registrar").val();
        if (tipo_registro == 1) {
            var route = "/procesos";

            swal({
                title: "¿Desea registrar el proceso?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, REGISTRAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            dataType: 'json',
                            data: $('#formularioProceso').serialize(),
                            success: function (data) {

                                swal('PROCESO N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                $('.nav-tabs a[href="#lista"]').tab('show');
                                $('#lts_procesos').DataTable().ajax.reload();
                                document.getElementById('formularioProceso').reset();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        } else {
            var proceso = $('#proceso').val();
            var route = "/procesos/" + proceso;

            swal({
                title: "¿Desea actualizar el proceso?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, ACTUALIZAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioProceso').serialize(),
                            success: function (data) {

                                swal('PROCESO N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                $('.nav-tabs a[href="#lista"]').tab('show');
                                $('#lts_procesos').DataTable().ajax.reload();
                                $("#posicion").show();
                                $("#posicionAct").hide();
                                $("#cliente").next(".select2-container").show();
                                $("#clienteAct").hide();
                                $("#expediente").show();
                                $("#expedienteAct").hide();
                                document.getElementById('formularioProceso').reset();

                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        }
    });
    $("#registrar_respaldo").click(function () {

        var route = "/registrarRespaldo";
        var token = $("#token_respaldo").val();
        var file_data = $('#respaldo_doc').prop('files')[0];
        var form_data = new FormData();
        form_data.append('respaldo_doc', file_data);
        form_data.append('id_proceso', $('#id_proceso').val());
        form_data.append('nombre_documento', $('#nombre_documento').val());
        form_data.append('observacion_documento', $('#observacion_documento').val());

        swal({
            title: "¿Subir documento de respaldo?",
            text: "Se sugiere revise los datos antes de proceder",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, subir!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (!isConfirm)
                        return;
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function (data) {
                            swal('RESPALDO', data.err_mensaje, "success");
                            console.log(data);
                            $("#myUploadFile").modal('toggle');
                            //swal(data.unimed_codigo, data.err_mensaje, "success");
                            $('#lts_proceso_documento').DataTable().ajax.reload();

                        }, error: function (result) {
                            swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                        }
                    });

                });

    });

    function eliminarRespaldo(id_proc_doc) {

        var route = "/eliminarRespaldo/" + id_proc_doc;

        swal({
            title: "¿Eliminar documento de respaldo?",
            text: "Se sugiere revise los datos antes de proceder",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, eliminar!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (!isConfirm)
                        return;
                    $.ajax({
                        url: route,
                        type: 'GET',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            swal('RESPALDO', data.Mensaje, "success");
                            $('#lts_proceso_documento').DataTable().ajax.reload();

                        }, error: function (result) {
                            swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                        }
                    });

                });

    }

    //Progress Bar
    var reader;
    var progress = document.querySelector('.percent');

    function abortRead() {
        reader.abort();
    }

    function errorHandler(evt) {
        switch (evt.target.error.code) {
            case evt.target.error.NOT_FOUND_ERR:
                alert('File Not Found!');
                break;
            case evt.target.error.NOT_READABLE_ERR:
                alert('File is not readable');
                break;
            case evt.target.error.ABORT_ERR:
                break; // noop
            default:
                alert('An error occurred reading this file.');
        }
        ;
    }

    function updateProgress(evt) {
        // evt is an ProgressEvent.
        if (evt.lengthComputable) {
            var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
            // Increase the progress bar length.
            if (percentLoaded < 100) {
                progress.style.width = percentLoaded + '%';
                progress.textContent = percentLoaded + '%';
            }
        }
    }

    function handleFileSelect(evt) {
        // Reset progress indicator on new file selection.
        progress.style.width = '0%';
        progress.textContent = '0%';

        reader = new FileReader();
        reader.onerror = errorHandler;
        reader.onprogress = updateProgress;
        reader.onabort = function (e) {
            alert('File read cancelled');
        };
        reader.onloadstart = function (e) {
            document.getElementById('progress_bar').className = 'loading';
        };
        reader.onload = function (e) {
            // Ensure that the progress bar displays 100% at the end.
            progress.style.width = '100%';
            progress.textContent = '100%';
            setTimeout("document.getElementById('progress_bar').className='';", 2000);
        }

        // Read in the image file as a binary string.
        reader.readAsBinaryString(evt.target.files[0]);
    }

    document.getElementById('respaldo_doc').addEventListener('change', handleFileSelect, false);
    //End progress bar

</script>
@endsection