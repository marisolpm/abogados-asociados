@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .row{
        margin-bottom:10px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo 'Empleados' ?>
        <small><?php echo 'Editar' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/empleados">Empleados</a></li>
        <li class="active"><?php echo 'Editar' ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="" method="POST" id="formularioEditarPersona" >
                    <input id="token_editar" name="token_editar" type="hidden" value="{{ csrf_token() }}"/>
                    <input id="id_persona" name="id_persona" type="hidden" value="{{ $clientes[0]->o_prs_id }}"/>
                    <div class="box-body">

                        <!--                        <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="name" style="clear:both;"><?php echo 'profile' ?> <?php echo 'picture' ?></label>
                                                        <input type="file" name="img" value="<?php echo 'img' ?>" class="form-control">
                                                    </div>
                                                </div>-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" style="clear:both;">Nombre</label>
                                <input type="text" name="nombre" placeholder="Nombre" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_nombres}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paterno" style="clear:both;">Paterno</label>
                                <input type="text" name="paterno" placeholder="Paterno" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_paterno}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="materno" style="clear:both;">Materno</label>
                                <input type="text" name="materno" placeholder="Materno" class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_materno}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ci" style="clear:both;">C.I.</label>
                                <input type="text" name="ci" placeholder="C.I." class="form-control" autocomplete="off" value="{{$clientes[0]->o_prs_ci}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ci_expedido" style="clear:both;">Expedido</label>
                                <select name="ci_expedido" class="form-control">
                                    <option value="0">Seleccione lugar expedido</option>
                                    <option value="1">La Paz</option>
                                    <option value="2">Cochabamba</option>
                                    <option value="3">Santa Cruz</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dob" style="clear:both;">Fecha de nacimiento</label>
                                <input type="text" name="fecha_nacimiento" placeholder="YYYY-MM-DD" class="form-control datepicker" autocomplete="off" value="{{$clientes[0]->o_prs_fecha_nacimiento}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" style="clear:both;">Correo</label>
                                <input type="email" name="email" placeholder="ejemplo@ejemplo.com" class="form-control" value="{{$clientes[0]->o_prs_email}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Teléfono/Celular</label>
                                <input type="text" name="contacto" placeholder="XXXXXXX" class="form-control" value="{{$clientes[0]->o_prs_contacto}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact" style="clear:both;">Dirección</label>
                                <textarea name="direccion"  class="form-control" placeholder="Dirección">{{$clientes[0]->o_prs_direccion}}</textarea>
                            </div>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <a id="actualizar_persona" class="btn btn-primary">Actualizar</a>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>  

<script type="text/javascript">
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".txtarea").wysihtml5();
    });

    jQuery('.datepicker').datetimepicker({
        lang: 'en',
        i18n: {
            de: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do.", "Lu", "Ma", "Mi",
                    "Ju", "Vi", "Sa.",
                ]
            }
        },
        timepicker: false,
        format: 'Y-m-d'
    });

    $("#actualizar_persona").click(function () {

        var route = "/empleados/" + $("#id_persona").val();
        var token = $("#token_editar").val();
        swal({
            title: "¿Desea editar el registro?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, EDITAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioEditarPersona').serialize(),
                            success: function (data) {
                                swal(data.retorno_codigo, data.err_mensaje, "success");
                                window.location.href = "/empleados"
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    });

</script>

@endsection