@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>Mensajes
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="/cuentas">Mensajeria</a></li>
        <li class="active">Listado</li>
    </ol>
</section>

<section class="content" >
    <div class="row">

        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Mensajes</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo Mensaje</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped table-responsive" id="lts_mensajes">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Asunto</th>
                                        <th>Cliente</th>
                                        <th>Abogado</th>
                                        <th>Mensaje</th>
                                        <th>Fecha Envío</th>
                                        <th>Usuario Edición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVO MENSAJE</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioMensaje" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="id_mensaje" name="id_mensaje" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>

                                <div class="col-md-3">
                                    <label for="cliente" style="clear:both;">Cliente</label>
                                    <select name="cliente" id="cliente" class="form-control" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="abogado" style="clear:both;">Abogado</label>
                                    <select name="abogado" id="abogado" class="form-control" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="asunto" style="clear:both;">Asunto</label>
                                    <input type="text" name="asunto" id="asunto" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-12">
                                    <label for="mensaje" style="clear:both;">Mensaje</label>
                                    <textarea name="mensaje" id="mensaje" class="form-control" placeholder="Cuerpo del mensaje"></textarea>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <a  class="btn btn-success"  id="enviar_mensaje">ENVIAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">
                                        $(function () {

                                            $('.chzn').chosen();
                                            $('#cliente').select2();
                                            $('#abogado').select2();
                                            //$("#mensaje").Editor();
                                        });


                                        $(function () {
                                            $.ajax({
                                                url: '/personas/-1/2',
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#cliente').find('option').remove();
                                                    $('#cliente').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#cliente').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                    });

                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });

                                            $.ajax({
                                                url: '/personas/-1/1',
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#abogado').find('option').remove();
                                                    $('#abogado').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#abogado').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                    });

                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });


                                            $("#lts_mensajes").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/mensajesListado/-1/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE MENSAJES ENVIADOS',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE MENSAJES ENVIADOS',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE MENSAJES ENVIADOS',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_id", orderable: false},
                                                    {data: "o_asunto"},
                                                    {data: "o_nombre_cliente"},
                                                    {data: "o_nombre_abogado"},
                                                    {data: "o_mensaje"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });
                                        });

                                        $("#enviar_mensaje").click(function () {
                                            var token = $("#token_registrar").val();

                                            var route = "/mensajeria";
                                            swal({
                                                title: "¿Desea enviar el mensaje?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, ENVIAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'POST',
                                                            dataType: 'json',
                                                            data: $('#formularioMensaje').serialize(),
                                                            success: function (data) {
                                                                swal('MENSAJE : ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_mensajes').DataTable().ajax.reload();
                                                                document.getElementById('formularioMensaje').reset();
                                                                $('#cliente').prop('selectedIndex', 0).trigger("change");
                                                                $('#abogado').prop('selectedIndex', 0).trigger("change");
                                                            }, error: function (result) {
                                                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                            }
                                                        });
                                                    });
                                        });

                                        function eliminarMensaje(id) {
                                            var id_mensaje = id;
                                            var route = "mensajeria/" + id_mensaje;
                                            var token = $("#token_registrar").val();
                                            swal({
                                                title: "¿Desea eliminar mensaje ID: " + id_mensaje + "?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, ELIMINAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'DELETE',
                                                            dataType: 'json',
                                                            data: $('#formularioMensaje').serialize(),
                                                            success: function (data) {

                                                                swal('MENSAJE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_mensajes').DataTable().ajax.reload();
                                                                document.getElementById('formularioMensaje').reset();
                                                            }, error: function (result) {
                                                                swal("Opss..!", "El mensaje posee registro en otras tablas, no es posible eliminar!", "error");
                                                            }
                                                        });
                                                    });
                                        }
                                        function reenviarMensaje(id) {
                                            var route = "/mensajeria/" + id;
                                            swal({
                                                title: "¿Desea Reenviar el mensaje de ID: " + id + "?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, REENVIAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            type: 'GET',
                                                            success: function (data) {
                                                                swal('MENSAJE : ' + data.retorno_codigo, data.err_mensaje, "success");

                                                            }, error: function (result) {
                                                                swal("Opss..!", "No se pudo reenviar, intente mas tarde!", "error");
                                                            }
                                                        });
                                                    });
                                        }


</script>
@endsection