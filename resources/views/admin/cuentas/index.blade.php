@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>Cuentas
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="/cuentas">Cuentas</a></li>
        <li class="active">Listado</li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        @include('admin.procesos.partial.modalUploadFile')
        @include('admin.procesos.partial.modalViewFile')
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Cuentas</span>

                    </a>
                </li>
                <li class="" id="tabDetalle">
                    <a data-toggle="tab" href="#detalle">
                        <i class="fa fa-eye"></i> <span>Detalle Cuenta (Pagos)</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nueva/Editar Cuenta</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped table-responsive" id="lts_cuentas">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Cuenta</th>
                                        <th>Tipo Cuenta</th>
                                        <th>Cliente</th>
                                        <th>Proceso</th>
                                        <th>Descripcion</th>
                                        <th>Fecha Ultima Edición</th>
                                        <th>Usuario Edición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>


                <div class="tab-pane fade" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Proceso</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Nombre</th><td>&nbsp;</td><td id="nombreP"></td></tr>
                                            <tr><th>Fecha Inicio</th><td>&nbsp;</td><td><i class="fa fa-clock-o"></i> <span id="fechaIP"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Tipo Proceso</th><td>&nbsp;</td><td id="tipoP"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoP"></td></tr>
                                            <tr><th>Descripción</th><td>&nbsp;</td><td id="descripcionP"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroP"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoP"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Cliente</th><td>&nbsp;</td><td id="clienteP"></td>
                                                <th>Persona Contraria</th><td>&nbsp;</td><td id="contrarioP"></td>
                                            </tr>
                                            <tr>
                                                <th>Posición Cliente</th><td>&nbsp;</td><td id="posicionP"></td>
                                                <th>Involucrados</th><td>&nbsp;</td><td id="involucradosP"></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h4>Documentos del Proceso</h4>
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-xs-12">
                                        <div class="btn-group pull-right">
                                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                                <input id="proceso" name="proceso" type="hidden" value=""/>
                                                <input id="tipo_registro_proceso" name="tipo_registro_proceso" type="hidden" value="1"/>

                                                <div class="col-md-12">
                                                    <a  class="btn btn-primary" onclick="SubirDocumento(1)"><i class="fa fa-plus-circle"></i> Subir Documento</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_proceso_documento">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre Documento</th>
                                                <th>Tipo</th>
                                                <th>Enlace</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Registro</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVA/EDITAR CUENTA</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioCuenta" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="id_cuenta" name="id_cuenta" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>
                                <div class="col-md-3">
                                    <label for="numero_cuenta" style="clear:both;">Número de Cuenta</label>
                                    <input type="text" name="numero_cuenta" id="numero_cuenta" class="form-control" autocomplete="off" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="posicion" style="clear:both;">Tipo Cuenta</label>
                                    <select name="tipo_cuenta" id="tipo_cuenta" class="form-control">
                                        <option value="1">Anticipo</option>
                                        <option value="2">Efectivo</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="cliente" style="clear:both;">Cliente</label>
                                    <select name="cliente" id="cliente" class="form-control" style="width: 100%" onchange="procesosCliente(this)">

                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="proceso" style="clear:both;">Proceso Judicial</label>
                                    <select name="procesos" id="procesos" class="form-control">

                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="descripcion" style="clear:both;">Descripción</label>
                                    <textarea name="descripcion" id="descripcion" class="form-control" placeholder="Descripción proceso"></textarea>
                                </div>

                                <div class="col-md-12">
                                    <a  class="btn btn-success"  id="registrar_cuenta">REGISTRAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">
                                        $(function () {

                                            $('.chzn').chosen();
                                            $('#cliente').select2();

                                        });

                                        var id_cliente = $('#cliente').val();
                                        function cargando() {
                                            var texto = $("<div>", {
                                                text: "CARGANDO....",
                                                id: "myEstilo",
                                                css: {
                                                    "font-size": "30px",
                                                    "position": "relative",
                                                    "width": "500px",
                                                    "height": "300px",
                                                    "left": "180px",
                                                    "top": "50px"
                                                },
                                                fontawesome: "fa fa-spinner"
                                            });
                                            $.LoadingOverlay("show", {
                                                custom: texto,
                                                //fontawesome : "fa fa-spinner",
                                                color: "rgba(255, 255, 255, 0.8)",
                                            });
                                        }
                                        function SubirDocumento(pro_id) {
                                            jQuery('#myUploadFile').modal('show');
                                        }
                                        function resetear() {
                                            document.getElementById('formularioCuenta').reset();
                                            $('#tipo_registro').val(1);
                                            var path = 'datosCuenta';
                                            $.get(path, function (res) {
                                                $('#numero_cuenta').val(res[0].o_numero_cuenta);
                                                $('#id_cuenta').val(res[0].o_id_cuenta);
                                            });
                                        }
                                        function detalleProceso(id)
                                        {
                                            $('.nav-tabs a[href="#detalle"]').tab('show');
                                            var path = '/procesosListado/' + id + '/-1';
                                            $.get(path, function (res) {
                                                $('#id_proceso').val(id);
                                                console.log(res);
                                                document.getElementById("nombreP").innerHTML = res.data[0].o_proceso;
                                                document.getElementById("fechaIP").innerHTML = res.data[0].o_fecha_inicio;
                                                document.getElementById("tipoP").innerHTML = res.data[0].o_tipo_proceso;
                                                document.getElementById("estadoP").innerHTML = res.data[0].o_estado_proceso;
                                                document.getElementById("descripcionP").innerHTML = res.data[0].o_descripcion;
                                                document.getElementById("registroP").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoP").innerHTML = res.data[0].o_modificado;
                                                document.getElementById("clienteP").innerHTML = res.data[0].o_nombre_cliente;
                                                document.getElementById("contrarioP").innerHTML = res.data[0].o_contrario;
                                                document.getElementById("posicionP").innerHTML = res.data[0].o_nombre_posicion;
                                                document.getElementById("involucradosP").innerHTML = res.data[0].o_involucrados;
                                            });

                                            $('#lts_proceso_documento').DataTable({
                                                destroy: true,
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/procesosListado/' + id,
                                                columns: [
                                                    {data: "o_nombre_documento", orderable: false},
                                                    {data: "o_id_tipo_documento"},
                                                    {data: "o_documentos"},
                                                    {data: "o_registrado"},
                                                    {data: "o_usuario_reg"},
                                                    {data: "o_accion"}
                                                ],

                                                "language": {
                                                    "url": "/languaje"
                                                },
                                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                            });

                                        }
                                        function mostrarCuenta(id)
                                        {
                                            $('.nav-tabs a[href="#nuevo"]').tab('show');
                                            $('#tipo_registro').val(2);
                                            var path = '/cuentasListado/' + id + '/-1';
                                            $.get(path, function (res) {
                                                $('#tipo_registro').val(2);
                                                $('#id_cuenta').val(res.data[0].o_id);
                                                $('#proceso').val(id);
                                                $('#cliente').select2("val", res.data[0].o_id_cliente);
                                                $('#numero_cuenta').val(res.data[0].o_numero_cuenta);
                                                $('#tipo_cuenta').val(res.data[0].o_id_tipo_cuenta);
                                                $('#procesos').val(res.data[0].o_id_proceso);
                                                $('#descripcion').val(res.data[0].o_descripcion);
                                            });
                                        }

                                        function eliminarCuenta(id) {
                                            var id_cuenta = id;
                                            var route = "cuentas/" + id_cuenta;
                                            var token = $("#token_registrar").val();
                                            swal({
                                                title: "¿Desea eliminar la cuenta ID: " + id_cuenta + "?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, ELIMINAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'DELETE',
                                                            dataType: 'json',
                                                            data: $('#formularioProceso').serialize(),
                                                            success: function (data) {

                                                                swal('CUENTA N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_cuentas').DataTable().ajax.reload();
                                                                document.getElementById('formularioCuenta').reset();
                                                            }, error: function (result) {
                                                                swal("Opss..!", "La cuenta posee registro en otras tablas, no es posible eliminar!", "error");
                                                            }
                                                        });
                                                    });
                                        }

                                        function procesosCliente(id_cliente) {
                                            $.ajax({
                                                url: '/procesosListado/-1/' + id_cliente.value,
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#procesos').find('option').remove();
                                                    $('#procesos').append('<option value=0>ELIGA PROCESO</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#procesos').append('<option value= ' + z.o_id + '>' + z.o_proceso + '</option>');
                                                    });
                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });
                                        }

                                        $(function () {
                                            $.ajax({
                                                url: '/personas/-1/2',
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#cliente').find('option').remove();
                                                    $('#cliente').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#cliente').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                    });

                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });
                                            $("#lts_cuentas").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/cuentasListado/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE CUENTAS',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE CUENTAS',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE CUENTAS',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_id", orderable: false},
                                                    {data: "o_numero_cuenta"},
                                                    {data: "o_nombre_tipo_cuenta"},
                                                    {data: "o_nombre_cliente"},
                                                    {data: "o_nombre_proceso"},
                                                    {data: "o_descripcion"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });

                                        });
                                        $("#registrar_cuenta").click(function () {
                                            var tipo_registro = $('#tipo_registro').val();
                                            var token = $("#token_registrar").val();
                                            if (tipo_registro == 1) {
                                                var route = "/cuentas";
                                                swal({
                                                    title: "¿Desea registrar la cuenta?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, REGISTRAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formularioCuenta').serialize(),
                                                                success: function (data) {
                                                                    swal('CUENTA N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_cuentas').DataTable().ajax.reload();
                                                                    document.getElementById('formularioCuenta').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            } else {
                                                var route = "/cuentas";
                                                swal({
                                                    title: "¿Desea actualizar la cuenta?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formularioCuenta').serialize(),
                                                                success: function (data) {
                                                                    swal('CUENTA N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_cuentas').DataTable().ajax.reload();
                                                                    document.getElementById('formularioCuenta').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            }
                                        });
                                        $("#registrar_respaldo").click(function () {

                                            var route = "/registrarRespaldo";
                                            var token = $("#token_act").val();

                                            swal({
                                                title: "¿Subir documento de respaldo?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, subir!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {

                                                        var file_data = $('#respaldo_doc').prop('files')[0];
                                                        var form_data = new FormData();
                                                        form_data.append('respaldo_doc', file_data);
                                                        form_data.append('id_proceso', $('#id_proceso').val());
                                                        form_data.append('nombre_documento', $('#nombre_documento').val());
                                                        form_data.append('observacion_documento', $('#observacion_documento').val());
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'POST',
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            dataType: 'json',
                                                            data: form_data,
                                                            success: function (data) {
                                                                console.log(data);
                                                                $("#myUploadFile").modal('toggle');
                                                                //swal(data.unimed_codigo, data.err_mensaje, "success");
                                                                $('#lts_proceso_documento').DataTable().ajax.reload();

                                                            }, error: function (result) {
                                                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                            }
                                                        });

                                                    });

                                        });

                                        //Progress Bar
                                        function _(el) {
                                            return document.getElementById(el);
                                        }

                                        function uploadFile() {
                                            var file = _("man_respaldo_doc_act").files[0];
                                            console.log(file);
                                            // alert(file.name+" | "+file.size+" | "+file.type);
                                            var formdata = new FormData();
                                            formdata.append("man_respaldo_doc_act", file);
                                            var ajax = new XMLHttpRequest();
                                            ajax.upload.addEventListener("progress", progressHandler, false);
                                            ajax.addEventListener("load", completeHandler, false);
                                            ajax.addEventListener("error", errorHandler, false);
                                            ajax.addEventListener("abort", abortHandler, false);
                                            ajax.open("POST", ""); // http://www.developphp.com/video/JavaScript/File-Upload-Progress-Bar-Meter-Tutorial-Ajax-PHP
                                            //use file_upload_parser.php from above url
                                            ajax.send(formdata);
                                        }

                                        function progressHandler(event) {
                                            _("loaded_n_total").innerHTML = "Listo " + event.loaded + " bytes de " + event.total;
                                            var percent = (event.loaded / event.total) * 100;
                                            _("progressBar").value = Math.round(percent);
                                            _("status").innerHTML = Math.round(percent) + "% cargando... espere un momento";
                                        }

                                        function completeHandler(event) {
                                            _("status").innerHTML = event.target.responseText;
                                            _("progressBar").value = 0; //wil clear progress bar after successful upload
                                        }

                                        function errorHandler(event) {
                                            _("status").innerHTML = "Fallo la subida";
                                        }

                                        function abortHandler(event) {
                                            _("status").innerHTML = "Subida Cancelada";
                                        }
                                        //End progress bar

</script>
@endsection