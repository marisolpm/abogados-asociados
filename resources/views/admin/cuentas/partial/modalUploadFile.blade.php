<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUploadFile" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    x
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Subir Documento de Respaldo
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                    <form id='formulario_actualizar_mantenimiento' enctype='multipart/form-data' action="POST">
                        <input id="token_act" name="csrf-token_act" type="hidden" value="{{ csrf_token() }}">
                        <input type="hidden" id="id_proceso" name="id_proceso">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">Datos del Documento de Respaldo</legend>

                                        <div class="col-md-12" id="div_respaldo_documento" style="display: yes">
                                            <label>
                                                Respaldo del Documento:
                                            </label>
                                            <div class="form-group col-md-12">
                                                <center><span class="btn btn-primary btn-file">
                                                        SUBIR DOCUMENTO
                                                        <input onchange="uploadFile()" class="form-control" id="respaldo_doc" type="file" name="respaldo_doc" value="TOMAR FOTO / SUBIR IMÁGEN" />
                                                    </span>
                                                    <progress id="progressBar" value="0" max="100" style="width:80%;"></progress>
                                                    <h4 id="status"></h4>
                                                    <p id="loaded_n_total"></p>
                                                </center>

                                            </div>
                                        </div>
                                        <div class="col-sm-12" id="div_observacion_proceso" style="display: yes">
                                            <label>
                                                Nombre del Documento:
                                            </label>
                                            <span class="block input-icon input-icon-right">
                                                <input id="nombre_documento" name="nombre_documento" class="form-control">
                                            </span>
                                        </div>
                                        <div class="col-sm-12" id="div_observacion_proceso" style="display: yes">
                                            <label>
                                                Observación:
                                            </label>
                                            <span class="block input-icon input-icon-right">
                                                <textarea placeholder = 'Haga una breve observación' class = 'form-control' name="observacion_documento" id='observacion_documento' rows='2'></textarea>
                                            </span>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                <a id='registrar_respaldo' class='btn btn-primary' style= 'background:#57BC90'>REGISTRAR</a>


            </div>
        </div>
    </div>
</div>
</div>
