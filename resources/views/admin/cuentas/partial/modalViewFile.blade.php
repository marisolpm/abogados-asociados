<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myViewMantenimiento" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Documento de respaldo</h4>
                </div>
                <div class="modal-body">
                    <img src="" width="100%" id="imagen">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                </div>
            </div>
        </div>
    </div>
</div>