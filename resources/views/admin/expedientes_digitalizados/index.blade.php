@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }

    #progress_bar {
        margin: 10px 0;
        padding: 3px;
        border: 1px solid #000;
        font-size: 14px;
        clear: both;
        opacity: 0;
        -moz-transition: opacity 1s linear;
        -o-transition: opacity 1s linear;
        -webkit-transition: opacity 1s linear;
    }
    #progress_bar.loading {
        opacity: 1.0;
    }
    #progress_bar .percent {
        background-color: #99ccff;
        height: auto;
        width: 0;
    }

</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Expedientes Digitalizados
        <small><?php echo 'Ver' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/expedientes">Expedientes Digitalizados</a></li>
        <li class="active"> Ver</li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"> </h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Expedientes Digitalizados</span>

                    </a>
                </li>

                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo/Editar Expediente</span>
                    </a>
                </li>
                <li class="" id="tabDigitalizar">
                    <a data-toggle="tab" href="#digitalizar">
                        <i class="fa fa-eye"></i> <span>Digitalizar Expediente</span>

                    </a>
                </li>

            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body table-responsive">
                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped" id="lts_expedientes">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Expediente N°</th>
                                        <th>Cliente</th>
                                        <th>Tipo Expediente</th>
                                        <th>Enlace</th>
                                        <th>Fecha Registro</th>
                                        <th>Usuario Registro</th>
                                        <th></th>

                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>


                <div class="tab-pane fade" id="digitalizar">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Digitalizar Expediente</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-3">

                                <div class="table-responsive col-md-12">
                                    <table>
                                        <thead>
                                            <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoE"></td></tr>
                                            <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive col-md-12">
                                    <table>
                                        <thead>
                                            <tr><th>Materia</th><td>&nbsp;</td><td id="materiaE"></td></tr>
                                            <tr><th>Tipo</th><td>&nbsp;</td><td id="tipoE"></td></tr>
                                            <tr><th></th><td>&nbsp;</td><td id="contingenciaE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-5">

                                <div class="table-responsive col-md-12">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroE"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoE"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <form enctype="multipart/form-data" method="POST" id="formularioExpedienteD"  >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="cliente" name="cliente" type="hidden"/>    
                                <input id="expediente" name="expediente" type="hidden" value=""/>    
                                <input id="url" name="url" type="hidden"/> 

                                <div class="col-md-12">
                                    <label for="archivo" style="clear:both;">Documento</label>
                                    <input type="file" name="archivo" id="archivo" class="form-control" autocomplete="off" onchange="ExtraerTexto()";>
                                    <a class="btn btn-primary" onclick="executeProcess()">DIGITALIZAR</a>
                                </div>
                                <div id="sending" class="col-md-12" style="display:yes;">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-progress="0" style="width: 0%;">
                                            0%
                                        </div>
                                    </div>
                                    <div class="counter-sending">
                                    (Procesando pàgina <span id="done">0</span> de <span id="total">0</span>)
                                    </div>

                                    <div class="execute-time-content">
                                        Tiempo transcurrido: <span class="execute-time">0 segundos</span>
                                    </div>

                                    <div class="end-process" style="display:none;">
                                        <div class="alert alert-success">El proceso ha sido completado.</div>
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <label for="observaciones" style="clear:both;">Texto extraido</label>
                                    <textarea name="textoExtraido" id="textoExtraido" class="form-control" placeholder="Observaciones/Comentario" rows="40"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <a  class="btn btn-success"  id="registrar_expedienteD">REGISTRAR</a>
                                    <!--<button id="descargar_word" class="btn btn-info"  id="descargar_expediente_word" disabled> <i class="fa fa-file-word-o"> DESCARGAR</i></button>-->
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>



                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">

                        <div class="panel-body">
                            <form enctype="multipart/form-data" method="POST" id="formularioExpediente"  >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="cliente" name="cliente" type="hidden"/>    
                                <input id="expediente" name="expediente" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value="1"/> 
                                <div class="col-md-4">
                                    <label for="cliente" style="clear:both;">Cliente</label>
                                    <select name="id_cliente"  id="id_cliente" class="form-control" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="id_expediente" style="clear:both;">ID Expediente</label>
                                    <input type="text" name="id_expediente" id="id_expediente" placeholder="ID Expediente" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label for="numero" style="clear:both;">Expediente N°</label>
                                    <input type="text" name="numero" id="numero" placeholder="Expediente N°" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label for="estado" style="clear:both;">Tipo Expediente</label>
                                    <select name="tipo" id="tipo" class="form-control">
                                        <option value="0">Seleccione Tipo</option>
                                        <option value="1">Judicial</option>
                                        <option value="2">Administrativo</option>
                                        <option value="3">Policial</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="materia" style="clear:both;">Materia</label>
                                    <select name="materia" id="materia" class="form-control">
                                        <option value="0">Seleccione Materia</option>
                                        <option value="1">Postulatoria</option>
                                        <option value="2">Probatoria</option>
                                        <option value="3">Expositiva</option>
                                        <option value="4">Consultiva</option>
                                        <option value="5">Juicio</option>

                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <a  class="btn btn-success"  id="registrar_expediente">REGISTRAR</a>

                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>  
<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUploadFile" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    x
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Subir Word Modificado
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                    <form>
                        <input id="token_respaldo" name="token_respaldo" type="hidden" value="{{ csrf_token() }}">
                        <input type="hidden" id="id_expediente" name="id_expediente">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"></legend>
                                        <div class="col-md-12" id="div_respaldo_documento" style="display: yes">
                                            <div class="form-group col-md-12">
                                                <center><span class="btn btn-primary btn-file">
                                                        ABRÍR DOCUMENTO
                                                        <input class="form-control" id="respaldo_doc" type="file" name="respaldo_doc" value="TOMAR FOTO / SUBIR IMÁGEN" />
                                                    </span>
                                                    <div id="progress_bar"><div class="percent">0%</div></div>
                                                </center>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                <a id='subir_word' class='btn btn-primary' style= 'background:#57BC90'>SUBÍR</a>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
                                        function executeProcess() {
                                            var token = $("#token_registrar").val();

//                                            if (offset == 0) {
//                                                $('#start_form').hide();
//                                                $('#sending').show();
//                                                $('#sended').text(0);
//                                                $('#total').text($('#total_comments').val());
//
//                                                //reset progress bar
//                                                $('.progress-bar').css('width', '0%');
//                                                $('.progress-bar').text('0%');
//                                                $('.progress-bar').attr('data-progress', '0');
//                                            }

                                            $.ajax({
                                                type: 'POST',
                                                dataType: "json",
                                                url: "/extraeTextoP",
                                                headers: {'X-CSRF-TOKEN': token},
                                                data: {
                                                    id_process: 1
                                                },
                                                success: function (response) {
                                                    console.log(response);
                                                    $('.progress-bar').css('width', response.percentage + '%');
                                                    $('.progress-bar').text(response.percentage + '%');
                                                    $('.progress-bar').attr('data-progress', response.percentage);

                                                    $('#done').text(response.executed);
                                                    $('#total').text(response.total);
                                                    $('.execute-time').text(response.execute_time);

                                                    if (response.percentage === 100) {
                                                        $('.end-process').show();
                                                        $("#textoExtraido").text(response.texto);
                                                    } else {
                                                        executeProcess();
                                                    }
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    if (textStatus == 'parsererror') {
                                                        textStatus = 'Technical error: Unexpected response returned by server. Sending stopped.';
                                                    }
                                                    alert(textStatus);
                                                }
                                            });
                                        }
                                        function SubirDocumento(id) {
                                            $("#id_expediente").val(id);
                                            jQuery('#myUploadFile').modal('show');
                                        }


                                        function ExtraerTexto() {

                                            var route = "/extraeTexto";
                                            var token = $("#token_registrar").val();

                                            var file_data = $('#archivo').prop('files')[0];
                                            var form_data = new FormData();
                                            form_data.append('archivo', file_data);
                                            form_data.append('id', $("#expediente").val());
                                            $.ajax({
                                                url: route,
                                                headers: {'X-CSRF-TOKEN': token},
                                                type: 'POST',
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                dataType: 'json',
                                                data: form_data,
                                                success: function (data) {
//                                                    executeProcess();
//                                                    console.log(data.url);
//                                                    $("#textoExtraido").text(data.texto);
//                                                    $("#url").val(data.url);

                                                }, error: function (result) {
                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                }
                                            });

                                        }

                                        function mostrarDigitalizado(id) {

                                            var route = "/verExpedientesDigitalizado/" + id;
                                            var token = $("#token_registrar").val();

                                            $.ajax({
                                                url: route,
                                                type: 'GET',
                                                success: function (data) {

                                                }, error: function (result) {
                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                }
                                            });

                                        }
                                        function resetear() {
                                            document.getElementById('formularioExpediente').reset();
                                            $('#tipo_registro').val(1);
                                        }

                                        function mostrarExpediente(id)
                                        {
                                            $('.nav-tabs a[href="#nuevo"]').tab('show');
                                            $('#tipo_registro').val(2);
                                            var path = '/expedientesDListado/' + id + '/-1';
                                            $.get(path, function (res) {
                                                $('#expediente').val(id);
                                                $('#id_expediente').val(res.data[0].o_id_expediente);
                                                $('#numero').val(res.data[0].o_nro_expediente);
                                                $('#id_cliente').select2("val", res.data[0].o_id_cliente);
                                                $('#materia').val(res.data[0].o_tipo_materia);
                                                $('#tipo').val(res.data[0].o_tipo_expediente);
                                            });

                                        }

                                        function digitalizarExpediente(id)
                                        {
                                            $('.nav-tabs a[href="#digitalizar"]').tab('show');
                                            $('#tipo_registro').val(2);
                                            var path = '/expedientesDListado/' + id + '/-1';
                                            $.get(path, function (res) {
                                                $("#expediente").val(res.data[0].o_id);
                                                document.getElementById("codigoE").innerHTML = res.data[0].o_id_expediente;
                                                document.getElementById("expedienteE").innerHTML = res.data[0].o_nro_expediente;
                                                document.getElementById("materiaE").innerHTML = res.data[0].o_nombre_tipo_materia;
                                                document.getElementById("tipoE").innerHTML = res.data[0].o_nombre_tipo_expediente;
                                                document.getElementById("registroE").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoE").innerHTML = res.data[0].o_modificado;
                                            });

                                        }

                                        $(function () {

                                            var id_cliente = $('#cliente').val();

                                            $("#id_cliente").select2();

                                            $.ajax({
                                                url: '/personas/-1/2',
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#id_cliente').find('option').remove();
                                                    $('#id_cliente').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#id_cliente').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                    });

                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });

                                            $("#lts_expedientes").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/expedientesDListado/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE EXPEDIENTES DIGITALIZADOS',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE EXPEDIENTES DIGITALIZADOS',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE EXPEDIENTES DIGITALIZADOS',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_id_expediente", orderable: false},
                                                    {data: "o_nro_expediente"},
                                                    {data: "o_nombre_cliente"},
                                                    {data: "o_nombre_tipo_expediente"},
                                                    {data: "o_escaneado"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });

                                            $("#registrar_expedienteD").click(function () {
                                                var id_expediente = $("#expediente").val();
                                                var token = $("#token_registrar").val();
                                                var file_data = $('#archivo').prop('files')[0];
                                                var form_data = new FormData();
                                                form_data.append('archivo', file_data);
                                                form_data.append('id_expediente', $('#expediente').val());
                                                form_data.append('textoExtraido', $('#textoExtraido').val());

                                                var route = "/digitalizacion/" + id_expediente;


                                                swal({
                                                    title: "¿Desea registrar el documento digitalizado?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, REGISTRAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'PUT',
                                                                dataType: 'json',
                                                                data: $("#formularioExpedienteD").serialize(),
                                                                success: function (data) {

                                                                    swal('EXPEDIENTE', 'DIGITALIZADO CORRECTAMENTE', "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_expedientes').DataTable().ajax.reload();
                                                                    document.getElementById('formularioExpediente').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            });

                                            $("#registrar_expediente").click(function () {
                                                var tipo_registro = $('#tipo_registro').val();
                                                var token = $("#token_registrar").val();
                                                var file_data = $('#archivo').prop('files')[0];
                                                var form_data = new FormData();
                                                form_data.append('archivo', file_data);
                                                form_data.append('numero', $('#numero').val());
                                                form_data.append('tipo', $('#tipo').val());
                                                form_data.append('id_expediente', $('#id_expediente').val());
                                                form_data.append('materia', $('#materia').val());
                                                form_data.append('id_cliente', $('#id_cliente').val());

                                                if (tipo_registro == 1) {
                                                    var route = "/digitalizacion";

                                                    swal({
                                                        title: "¿Desea registrar el expediente?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, REGISTRAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                    },
                                                            function (isConfirm) {
                                                                if (!isConfirm)
                                                                    return;
                                                                $.ajax({
                                                                    url: route,
                                                                    headers: {'X-CSRF-TOKEN': token},
                                                                    type: 'POST',
                                                                    dataType: 'json',
                                                                    data: $("#formularioExpediente").serialize(),
                                                                    success: function (data) {
                                                                        swal('EXPEDIENTE', 'REGISTRADO CORRECTAMENTE', "success");
                                                                        $('.nav-tabs a[href="#lista"]').tab('show');
                                                                        $('#lts_expedientes').DataTable().ajax.reload();
                                                                        document.getElementById('formularioExpediente').reset();
                                                                    }, error: function (result) {
                                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                    }
                                                                });
                                                            });
                                                } else {
                                                    var expediente = $('#expediente').val();
                                                    var route = "/digitalizacion/" + expediente;

                                                    swal({
                                                        title: "¿Desea actualizar el expediente?",
                                                        text: "Se sugiere revise los datos antes de proceder",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                        cancelButtonText: "No, revisar",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: true,
                                                        showLoaderOnConfirm: true
                                                    },
                                                            function (isConfirm) {
                                                                if (!isConfirm)
                                                                    return;
                                                                $.ajax({
                                                                    url: route,
                                                                    headers: {'X-CSRF-TOKEN': token},
                                                                    type: 'PUT',
                                                                    dataType: 'text',
                                                                    data: $('#formularioExpediente').serialize(),
                                                                    success: function (data) {
                                                                        console.log(data);
                                                                        swal('EXPEDIENTE', 'ACTUALIZADO CORRECTAMENTE', "success");
                                                                        $('.nav-tabs a[href="#lista"]').tab('show');
                                                                        $('#lts_expedientes').DataTable().ajax.reload();
                                                                        document.getElementById('formularioExpediente').reset();
                                                                    }, error: function (result) {
                                                                        swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                    }
                                                                });
                                                            });
                                                }
                                            });

                                        }
                                        );

                                        //Progress Bar
                                        var reader;
                                        var progress = document.querySelector('.percent');

                                        function abortRead() {
                                            reader.abort();
                                        }

                                        function errorHandler(evt) {
                                            switch (evt.target.error.code) {
                                                case evt.target.error.NOT_FOUND_ERR:
                                                    alert('File Not Found!');
                                                    break;
                                                case evt.target.error.NOT_READABLE_ERR:
                                                    alert('File is not readable');
                                                    break;
                                                case evt.target.error.ABORT_ERR:
                                                    break; // noop
                                                default:
                                                    alert('An error occurred reading this file.');
                                            }
                                            ;
                                        }

                                        function updateProgress(evt) {
                                            // evt is an ProgressEvent.
                                            if (evt.lengthComputable) {
                                                var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
                                                // Increase the progress bar length.
                                                if (percentLoaded < 100) {
                                                    progress.style.width = percentLoaded + '%';
                                                    progress.textContent = percentLoaded + '%';
                                                }
                                            }
                                        }

                                        function handleFileSelect(evt) {
                                            // Reset progress indicator on new file selection.
                                            progress.style.width = '0%';
                                            progress.textContent = '0%';

                                            reader = new FileReader();
                                            reader.onerror = errorHandler;
                                            reader.onprogress = updateProgress;
                                            reader.onabort = function (e) {
                                                alert('File read cancelled');
                                            };
                                            reader.onloadstart = function (e) {
                                                document.getElementById('progress_bar').className = 'loading';
                                            };
                                            reader.onload = function (e) {
                                                // Ensure that the progress bar displays 100% at the end.
                                                progress.style.width = '100%';
                                                progress.textContent = '100%';
                                                setTimeout("document.getElementById('progress_bar').className='';", 2000);
                                            }

                                            // Read in the image file as a binary string.
                                            reader.readAsBinaryString(evt.target.files[0]);
                                        }

                                        document.getElementById('respaldo_doc').addEventListener('change', handleFileSelect, false);
                                        //End progress bar


                                        $("#subir_word").click(function () {

                                            var route = "/subirWord";
                                            var token = $("#token_respaldo").val();
                                            var file_data = $('#respaldo_doc').prop('files')[0];
                                            var form_data = new FormData();
                                            form_data.append('respaldo_doc', file_data);
                                            form_data.append('id_expediente', $('#id_expediente').val());

                                            swal({
                                                title: "¿Subir word modificado?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, subir!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'POST',
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            data: form_data,
                                                            success: function (data) {
                                                                swal('RESPALDO', data.Mensaje, "success");
                                                                $("#myUploadFile").modal('toggle');
                                                                $('#lts_expedientes').DataTable().ajax.reload();

                                                            }, error: function (result) {
                                                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                            }
                                                        });

                                                    });

                                        });
</script>
@endsection