@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Recibos
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="/recibos">Recibos</a></li>
        <li class="active">Listado</li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        @include('admin.recibos.partial.modalViewFile')
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Recibos</span>

                    </a>
                </li>
                <!--                <li class="" id="tabDetalle">
                                    <a data-toggle="tab" href="#detalle">
                                        <i class="fa fa-eye"></i> <span>Detalle Recibo (Pagos)</span>
                
                                    </a>
                                </li>-->
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo/Editar Recibo</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped table-responsive" id="lts_recibo">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th># Recibo</th>
                                        <th>Cliente</th>
                                        <th>Cuenta</th>
                                        <th>Monto</th>
                                        <th>Glosa</th>
                                        <th>Fecha Ultima Edición</th>
                                        <th>Usuario Edición</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>


                <div class="tab-pane fade" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Proceso</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Nombre</th><td>&nbsp;</td><td id="nombreP"></td></tr>
                                            <tr><th>Fecha Inicio</th><td>&nbsp;</td><td><i class="fa fa-clock-o"></i> <span id="fechaIP"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Tipo Proceso</th><td>&nbsp;</td><td id="tipoP"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoP"></td></tr>
                                            <tr><th>Descripción</th><td>&nbsp;</td><td id="descripcionP"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroP"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoP"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Cliente</th><td>&nbsp;</td><td id="clienteP"></td>
                                                <th>Persona Contraria</th><td>&nbsp;</td><td id="contrarioP"></td>
                                            </tr>
                                            <tr>
                                                <th>Posición Cliente</th><td>&nbsp;</td><td id="posicionP"></td>
                                                <th>Involucrados</th><td>&nbsp;</td><td id="involucradosP"></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h4>Documentos del Proceso</h4>
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-xs-12">
                                        <div class="btn-group pull-right">
                                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                                <input id="proceso" name="proceso" type="hidden" value=""/>
                                                <input id="tipo_registro_proceso" name="tipo_registro_proceso" type="hidden" value="1"/>

                                                <div class="col-md-12">
                                                    <a  class="btn btn-primary" onclick="SubirDocumento(1)"><i class="fa fa-plus-circle"></i> Subir Documento</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_proceso_documento">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre Documento</th>
                                                <th>Tipo</th>
                                                <th>Enlace</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Registro</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVA/EDITAR RECIBO</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioRecibo" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="id_recibo" name="id_recibo" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>
                                <div class="col-md-3">
                                    <label for="numero_recibo" style="clear:both;">Número de Recibo</label>
                                    <input type="text" name="numero_recibo" id="numero_recibo" class="form-control" autocomplete="off" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="cliente" style="clear:both;">Cliente</label>
                                    <select name="cliente" id="cliente" class="form-control" onchange="procesosCliente(this)" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="cuenta" style="clear:both;">Cuenta</label>
                                    <select name="cuenta" id="cuenta" class="form-control">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="monto" style="clear:both;">Monto</label>
                                    <input type="text" id="monto" name="monto" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <label for="glosa" style="clear:both;">Glosa</label>
                                    <textarea name="glosa" id="glosa" class="form-control" placeholder="Glosa pago"></textarea>
                                </div>

                                <div class="col-md-12">
                                    <a  class="btn btn-success"  id="registrar_recibo">REGISTRAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">
                                        $(function () {

                                            $('.chzn').chosen();

                                        });

                                        var id_cliente = $('#cliente').val();
                                        function cargando() {
                                            var texto = $("<div>", {
                                                text: "CARGANDO....",
                                                id: "myEstilo",
                                                css: {
                                                    "font-size": "30px",
                                                    "position": "relative",
                                                    "width": "500px",
                                                    "height": "300px",
                                                    "left": "180px",
                                                    "top": "50px"
                                                },
                                                fontawesome: "fa fa-spinner"
                                            });
                                            $.LoadingOverlay("show", {
                                                custom: texto,
                                                //fontawesome : "fa fa-spinner",
                                                color: "rgba(255, 255, 255, 0.8)",
                                            });
                                        }
                                        function respaldoRecibo(pro_id) {
//                                            jQuery('#myViewRecibo').modal('show');
//                                            var pdfhtml = '';
//                                            pdfhtml = '<iframe src="imprimirRecibo/' + pro_id + '" width="100%" height="500" frameborder="0" allowtransparency="true"></iframe>';
//                                            $('#vistapdf').html(pdfhtml);
//                                            jQuery('#myModalpdf').modal('show');

                                            var ventana = window.open('imprimirRecibo/' + pro_id, 'PRINT', 'height=800,width=1200').print();

//                                            ventana.focus();
//                                            ventana.onload = function () {
//                                                ventana.print();
//                                                //ventana.close();
//                                            };
//                                            return true;
                                        }
                                        function resetear() {
                                            document.getElementById('formularioRecibo').reset();
                                            $('#tipo_registro').val(1);
                                            var path = 'datosRecibo';
                                            $.get(path, function (res) {
                                                $('#numero_recibo').val(res[0].o_numero_recibo);
                                                $('#id_recibo').val(res[0].o_id_recibo);
                                            });
                                        }
                                        function detalleProceso(id)
                                        {
                                            $('.nav-tabs a[href="#detalle"]').tab('show');
                                            var path = '/procesosListado/' + id + '/-1';
                                            $.get(path, function (res) {
                                                $('#id_proceso').val(id);
                                                console.log(res);
                                                document.getElementById("nombreP").innerHTML = res.data[0].o_proceso;
                                                document.getElementById("fechaIP").innerHTML = res.data[0].o_fecha_inicio;
                                                document.getElementById("tipoP").innerHTML = res.data[0].o_tipo_proceso;
                                                document.getElementById("estadoP").innerHTML = res.data[0].o_estado_proceso;
                                                document.getElementById("descripcionP").innerHTML = res.data[0].o_descripcion;
                                                document.getElementById("registroP").innerHTML = res.data[0].o_registrado;
                                                document.getElementById("modificadoP").innerHTML = res.data[0].o_modificado;
                                                document.getElementById("clienteP").innerHTML = res.data[0].o_nombre_cliente;
                                                document.getElementById("contrarioP").innerHTML = res.data[0].o_contrario;
                                                document.getElementById("posicionP").innerHTML = res.data[0].o_nombre_posicion;
                                                document.getElementById("involucradosP").innerHTML = res.data[0].o_involucrados;
                                            });

                                            $('#lts_proceso_documento').DataTable({
                                                destroy: true,
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/procesosListado/' + id,
                                                columns: [
                                                    {data: "o_nombre_documento", orderable: false},
                                                    {data: "o_id_tipo_documento"},
                                                    {data: "o_documentos"},
                                                    {data: "o_registrado"},
                                                    {data: "o_usuario_reg"},
                                                    {data: "o_accion"}
                                                ],
                                                responsive: true,
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                            });

                                        }
                                        function procesosCliente(id_cliente) {
                                            $.ajax({
                                                url: '/cuentasListado/-1/' + id_cliente.value,
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#cuenta').find('option').remove();
                                                    $('#cuenta').append('<option value=0>ELIGA CUENTA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#cuenta').append('<option value= ' + z.o_id + '>' + z.o_numero_cuenta + ' ->  ' + z.o_nombre_proceso + '</option>');
                                                    });
                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });

                                            $.ajax({
                                                url: '/procesosListado/-1/' + id_cliente.value,
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#procesos').find('option').remove();
                                                    $('#procesos').append('<option value=0>ELIGA PROCESO</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#procesos').append('<option value= ' + z.o_id + '>' + z.o_proceso + '</option>');
                                                    });
                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });
                                        }


                                        $(function () {
                                            $("#cliente").select2()
                                            $.ajax({
                                                url: '/personas/-1/2',
                                                type: 'GET',
                                                dataType: 'json',
                                                success: function (s)
                                                {
                                                    $('#cliente').find('option').remove();
                                                    $('#cliente').append('<option value=0>ELIGA UNA PERSONA</option>');
                                                    $(s.data).each(function (k, z) { // indice, valor
                                                        $('#cliente').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                                                    });

                                                },
                                                error: function ()
                                                {
                                                    alert('Ocurrio un error en el servidor ..');
                                                }
                                            });

                                            $("#lts_recibo").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/recibosListado/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE RECIBOS',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6, 7]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE RECIBOS',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4, 5, 6, 7]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE RECIBOS',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_id", orderable: false},
                                                    {data: "o_numero_recibo"},
                                                    {data: "o_nombre_cliente"},
                                                    {data: "o_nombre_cuenta"},
                                                    {data: "o_monto_pago"},
                                                    {data: "o_glosa_pago"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_recibo"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                responsive: true,
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });

                                        });
                                        $("#registrar_recibo").click(function () {
                                            var tipo_registro = $('#tipo_registro').val();
                                            var token = $("#token_registrar").val();
                                            if (tipo_registro == 1) {
                                                var route = "/recibos";
                                                swal({
                                                    title: "¿Desea registrar pago?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, REGISTRAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                data: $('#formularioRecibo').serialize(),
                                                                success: function (data) {
                                                                    swal('RECIBO N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_recibo').DataTable().ajax.reload();
                                                                    document.getElementById('formularioRecibo').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            } else {
                                                var expediente = $('#expediente').val();
                                                var route = "/expedientes/" + expediente;

                                                swal({
                                                    title: "¿Desea actualizar el expediente?",
                                                    text: "Se sugiere revise los datos antes de proceder",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "¡Sí, ACTUALIZAR!",
                                                    cancelButtonText: "No, revisar",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: true,
                                                    showLoaderOnConfirm: true
                                                },
                                                        function (isConfirm) {
                                                            if (!isConfirm)
                                                                return;
                                                            $.ajax({
                                                                url: route,
                                                                headers: {'X-CSRF-TOKEN': token},
                                                                type: 'PUT',
                                                                dataType: 'json',
                                                                data: $('#formularioExpediente').serialize(),
                                                                success: function (data) {

                                                                    swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                    $('.nav-tabs a[href="#lista"]').tab('show');
                                                                    $('#lts_expedientes').DataTable().ajax.reload();
                                                                    document.getElementById('formularioExpediente').reset();
                                                                }, error: function (result) {
                                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                                }
                                                            });
                                                        });
                                            }
                                        });

</script>
@endsection