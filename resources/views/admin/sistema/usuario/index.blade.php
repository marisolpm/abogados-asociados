@extends('layouts.partials.main')
@section('content')
@include('admin.sistema.usuario.partials.modalUpdate')
<section class="content-header">
    <h1>
        <?php echo 'Usuarios' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> <?php echo 'Dashboard' ?></a></li>
        <li class="active"><?php echo 'Usuarios' ?></li>
    </ol>
</section>

<section class="content">

    <input id="token_editar_c" name="token_editar_c" type="hidden" value="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body ">
                    <table class="table table-hover table-striped" id="lts-usuario">
                        <thead class="cf">
                            <tr>
                                <th>Nombre</th>
                                <th>Nombre Usuario</th>
                                <th>Modificado</th>
                                <th>Usuario Mod.</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function MostrarUsuario(id)
    {
        $('#myUserUpdate').modal();
        var route = "/usuarios/" + id;
        $.get(route, function (res) {
            console.log(res);
            $("#id_usuario").val(res[0].id);
            $("#nombre").val(res[0].nombre);
            $("#nombre_usuario").val(res[0].nombre_usuario);

        });
    }

    function EliminarUsuario(id) {
        var route = "/usuarios/" + id;
        var token = $("#token").val();
        swal({
            title: "Esta seguro de eliminar la persona?",
            text: "Presione ok para eliminar el registro de la base de datos!",
            type: "warning", showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar!",
            closeOnConfirm: false
        },
                function () {
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (data) {
                            $('#lts-usuario').DataTable().ajax.reload();
                            swal("Usuario!", "Fue eliminado correctamente!", "success");
                        },
                        error: function (result) {
                            swal("Opss..!", "El Usuario tiene registros en otras tablas!", "error")
                        }
                    });
                });
    }
    $(function () {

        $('#lts-usuario').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/usuarios/create",
            "columns": [
                {data: 'nombres', orderable: false},
                {data: 'usuario'},
                {data: 'modificado'},
                {data: 'usuario_mod'},
                {data: 'acciones', searchable: false},
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });


        $("#actualizar").click(function ()
        {
            var id = $("#id").val();
            var route = "usuarios/" + id;
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {usuario: $("#nombre_usuario").val(), id: $("#id_usuario").val()},
                success: function (data) {
                    console.log(data);
                    $("#myUserUpdate").modal('toggle');
                    swal("El Usuario!", data.Mensaje, "success");
                    $('#lts-usuario').DataTable().ajax.reload();

                }, error: function (result) {
                    console.log(result);
                    swal("Opss..!", "El Usuario no se puedo actualizar intente de nuevo!", "error")
                }
            });
        });



    });
    function resetearAcceso(id) {

        var route = "usuarios/" + id;
        var token = $("#token_editar_c").val();
        swal({
            title: "¿Desea resetear contraseña?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, RESETEAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioEditarC').serialize(),
                            success: function (data) {
                                console.log(data);
                                swal(data[0].retorno_codigo, data[0].err_mensaje, "success");

                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
</script>
@endsection
