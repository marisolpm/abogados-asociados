<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Modificar Grupo
                </h4>
            </div>
            <form id="actualizarGrupo">
                <div class="modal-body">

                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <input id="id" type="hidden">
                    <div class="form-group">
                        <label for="nombre" style="clear:both;">Nombre</label>
                        <input type="text" placeholder = 'Nombre de Grupo' class = 'form-control' name="grupo1" id="grupo1" >
                    </div>
                    <div class="form-group">
                        <label for="ruta" style="clear:both;">Ruta Imagen</label>
                        <input type = 'text' placeholder = 'Ruta de la imagen' class = 'form-control' name="imagen1" id="imagen1">
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                    <a title='Modificar' id='actualizar' class ='btn btn-primary'> Modificar</a>
                    
                </div>
            </form>
        </div>
    </div>
</div>