@extends('admin.app')

@section('htmlheader_title')
@endsection
@section('main-content')
 
{!! Form::open(array('id'=>'formulario','class'=>'')) !!}
<input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">

<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title" role="group">
         <h3>
         
            Accesos
            <small>
                <select id="filtrotrimestral" name="filtrotrimestral" placeholder="Elija el Rol" class="form-control" onchange="buscar()">
                    @foreach($rol as $vaciado_rol)
                    <option value="{{$vaciado_rol->rls_id}}">
                        {{$vaciado_rol->rls_rol}}
                    </option>
                    @endforeach
                </select>
            </small>
         </h3>
       </div>
      </section>
   </div>   
</div>
<div class="row">
           <div class="col-md-4">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box" id="no-more-tables">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Tabla opciones</h3>
                                </div>
                                <div class="box-body"> 
                                    <table class="table table-hover table-striped" style="width: 100% !important" id="lts-opc"  >
                                        <thead class="cf" >
                                            <tr>
                                              <th><i class="fa fa-check-square-o"></i></th>
                                              <th>Grupo</th>
                                              <th>Opción</th>
                                              <th>Contenido</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>       
                    </div>
                </section>
           </div>

           <div class="col-md-1">
                <section class="content">
                    <div class="row">
                       <div class="button_center_lzm">
                            <div style="margin-bottom: 8px">
                                <button class="btn btn-primary" name="btn_enviar" id="btn_agregar"  ><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                            <div >
                                <button class="btn btn-primary" name="btn_enviar" id="btn_retirar"><i class="fa fa-minus"></i> Retirar</button>
                            </div>
                       </div>
                    </div>
                </section>       
           </div>
           
           <div class="col-md-7">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box" id="no-more-tables">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Tabla accesos</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body"> 
                                  <table  class="table table-hover table-striped" style="width: 100% !important" id="lts-acc" >
                                  <thead class="cf">
                                    <tr>
                                      <th><i class="fa fa-check-square-o"></i></th>
                                      <th>rol</th>
                                      <th>Opción</th>
                                      <th>Contenido</th>
                                      <th>Registrado</th>
                                      <th>Modificado</th>
                                    </tr>
                                  </thead>
                                </table>
                                </div>
                            </div>
                        </div>       
                    </div>
                </section>
           </div>
       </div>
       
       @endsection


@push('scripts')
<script>
var id_r = $("#filtrotrimestral").val();
function buscar(){
      id_r = $("#filtrotrimestral").val();
      var url="listaAc/"+id_r;
       
        $('#lts-acc').DataTable({
            destroy:true,
            processing: true,
            serverSide: true,
            ajax: url,
            columns:[
                {data: 'asignacion'},
                {data: 'rls_rol'},
                {data: 'opc_opcion'},
                {data: 'opc_contenido'},
                {data: 'acc_registrado'},
                {data: 'acc_modificado'},
            ],

        "language": {
            "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
         $('#lts-opc').DataTable({
            destroy:true,
            processing: true,
            serverSide: true,
            ajax: "listaOp/"+id_r,
            columns:[
                {data: 'opciones'},
                {data: 'grp_grupo'},
                {data: 'opc_opcion'},
                {data: 'opc_contenido'},
            ],

        "language": {
            "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
    
    
}

function buscar1(valor){
      var url="listaAc/"+valor;
       
        $('#lts-acc').DataTable({
            destroy:true,
            processing: true,
            serverSide: true,
            ajax: url,
            columns:[
                {data: 'asignacion'},
                {data: 'rls_rol'},
                {data: 'opc_opcion'},
                {data: 'opc_contenido'},
                {data: 'acc_registrado'},
                {data: 'acc_modificado'},
            ],

        "language": {
            "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
         $('#lts-opc').DataTable({
            destroy:true,
            processing: true,
            serverSide: true,
            ajax: "listaOp/"+valor,
            columns:[
                {data: 'opciones'},
                {data: 'grp_grupo'},
                {data: 'opc_opcion'},
                {data: 'opc_contenido'},
            ],

        "language": {
            "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
    
    
}

 $('#btn_agregar').click(function(){
    var url="actualiza1";
    id_r = $("#filtrotrimestral").val();
    var token =$("#token").val();
    console.log(token);
   $.ajax({
    type:'POST',
    url:url,
    headers: {'X-CSRF-TOKEN': token},
    data:$('#formulario').serialize(),
    success:function(data){
        buscar1(id_r);
    }, 
    error: function(result) {
        console.log(result);
      }
   });
   return false;
 });

 $('#btn_retirar').click(function(){
    var url="actualiza2";
    id_r = $("#filtrotrimestral").val();
    var token =$("#token").val();
    console.log(token);
   $.ajax({
    type:'POST',
    url:url,
    headers: {'X-CSRF-TOKEN': token},
    data:$('#formulario').serialize(),
    success:function(data){
        buscar1(id_r);
    }, 
    error: function(result) {
        console.log(result);
      }
   });
   return false;
 });


 $(document).ready(function (){
     buscar();
  });

</script>
@endpush
      
