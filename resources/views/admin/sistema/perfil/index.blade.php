@extends('layouts.partials.main')
@section('content')

<script type="text/javascript">

</script>

<section class="content-header">
    <h1>
        <?php echo 'Perfil' ?>
        <small><?php echo 'Mi Perfil' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Perfil</a></li>
        <li class="active"> Mi Perfil</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active" id="tabDatos">
                    <a data-toggle="tab" href="#datos">
                        <i class="fa fa-user"></i> <span>Mis datos</span>

                    </a>
                </li>

                <li  class="left" id="tabContrasena">
                    <a data-toggle="tab" href="#contrasena" onclick="resetear()">
                        <i class="fa fa-key"></i> <span>Cambiar contraseña</span>
                    </a>
                </li>


            </ul>
        </div>
        <div class="tab-content">

            <!--///////////   inicio    /////////////////////    -->
            <div class="tab-pane active" id="datos">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">


                        <div class="box-header">
                            <h3 class="box-title">Mis datos personales</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <form action="" method="POST" id="formularioEditarPersona" >
                                <input id="token_editar" name="token_editar" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="id_persona" name="id_persona" type="hidden" value=""/>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <center><h3 class="panel-title" id="informacionPersonal" style="font-weight: bold"></h3></center>
                                        <br>
                                        <center>
                                            <img width="150" src="assets/img/avatar5.png" height="150" id="imagen_preview" class="ImagePreviewBox" />
                                            <br>
                                            <button type="button" class="btn btn-default" onclick="rotar_foto(1, 1)" data-toggle="tooltip" title data-original-title="Girar foto a la izquierda">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                            </button><button type="button" class="btn btn-default" onclick="rotar_foto(1, 2)" data-toggle="tooltip" title data-original-title="Girar foto a la derecha">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </button>
                                            <br><br><span class="btn btn-primary btn-file">
                                                TOMAR FOTO / SUBIR IMÁGEN <input type="file" id="imagen" class="btn btn-success btn-block" onchange="revisarImagen(this, 1);" value="TOMAR FOTO / SUBIR IMÁGEN" ></span>

                                        </center>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="name" style="clear:both;">Nombre</label>
                                    <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="form-control" autocomplete="off">
                                </div>


                                <div class="col-md-4">
                                    <label for="paterno" style="clear:both;">Paterno</label>
                                    <input type="text" id="paterno" name="paterno" placeholder="Paterno" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-4">
                                    <label for="materno" style="clear:both;">Materno</label>
                                    <input type="text" id="materno" name="materno" placeholder="Materno" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-4">
                                    <label for="ci" style="clear:both;">C.I.</label>
                                    <input type="text" id="ci" name="ci" placeholder="C.I." class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-4">
                                    <label for="ci_expedido" style="clear:both;">Expedido</label>
                                    <select name="ci_expedido" id="ci_expedido" class="form-control">
                                        <option value="0">Seleccione lugar expedido</option>
                                        @foreach($departamentos as $departamento)
                                        <option value="{{$departamento->o_dep_id}}">{{$departamento->o_departamento}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="dob" style="clear:both;">Fecha de nacimiento</label>
                                    <input type="text" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="YYYY-MM-DD" class="form-control datepicker" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label for="area" style="clear:both;">Área</label>
                                    <select name="area" id="area" class="form-control">
                                        <option value="0">Seleccione Área</option>
                                        @foreach($areas as $area)
                                        <option value="{{$area->o_id}}">{{$area->o_tipo_proceso}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="matricula" style="clear:both;">Matricula</label>
                                    <input type="matricula" id="matricula" name="matricula" placeholder="" class="form-control">
                                </div>


                                <div class="col-md-4">
                                    <label for="email" style="clear:both;">Correo</label>
                                    <input type="text" id="email" name="email" placeholder="ejemplo@ejemplo.com" class="form-control">
                                </div>

                                <div class="col-md-4">
                                    <label for="contact" style="clear:both;">Teléfono/Celular</label>
                                    <input type="text" id="contacto" name="contacto" placeholder="XXXXXXX" class="form-control">
                                </div>

                                <div class="col-md-4">
                                    <label for="contact" style="clear:both;">Dirección</label>
                                    <textarea name="direccion" id="direccion"  class="form-control" placeholder="Dirección"></textarea>
                                </div>

                            </form>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <a id="actualizar_persona" class="btn btn-primary">Actualizar</a>
                        </div>

                    </div><!-- /.box -->
                </div>
            </div>
            <div class="tab-pane fade" id="contrasena">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">


                        <div class="box-header">
                            <h3 class="box-title">Cambiar contraseña</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <form action="" method="POST" id="formularioEditarC" >
                                <input id="token_editar_c" name="token_editar_c" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="id_persona_c" name="id_persona_c" type="hidden" value=""/>

                                <div class="col-md-4">
                                    <label for="password" style="clear:both;">Nueva contraseña</label>
                                    <input type="password" id="password" name="password" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-4">
                                    <label for="password" style="clear:both;">Repita contraseña</label>
                                    <input type="password" id="password-confirm" name="password-confirm" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-1">
                                    <label for="password" style="clear:both;">Ver</label>
                                    <a id="contrasena" class="btn btn-primary form-control" onclick="hideOrShowPassword()"><i class="fa fa-eye"></i></a>
                                </div>

                            </form>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <a id="actualizar_contrasena" class="btn btn-primary">Cambiar contraseña</a>
                        </div>

                    </div><!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</section>

<script>
        jQuery('.datepicker').datetimepicker({
        lang: 'es',
        i18n: {
            de: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do.", "Lu", "Ma", "Mi",
                    "Ju", "Vi", "Sa.",
                ]
            }
        },
        timepicker: false,
        format: 'Y-m-d'
    });

    $.ajax({
        url: '/perfil/<?php echo Auth::user()->id ?>/edit',
        headers: {'X-CSRF-TOKEN': $("#token_editar").val()},
        success: function (data) {
            console.log(data[0]);
            $("#nombre").val(data[0].o_prs_nombres);
            $("#paterno").val(data[0].o_prs_paterno);
            $("#materno").val(data[0].o_prs_materno);
            $("#ci").val(data[0].o_prs_ci);
            $("#ci_expedido").val(data[0].o_prs_expedido);
            $("#area").val(data[0].o_prs_id_area);
            $("#matricula").val(data[0].o_prs_matricula);
            //$("#fecha_nacimiento").datepicker({dateFormat: 'dd/mm/yy', firstDay: 1}).datepicker("setDate", moment(data[0].o_prs_fecha_nacimiento).format('YYYY-MM-DD'));
            $("#fecha_nacimiento").val(data[0].o_prs_fecha_nacimiento);
            //document.getElementById("fecha_nacimiento").value = data[0].o_prs_fecha_nacimiento.toJSON().slice(0,10);
            $("#email").val(data[0].o_prs_email);
            $("#contacto").val(data[0].o_prs_contacto);
            $("#direccion").val(data[0].o_prs_direccion);
            $("#id_persona").val(data[0].o_prs_id);
            $("#id_persona_c").val(data[0].o_prs_id)
            var hora = new Date().getTime();
            if (data[0].o_prs_url !== ' ') {
                document.getElementById('imagen_preview').src = data[0].o_prs_url + '?' + hora;
                document.getElementById('foto_perfil').src = data[0].o_prs_url + '?' + hora;
                document.getElementById('foto_perfil_menu').src = data[0].o_prs_url + '?' + hora;
            }
        }, error: function (result) {
            swal("Opss..!", "Succedio un problema al recuperar sus datos, refresque la pagina otra vez!", "error");
        }
    });
    function hideOrShowPassword() {
        var x = document.getElementById("password");
        var y = document.getElementById("password-confirm");
        if (x.type === "password") {
            x.type = "text";
            y.type = "text";
        } else {
            x.type = "password";
            y.type = "password";
        }
    }

    $("#actualizar_persona").click(function () {

        var route = "empleados/" + $("#id_persona").val();
        var token = $("#token_editar").val();
        swal({
            title: "¿Desea editar el registro?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, EDITAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioEditarPersona').serialize(),
                            success: function (data) {
                                swal(data.retorno_codigo, data.err_mensaje, "success");
                                window.location.href = "/perfil"
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    });
    $("#actualizar_contrasena").click(function () {

        var route = "perfil/" + $("#id_persona_c").val();
        var token = $("#token_editar_c").val();
        swal({
            title: "¿Desea cambiar su contraseña?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, CAMBIAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioEditarC').serialize(),
                            success: function (data) {
                                console.log(data);
                                swal(data[0].retorno_codigo, data[0].err_mensaje, "success");
                                
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    });
    function revisarImagen(input, num) {
        var id_preview = input.getAttribute("id") + "_preview";
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function (e) {
                var id_preview_text = "#" + id_preview;
                var base64image = e.target.result;
                $("body").append("<canvas id='tempCanvas' width='150' height='150' style='display:none'></canvas>");
                var canvas = document.getElementById("tempCanvas");
                var ctx = canvas.getContext("2d");
                var cw = canvas.width;
                var ch = canvas.height;
                var maxW = 150;
                var maxH = 150;
                var img = new Image;
                img.src = this.result;
                img.onload = function () {
                    var iw = img.width;
                    var ih = img.height;
                    var scale = Math.min((maxW / iw), (maxH / ih));
                    var iwScaled = iw * scale;
                    var ihScaled = ih * scale;
                    canvas.width = iwScaled;
                    canvas.height = ihScaled;
                    ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
                    base64image = canvas.toDataURL("image/jpeg");
                    $(id_preview_text).attr('src', base64image).width(150).height(150);
                    imagen[num] = base64image;
                    $("#tempCanvas").remove();
                }
            };
            reader.readAsDataURL(input.files[0]);
        }


        // creacion de registro POST
        var form_data = new FormData();
        var file_data = $('#imagen').prop('files')[0];
        form_data.append('prs_id', $('#id_persona').val());
        form_data.append('prs_imagen', file_data);
        //console.log(form_data);
        $.ajax({
            url: "/subirFoto",
            headers: {'X-CSRF-TOKEN': $("#token_editar").val()},
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'text',
            data: form_data,
            success: function (data) {
                var imagen = JSON.parse(data);
                var hora = new Date().getTime();
                document.getElementById('imagen_preview').src = imagen.imagen + '?' + hora;
                document.getElementById('foto_perfil').src = imagen.imagen + '?' + hora;
                document.getElementById('foto_perfil_menu').src = imagen.imagen + '?' + hora;
                $('#imagen').val('');
                swal("Foto fue subida", "Correctamente!", "success");
            }, error: function (result) {
                $.LoadingOverlay("hide", true);
                swal("Opss..!", "No se pudo actualizar foto de la persona!", "error")
            }
        });
    }
</script>
@endsection
