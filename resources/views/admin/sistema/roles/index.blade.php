@extends('layouts.partials.main')
@section('content')
@include('admin.sistema.roles.partials.modalCreate')
@include('admin.sistema.roles.partials.modalUpdate')
<section class="content-header">
    <h1>
        <?php echo 'Roles' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> <?php echo 'Dashboard' ?></a></li>
        <li class="active"><?php echo 'Roles' ?></li>
    </ol>
</section>
<div class="col-xs-12">
    <div class="btn-group pull-right">
        <button class="btn btn-primary fa fa-plus-square pull-right" data-target="#myCreate" data-toggle="modal">&nbsp;Nuevo</button>
    </div>    
</div>
<div class="row" style="margin-bottom:10px;">

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <!--<h3 class="box-title">Tabla grupos</h3>-->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">    
                        <table class="table table-hover table-bordered" id="lts-rol">
                            <thead class="cf">
                                <tr>
                                    <th>
                                        Rol
                                    </th>
                                    <th>
                                        Fecha modificado
                                    </th>
                                    <th>
                                        Usuario modificación
                                    </th>
                                    <th>
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <!-- <ul class="pagination pagination-sm no-margin pull-right">
                          <li><a href="#">«</a></li>
                          <li><a href="#">1</a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">»</a></li>
                        </ul> -->
                    </div>
                </div>
            </div>       
        </div>
    </section>

    <!-- <div id="no-more-tables">
        <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-rol">
            <thead class="cf">
                <tr>
                    <th>
                        Operaciones
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Fecha registro
                    </th>
                    <th>
                        Fecha modificado
                    </th>
                    <th>
                        Estado
                    </th>
                </tr>
            </thead>
        </table>
    </div> -->

    <script>
        function Mostrar(id) {
            $('#myUpdate').modal();
            var route = "/roles/" + id + "/edit";
            $.get(route, function (res) {
                $("#rls_rol1").val(res.rol);
                $("#id").val(res.id);
            });
        }
        function Eliminar(id) {
            var route = "/roles/" + id + "";
            var token = $("#token").val();
            swal({title: "Esta seguro de eliminar el Rol?",
                text: "Presione ok para eliminar el registro de la base de datos!",
                type: "warning", showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    dataType: 'json',

                    success: function (data) {
                        $('#lts-rol').DataTable().ajax.reload();
                        swal("Rol!", "Fue eliminado correctamente!", "success");
                    },
                    error: function (result) {
                        swal("Opss..!", "El Grupo tiene registros en otras tablas!", "error")
                    }
                });
            });
        }
        $(function () {
            $('#lts-rol').DataTable({

                "processing": true,
                "serverSide": true,
                "ajax": "/roles/create",
                "columns": [
                    {data: 'rol', orderable: false},
                    {data: 'modificado'},
                    {data: 'usuario'},
                    {data: 'acciones', searchable: false},
                ],

                "language": {
                    "url": "/lenguaje"
                },
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

            });


            $("#registro").click(function () {
                var route = "/roles";
                var token = $("#token").val();
                //$('#myCreate').html('<div><img src="../img/ajax-loader.gif"/></div>');
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    data: {'rls_rol': $("#rls_rol").val()},
                    success: function (data) {
                        //$('#myCreate').fadeIn(1000).html(data);
                        $("#myCreate").modal('toggle');
                        swal("El Rol!", "Se registrado correctamente!", "success");
                        $('#lts-rol').DataTable().ajax.reload();

                    },
                    error: function (result) {
                        swal("Opss..!", "Succedio un problema al registrar inserte bien los datos!", "error");
                    }
                });
            });



            $("#actualizar").click(function () {
                var value = $("#id").val();
                var route = "/roles/" + value + "";
                var token = $("#token").val();
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'PUT',
                    dataType: 'json',
                    data: {'rls_rol': $("#rls_rol1").val()},
                    success: function (data) {
                        $("#myUpdate").modal('toggle');
                        swal("El Rol!", "Fue actualizado correctamente!", "success");
                        $('#lts-rol').DataTable().ajax.reload();


                    }, error: function (result) {
                        console.log(result);
                        swal("Opss..!", "El Rol no se puedo actualizar intente de nuevo!", "error")
                    }
                });
            });

        });
    </script>
    @endsection
