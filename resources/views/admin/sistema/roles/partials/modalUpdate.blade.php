<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Modificar Rol
                </h4>
            </div>
            <form id='rolud'>
                <div class="modal-body">
                    <input type="hidden" name="csrf-token" value="{{ csrf_token() }}" id="token">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="grupo" style="clear:both;">Rol</label>
                        <input placeholder = 'Nombre de Rol' class = 'form-control' id= 'rls_rol1'>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <a title='Actualizar'  id='actualizar' class='btn btn-primary'>Registrar </a>
                </div>
            </form>
        </div>
    </div>
</div>