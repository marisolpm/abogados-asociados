@extends('layouts.partials.main')
@section('content')
@include('admin.sistema.opciones.partials.modalCreate')
@include('admin.sistema.opciones.partials.modalUpdate')
<section class="content-header">
    <h1>
        Opciones
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Opciones</li>
    </ol>
</section>

<section class="content">
    <div class="row" style="margin-bottom:10px;">
        <div class="col-xs-12">
            <div class="btn-group pull-right">
                <button class="btn btn-primary fa fa-plus-square pull-right" data-target="#myCreate" data-toggle="modal" type="button">&nbsp;Nuevo</button>            </div>
        </div>    
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body"> 
                    <table id="lts-opcion" class="table table-hover table-bordered" style="width: 100% !important">
                        <thead class="cf">
                            <tr>
                                <th>Grupos</th>
                                <th>Opción</th>
                                <th>Contenido</th>
                                <th>Usuario</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>       
    </div>
</section>



<script>

    function Mostrar(id) {
        $('#myUpdate').modal();
        var route = "/opciones/" + id + "/edit";
        $.get(route, function (res) {
            $("#grpid1").val(res.grp_id);
            $("#opcion1").val(res.opcion);
            $("#contenido1").val(res.contenido);
            $("#id").val(res.id);
        });
    }
    function Eliminar(id) {
        var route = "/opciones/" + id + "";
        var token = $("#token").val();
        swal({title: "Esta seguro de eliminar la opcion?",
            text: "Presione ok para eliminar el registro de la base de datos!",
            type: "warning", showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar!",
            closeOnConfirm: false},
                function () {
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (data) {
                            $('#lts-opcion').DataTable().ajax.reload();
                            swal("Opcion!", "Fue eliminado correctamente!", "success");
                        },
                        error: function (result) {
                            swal("Opss..!", "El Grupo tiene registros en otras tablas!", "error")
                        }
                    });
                });
    }
    $(function () {
        $('#lts-opcion').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/opciones/create",
            "columns": [
                {data: 'grupo', orderable: false},
                {data: 'opcion'},
                {data: 'contenido'},
                {data: 'usuario'},
                {data: 'acciones', searchable: false}
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

        $("#registro").click(function () {
            var route = "/opciones";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {'id': $("#grpid").val(), 'opcion': $("#opcopcion").val(), 'contenido': $("#opccontenido").val()},
                success: function (data) {
                    $("#myCreate").modal('toggle');
                    swal("La opcion!", "Fue registrada correctamente!", "success");
                    $('#lts-opcion').DataTable().ajax.reload();
                },
                error: function (result) {
                    swal("Opss..!", "Succedio un problema al registrar inserte bien los datos!", "error");
                }
            });
        });



        $("#actualizar").click(function () {
            var value = $("#id").val();
            var route = "/opciones/" + value + "";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data: {'grp_id': $("#grpid1").val(), 'opcion': $("#opcion1").val(), 'contenido': $("#contenido1").val()},
                success: function (data) {
                    $("#myUpdate").modal('toggle');
                    swal("Las Opciones!", "Fueron actualizado correctamente!", "success");
                    $('#lts-opcion').DataTable().ajax.reload();
                }, error: function (result) {
                    console.log(result);
                    swal("Opss..!", "Las Opciones no lograron actualizarce intente de nuevo!", "error")
                }
            });
        });


    });
</script>
@endsection
