<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Registrar Opcion
                </h4>
            </div>
            <form id='opciones'>
                <div class="modal-body">

                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="grupo" style="clear:both;">Grupo</label>
                        <select class='form-control' name='grp_id' id='grpid'>
                            @foreach($grupos as $grupo)
                            <option value="{{$grupo->id}}">{{$grupo->grupo}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="opcion" style="clear:both;">Opción</label>
                        <input type = 'text' placeholder = 'Nombre de la opcion' class ='form-control' id='opcopcion'>
                    </div>
                    <div class="form-group">
                        <label for="clntenido" style="clear:both;">Contenido</label>
                        <input type = 'text' placeholder ='Contenido / Detalle' class = 'form-control' id='opccontenido'>
                    </div>
                    </input>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
                    <a title='Registrar' id='registro' class='btn btn-primary'>Registrar</a>

                </div>
            </form>
        </div>
    </div>
</div>