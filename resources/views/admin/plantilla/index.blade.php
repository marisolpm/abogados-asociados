@extends('layouts.partials.main')
@section('content')

<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>Plantillas
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="/cuentas">Plantillas</a></li>
        <li class="active">Listado</li>
    </ol>
</section>

<section class="content" >
    <div class="row">

        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Plantillas</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nueva Plantilla</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped table-responsive" id="lts_plantillas">
                                <thead class="cf">
                                    <tr>
                                        <th>Plantilla</th>
                                        <th>Descarga</th>
                                        <th>Fecha Registro</th>
                                        <th>Usuario Edición</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVA PLANTILLA</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioPlantilla" >
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>

                                <input id="id_mensaje" name="id_mensaje" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>

                                <div class="col-md-3">
                                    <label for="asunto" style="clear:both;">Nombre Plantilla</label>
                                    <input type="text" name="plantilla" id="plantilla" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-6">
                                    <label for="plantilla" style="clear:both;">Plantilla</label>
                                    <input type ="file" name="archivo" id="archivo" class="form-control" placeholder="Cuerpo del mensaje">
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <a  class="btn btn-success"  id="enviar_plantilla">ENVIAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a  class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">

                                        $(function () {

                                            $("#lts_plantillas").DataTable({
                                                processing: true,
                                                serverSide: true,
                                                ajax: '/plantillasListado/-1/-1/-1',
                                                "dom": 'Bfrtip',
                                                "buttons": ['pageLength',
                                                    {
                                                        extend: 'excelHtml5',
                                                        title: 'REPORTE PLANTILLAS',
                                                        text: '<i class="fa fa-file-excel-o"></i>',
                                                        titleAttr: 'Excel',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4]
                                                        }
                                                    },
                                                    {
                                                        extend: 'pdfHtml5',
                                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                                        titleAttr: 'PDF',
                                                        title: 'REPORTE PLANTILLAS',
                                                        exportOptions: {
                                                            columns: [0, 1, 2, 3, 4]
                                                        },
                                                        orientation: 'landscape',
                                                        pageSize: 'Letter',
                                                        download: 'open',
                                                        filename: 'REPORTE PLANTILLAS',
                                                        alignment: 'center'
                                                    }
                                                ],
                                                columns: [
                                                    {data: "o_plantilla", orderable: false},
                                                    {data: "o_enlace"},
                                                    {data: "o_modificado"},
                                                    {data: "o_usuario_mod"},
                                                    {data: "o_accion"}

                                                ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                                                    $('td', nRow).css('background-color', '#FFFFFF');
                                                },
                                                "language": {
                                                    "url": "/lenguaje"
                                                },
                                                "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
                                            });


                                        });

                                        $("#enviar_plantilla").click(function () {
                                            var token = $("#token_registrar").val();
                                            var file_data = $('#archivo').prop('files')[0];
                                            var form_data = new FormData();
                                            form_data.append('archivo', file_data);
                                            form_data.append('plantilla', $('#plantilla').val());

                                            var route = "/plantillas";
                                            swal({
                                                title: "¿Desea registrar plantilla?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, ENVIAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;

                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'POST',
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            data: form_data,
                                                            success: function (data) {
                                                                swal('PLANTILLA : ' + data[0].retorno_codigo, data[0].err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_plantillas').DataTable().ajax.reload();
                                                                document.getElementById('formularioPlantilla').reset();
                                                            }, error: function (result) {
                                                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                            }
                                                        });
                                                    });
                                        });

                                        function descargarPlantilla(id) {
                                            var route = '/plantillas/' + id;
                                            $.ajax({
                                                url: route,
                                                type: 'GET',
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                success: function (data) {
                                                    return false;
                                                }, error: function (result) {
                                                    swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                                                }
                                            });
                                        }

                                        function eliminarPlantilla(id) {
                                            var id_plantilla = id;
                                            var route = "plantillas/" + id_plantilla;
                                            var token = $("#token_registrar").val();
                                            swal({
                                                title: "¿Desea eliminar la plantilla ID: " + id_plantilla + "?",
                                                text: "Se sugiere revise los datos antes de proceder",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "¡Sí, ELIMINAR!",
                                                cancelButtonText: "No, revisar",
                                                closeOnConfirm: false,
                                                closeOnCancel: true,
                                                showLoaderOnConfirm: true
                                            },
                                                    function (isConfirm) {
                                                        if (!isConfirm)
                                                            return;
                                                        $.ajax({
                                                            url: route,
                                                            headers: {'X-CSRF-TOKEN': token},
                                                            type: 'DELETE',
                                                            dataType: 'json',
                                                            data: $('#formularioPlantilla').serialize(),
                                                            success: function (data) {

                                                                swal('PLANTILLA N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                                                $('.nav-tabs a[href="#lista"]').tab('show');
                                                                $('#lts_plantillas').DataTable().ajax.reload();
                                                                document.getElementById('formularioPlantilla').reset();
                                                            }, error: function (result) {
                                                                swal("Opss..!", "La cuenta posee registro en otras tablas, no es posible eliminar!", "error");
                                                            }
                                                        });
                                                    });
                                        }


</script>
@endsection