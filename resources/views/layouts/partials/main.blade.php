<html>
    <head>
        <meta http-equiv="Content-Type" charset="utf-8" /> 
        <title><?php echo "Medina & Asoc."; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset('assets/css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{asset('assets/css/morris/morris.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/pickmeup.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="{{asset('assets/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/redactor.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{asset('assets/plugins/editor/editor.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/datepicker.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/chosen.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/fullcalendar/fullcalendar.print.css')}}" media='print' rel="stylesheet" type="text/css" />

        <!-- jQuery 2.0.2 -->
        <script src="{{asset('assets/js/jquery.js')}}"></script>

        <link href="{{asset('assets/css/jquery.datetimepicker.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/js/pdfDatables/buttons.dataTables.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/js/pdfDatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">



    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->


                Medina & Asoc.

            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only"><?php echo 'toggle_navigation'; ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->


                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo Auth::user()->usuario ?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                     @inject('imagen','App\Http\Controllers\Admin\MenuController')
                                    <img src="{{$imagen->imagenPersona()->imagen}}" id="foto_perfil_menu" class="img-circle" alt="Foto de usuario" id="imagen_user" />

                                    <p>
                                        <?php echo Auth::user()->usuario ?>

                                    </p>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="perfil" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
    document.getElementById('logout-form').submit();" class="btn btn-default btn-flat"><?php echo 'Cerrar Sesión'; ?>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input id="token_persona" name="token_persona" type="hidden" value="{{ csrf_token() }}"/>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>




        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{$imagen->imagenPersona()->imagen}}" id="foto_perfil" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo Auth::user()->usuario ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        @php $class = ''; $classS = ''; $colorB = ''; @endphp
                        @inject('menu','App\Http\Controllers\Admin\MenuController')
                        @foreach ($menu->submenus() as $link01)
                        @php
                        $grupo01=0;
                        $grupo01= $link01->grupo;
                        $grupo01_id= $link01->id;
                        $grupo01_img= $link01->imagen;
                        @endphp
                        @if($grupo01 == $_SESSION['grupoController']) @php  $class = 'active'; @endphp @else  @php $class = ''; @endphp @endif
                        <li class="treeview {{$class}}">
                            <a href="#" class="">
                                <i class="{{$grupo01_img}}"></i> <span>{{$grupo01}}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                @foreach ($menu->links($grupo01_id) as $link02)
                                @if($link02->contenido == $_SESSION['sGrupoController']) @php $classS = 'active'; $colorB = '#d6d6d6'; @endphp @else @php $classS = ''; $colorB = ''; @endphp @endif
                                <li class="{{$classS}}" style="background-color: {{$colorB}}">
                                    <a href="/{{$link02->contenido}}">
                                        <i class="fa  fa-angle-double-right"></i> <span>{{$link02->opcion}}</span> 
                                    </a>
                                </li>


                                @endforeach
                            </ul>
                        </li>
                        @endforeach

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <script src="{{asset('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
            <script src="{{asset('assets/js/jquery.datetimepicker.js')}}" type="text/javascript"></script>
            <aside class="right-side">

                @yield('content')
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->

        </div><!-- ./wrapper -->
        <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="{{asset('assets/js/jquery-ui-1.10.3.js')}}" type="text/javascript"></script>

        <script src="{{asset('assets/js/jquery.pickmeup.min.js')}}" type="text/javascript"></script>
        <!-- Bootstrap -->

        <script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

        <!-- iCheck -->
        <script src="{{asset('assets/js/plugins/iCheck/icheck.min.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>



        <script src="{{asset('assets/sweetalert/dist/sweetalert.min.js')}}" type="text/javascript"></script>

        <script src="{{asset('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/jquery.datetimepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/plugins/fullcalendar/moment.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/plugins/fullcalendar/lang-all.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/pdfDatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/buttons.flash.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/jszip.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfDatables/buttons.print.min.js')}}"></script>

        <script>
$.ajax({
    url: '/perfil/<?php echo Auth::user()->id ?>/edit',
    headers: {'X-CSRF-TOKEN': $("#token_persona").val()},
    success: function (data) {
        var hora = new Date().getTime();
        if (data[0].o_prs_url !== ' ') {
            document.getElementById('foto_perfil').src = data[0].o_prs_url + '?' + hora;
            document.getElementById('foto_perfil_menu').src = data[0].o_prs_url + '?' + hora;
        }else{
            document.getElementById('foto_perfil').src = 'assets/img/avatar5.png' + '?' + hora;
            document.getElementById('foto_perfil_menu').src = 'assets/img/avatar5.png' + '?' + hora;
        }
    }, error: function (result) {
        swal("Opss..!", "Succedio un problema al recuperar sus datos, refresque la pagina otra vez!", "error");
    }
});
function rotar_foto(prs_id, tipo_rotacion) {
    prs_id = $("#id_persona").val();
    $.ajax({
        data: {prs_id: prs_id, tipo_rotacion: tipo_rotacion},
        url: 'rotarFoto',
        headers: {'X-CSRF-TOKEN': $("#token_editar").val()},
        type: 'POST',
        dataType: 'json',
        cache: false,
        success: function (r)
        {
            var hora = new Date().getTime();
            if (r.imagen !== ' ') {
                document.getElementById('foto_perfil').src = r.imagen + '?' + hora;
                document.getElementById('foto_perfil_menu').src = r.imagen + '?' + hora;
                imageObject = document.getElementById('imagen_preview');
                imageObject.src = r.imagen + '?' + hora;
            }

        },
        error: function ()
        {
            alert('Ocurrio un error en el servidor ..');
        }
    });
}
        </script>
    </body>
</html>